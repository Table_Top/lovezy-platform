package com.lovezy.platform.core.valid.annotation;


import com.lovezy.platform.core.valid.validators.ExpressionValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 通过正则表达式校验
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@ValidatorProvider(ExpressionValidator.class)
public @interface Expression {

	/**
	 * 正则表达式
	 * @return 正则
	 */
	String expression();

	/**
	 * 错误编号
	 * @return 编号
	 */
	String errorCode();

	/**
	 * 错误信息
	 * @return 信息
	 */
	String errorMsg();
}
