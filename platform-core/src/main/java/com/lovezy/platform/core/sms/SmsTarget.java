package com.lovezy.platform.core.sms;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 短信目标
 * Created by jin on 2017/11/10.
 */
@Getter
@Setter
@ToString
public class SmsTarget {

    private String mobile;

    private String text;

    private String templateCode;

}
