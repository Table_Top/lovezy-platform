package com.lovezy.platform.core.valid.annotation;

import com.lovezy.platform.core.exception.DefaultErrorCode;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 必填校验
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
public @interface Required {
	/**
	 * 错误码
	 * 
	 * @return
	 */
	String errorCode() default DefaultErrorCode.VALID_ERROR;

	/**
	 * 错误信息
	 * 
	 * @return
	 */
	String errorMsg();
	

}
