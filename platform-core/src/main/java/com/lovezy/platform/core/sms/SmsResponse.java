package com.lovezy.platform.core.sms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 短信结果
 * Created by jin on 2017/11/10.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SmsResponse {

    private boolean success = true;

    private String failReason;

}
