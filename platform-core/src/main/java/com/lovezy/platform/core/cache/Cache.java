package com.lovezy.platform.core.cache;


import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.function.Supplier;

/**
 * 抽象cache接口
 * Created by jin on 2017/11/6.
 */
public interface Cache {

    @Nullable
    <V> V getIfPresent(String key);


    @Nullable
    <V> V get(String key, Supplier<? extends V> supplier) ;


    <V> ImmutableMap<String,V> getAllPresent(Iterable<String> keys);


    <V> void put(String key, V value);


    <V> void putAll(Map<String, ? extends V> m);


    void invalidate(String key);


    void invalidateAll(Iterable<String> keys);


    void invalidateAll();


}
