package com.lovezy.platform.core.valid.validators;


import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.valid.Validator;
import com.lovezy.platform.core.valid.annotation.Max;

import java.lang.annotation.Annotation;

/**
 * 
 * 最大值校验
 *
 */
public class MaxValidator implements Validator {
	@Override
	public void validate(Object value, Annotation annotation) {
		Max an = (Max)annotation;
		double d = new Double(value.toString());

		if (d > an.max()) {
			throw new ValidationException(an.errorCode(), an.errorMsg());
		}
	}
}
