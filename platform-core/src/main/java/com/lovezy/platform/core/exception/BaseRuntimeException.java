package com.lovezy.platform.core.exception;

/**
 * 基础异常
 * Created by jin on 2017/11/4.
 */
public class BaseRuntimeException extends RuntimeException{

    private String errorCode;

    private String errorMsg;

    public BaseRuntimeException(String errorCode,String message) {
        super(message);
        this.errorCode = errorCode;
        this.errorMsg = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
