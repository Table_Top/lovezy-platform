package com.lovezy.platform.core.mq;

/**
 * 消息消费者
 * Created by jin on 2017/12/17.
 */
public interface MessageConsumer<T> {

    void accept(T t);

}
