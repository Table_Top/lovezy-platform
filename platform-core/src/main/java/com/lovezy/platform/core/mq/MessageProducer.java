package com.lovezy.platform.core.mq;

import lombok.AllArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsMessagingTemplate;

import javax.jms.JMSException;
import javax.jms.Queue;

/**
 * 消息生产者
 * Created by jin on 2017/12/17.
 */
@AllArgsConstructor
public class MessageProducer {

    private final Log log = LogFactory.getLog(MessageProducer.class);

    private JmsMessagingTemplate template;

    private Queue queue;

    public void send(Object msg) {
        if (log.isDebugEnabled()) {
            try {
                log.debug("发送到消息队列(" + queue.getQueueName() + ") -> " + msg);
            } catch (JMSException e) {
                log.error("得到消息队列名称失败", e);
            }
        }
        template.convertAndSend(queue, msg);
    }

}
