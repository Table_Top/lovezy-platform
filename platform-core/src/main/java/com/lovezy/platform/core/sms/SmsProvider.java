package com.lovezy.platform.core.sms;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 短信服务提供商
 * Created by jin on 2017/11/10.
 */
@Getter
@Setter
@ToString
public abstract class SmsProvider {

    protected Log log = LogFactory.getLog(this.getClass());

    protected String appKey;

    protected String appSecret;

    protected String serverUrl;

    protected String signName;

    public abstract SmsResponse send(SmsTarget target);

}
