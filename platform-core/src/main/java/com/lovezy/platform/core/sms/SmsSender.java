package com.lovezy.platform.core.sms;

import com.lovezy.platform.core.exception.DefaultErrorCode;
import com.lovezy.platform.core.exception.ServiceException;

import static com.lovezy.platform.core.utils.ObjectUtil.ifNull;

/**
 * 短信发送器
 * Created by jin on 2017/11/10.
 */
public class SmsSender {

    private SmsProvider provider;

    public SmsSender(SmsProvider provider) {
        this.provider = provider;
    }

    public SmsResponse send(SmsTarget target) {
        if (ifNull(target.getMobile(), target.getText())) {
            throw new ServiceException(DefaultErrorCode.SERVICE_ERROR, "短信目标数据为空");
        }
        return provider.send(target);
    }
}
