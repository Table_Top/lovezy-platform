package com.lovezy.platform.core.cache;

import com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * redis cache
 * Created by jin on 2017/12/23.
 */
@SuppressWarnings("unchecked")
@Slf4j
public class RedisCache implements Cache{

    private final static Integer REDIS_CACHE_EXPIRE_DAY = 7;

    private RedisTemplate<String, Object> redisTemplate;

    public RedisCache(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
        log.debug("初始化Cache:RedisCache ,缓存默认有效期为{}天", REDIS_CACHE_EXPIRE_DAY);
    }

    @Override
    public <V> @Nullable V getIfPresent(String key) {
        V value =  (V)redisTemplate.opsForValue().get(key);
        if (log.isDebugEnabled()) {
            log.debug("从缓存中尝试取" + key + "得到value:" + value);
        }
        return value;
    }

    @Override
    public <V> @Nullable V get(String key, Supplier<? extends V> supplier) {
        V v = (V)redisTemplate.opsForValue().get(key);
        if (v != null) {
            if (log.isDebugEnabled()) {
                log.debug("从缓存中取到" + key + "value:" + v);
            }
            return v;
        } else {
            V newV = supplier.get();
            redisTemplate.opsForValue().set(key, newV,REDIS_CACHE_EXPIRE_DAY, TimeUnit.DAYS);
            if (log.isDebugEnabled()) {
                log.debug("未从缓存中取到" + key + ",设置缓存:" + newV);
            }
            return newV;
        }
    }

    @Override
    public <V> ImmutableMap<String, V> getAllPresent(Iterable<String> keys) {
        Map<String, V> map = new HashMap<>();
        keys.forEach(k -> {
            V v1 = (V)redisTemplate.opsForValue().get(k);
            if (v1 != null) {
                map.put(k, v1);
            }
        });
        return ImmutableMap.copyOf(map);
    }

    @Override
    public <V> void put(String key, V value) {
        if (value == null) {
            if (log.isWarnEnabled()) {
                log.warn("设置缓存Key:{}的值为空", key);
            }
        }
        redisTemplate.opsForValue().set(key, value, REDIS_CACHE_EXPIRE_DAY, TimeUnit.DAYS);
    }

    @Override
    public <V> void putAll(Map<String, ? extends V> m) {
        m.forEach((k, v) -> redisTemplate.opsForValue().set(k, v, REDIS_CACHE_EXPIRE_DAY, TimeUnit.DAYS));
    }

    @Override
    public void invalidate(String key) {
        redisTemplate.delete(key);
    }

    @Override
    public void invalidateAll(Iterable<String> keys) {
        Stream.Builder<String> builder = Stream.builder();
        keys.forEach(builder::add);
        List<String> collect = builder.build().collect(toList());
        redisTemplate.delete(collect);
    }

    @Override
    public void invalidateAll() {
        log.warn("Redis cache do noting for invalidate all");
    }
}
