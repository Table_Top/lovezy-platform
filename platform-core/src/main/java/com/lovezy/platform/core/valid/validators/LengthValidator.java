package com.lovezy.platform.core.valid.validators;


import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.valid.Validator;
import com.lovezy.platform.core.valid.annotation.Length;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;

/**
 * 
 * 正则表达式校验
 */
@Slf4j
public class LengthValidator implements Validator {
	@Override
	public void validate(Object value, Annotation annotation) {
		Length an = (Length) annotation;
		if (value == null) {
			log.error("LengthValidator null value");
			return;
		}
		int d = value.toString().length();
		if (d > an.maxLength()) {
			throw new ValidationException(an.errorCode(), an.errorMsg());
		}
	}
}
