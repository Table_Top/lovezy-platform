package com.lovezy.platform.core.valid.annotation;


import com.lovezy.platform.core.valid.validators.ArrayValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 判断是否为数组，并且校验是否为空
 * Created by jin on 2016/12/30.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@ValidatorProvider(ArrayValidator.class)
public @interface NotEmptyArray {
    /**
     * 错误码
     *
     * @return
     */
    String errorCode();

    /**
     * 错误信息
     *
     * @return
     */
    String errorMsg();

}

