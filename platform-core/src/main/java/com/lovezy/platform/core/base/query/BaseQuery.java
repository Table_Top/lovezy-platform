package com.lovezy.platform.core.base.query;

/**
 * 查询基类
 * Created by jin on 2017/11/5.
 */
public class BaseQuery {

    private boolean limit = true;

    private Integer page = 1;

    private Integer pageSize = 10;

    private String sort = SortTypeEnum.ASC.sort;

    public void setPage(Integer page) {
        if(page == null) return;
        this.page = page;
    }

    public Integer getPage() {
        return page;
    }

    public void setPageSize(Integer pageSize) {
        if(pageSize == null) return;
        this.pageSize = pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public boolean isLimit() {
        return limit;
    }

    public void setLimit(boolean limit) {
        this.limit = limit;
    }

    public void setSort(byte sortType) {
        if (sortType == SortTypeEnum.DESC.type) {
            this.sort = SortTypeEnum.DESC.sort;
        }
    }

    public String getSort() {
        return sort;
    }
}
