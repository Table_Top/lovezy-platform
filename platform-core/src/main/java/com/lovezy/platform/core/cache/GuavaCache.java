package com.lovezy.platform.core.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.function.Supplier;

/**
 * Guava 缓存
 * Created by jin on 2017/11/6.
 */
public class GuavaCache implements Cache{

    private final Log log = LogFactory.getLog(this.getClass());

    private final Integer DEFAULT_MAXSIZE = 1000;

    private com.google.common.cache.Cache<String, Object> guavaCacheInstance;

    public GuavaCache() {
        init(DEFAULT_MAXSIZE);
    }

    public GuavaCache(Integer maxSize) {
        init(maxSize);
    }

    private void init(Integer maxSize) {
        log.debug("初始化Cache:GuavaCache : maxSize = " + maxSize);
        guavaCacheInstance = CacheBuilder.newBuilder().maximumSize(maxSize).build();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <V> @Nullable V getIfPresent(String key) {
        return (V)guavaCacheInstance.getIfPresent(key);
    }

    @Override
    @SuppressWarnings("unchecked")
    @Nullable
    public <V> V get(String key, Supplier<? extends V> loader) {
        Object o = guavaCacheInstance.getIfPresent(key);
        if (o != null) {
            if (log.isDebugEnabled()) {
                log.debug("从缓存中取到" + key + "value:" + o);
            }
            return (V) o;
        }else{
            V v =  loader.get();
            if (log.isDebugEnabled()) {
                log.debug("未从缓存中取到" + key + ",设置缓存:" + v);
            }
            if (v != null) {
                guavaCacheInstance.put(key, v);
            }
            return v;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <V> ImmutableMap<String, V> getAllPresent(Iterable<String> keys) {
        return (ImmutableMap<String, V>) guavaCacheInstance.getAllPresent(keys);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <V> void put(String key, V value) {
        guavaCacheInstance.put(key, value);
    }

    @Override
    public <V> void putAll(Map<String, ? extends V> m) {
        guavaCacheInstance.putAll(m);
    }

    @Override
    public void invalidate(String key) {
        guavaCacheInstance.invalidate(key);
    }

    @Override
    public void invalidateAll(Iterable<String> keys) {
        guavaCacheInstance.invalidateAll(keys);
    }

    @Override
    public void invalidateAll() {
        guavaCacheInstance.invalidateAll();
    }
}
