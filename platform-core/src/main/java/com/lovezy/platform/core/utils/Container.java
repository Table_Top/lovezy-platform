package com.lovezy.platform.core.utils;

/**
 * 容器类
 * Created by jin on 2017/11/29.
 */
public class Container<T> {

    private T t ;

    private Container(){
    }

    public static <T> Container<T> of(T t) {
        Container<T> container = new Container<>();
        container.t = t;
        return container;
    }

    public static <T> Container<T> empty() {
        return new Container<>();
    }

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

}
