package com.lovezy.platform.core.cache;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

/**
 * 可序列化的缓存容器
 * Created by jin on 2017/12/23.
 */
@Getter
@AllArgsConstructor
public class SerializableCacheContainer<V> implements Serializable{

    private V value;


    @Override
    public String toString() {
        if (value == null) {
            return "NULL";
        }else{
            return value.toString();
        }
    }
}
