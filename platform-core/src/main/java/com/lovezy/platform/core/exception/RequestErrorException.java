package com.lovezy.platform.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static com.lovezy.platform.core.exception.DefaultErrorCode.BAD_REQUEST;

/**
 * 请求参数错误异常
 * Created by jin on 2017/11/8.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RequestErrorException extends BaseRuntimeException{

    public RequestErrorException(String errorCode, String message) {
        super(errorCode, message);
    }

    public RequestErrorException() {
        super(BAD_REQUEST,"请求参数有误");
    }

    public RequestErrorException(String errorMsg) {
        super(BAD_REQUEST,errorMsg);
    }


}
