package com.lovezy.platform.core.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 错误响应
 * Created by jin on 2017/11/6.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse extends Response{

    private String errorCode;

    private String errorMsg;

    {
        setStatus(false);
    }
}
