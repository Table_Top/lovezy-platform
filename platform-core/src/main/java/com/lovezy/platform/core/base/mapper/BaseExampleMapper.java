package com.lovezy.platform.core.base.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by jin on 2017/11/3.
 */
public interface BaseExampleMapper<T,E,ID> extends BaseMapper<T,ID>{
    int countByExample(E example);

    int deleteByExample(E example);

    List<T> selectByExample(E example);

    int updateByExampleSelective(@Param("record") T record, @Param("example") E example);

    int updateByExample(@Param("record") T record, @Param("example") E example);

}
