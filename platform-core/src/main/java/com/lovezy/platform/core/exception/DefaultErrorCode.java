package com.lovezy.platform.core.exception;

/**
 * 默认公共错误编码
 * Created by jin on 2017/11/5.
 */
public class DefaultErrorCode {

    private final static String prefix = "COMMON-";

    public final static String NOT_LOGIN = prefix + "000001"; // 未登录

    public final static String SERVICE_ERROR = prefix + "000002"; // 服务端错误;

    public final static String VALID_ERROR = prefix + "000003"; // 数据校验错误

    public final static String CACHE_ERROR = prefix + "000004"; // 缓存失败

    public final static String BAD_REQUEST = prefix + "000005"; // 请求参数错误

    public final static String RESOURCE_NOT_FOUND = prefix + "000006"; // 资源不存在

    public final static String BIZ_ERROR = prefix + "000007"; //业务错误
}
