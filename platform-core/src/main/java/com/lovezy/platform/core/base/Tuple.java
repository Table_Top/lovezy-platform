package com.lovezy.platform.core.base;

import lombok.Getter;
import lombok.Setter;

/**
 * 一个元祖
 * Created by jin on 2017/11/7.
 */
@Getter
@Setter
public final class Tuple<T> {

    private T first;

    private T second;

}
