package com.lovezy.platform.core.base.mapper;

/**
 * 基础mapper
 * Created by jin on 2017/11/4.
 */
public interface BaseMapper<T,ID> {

    int insert(T record);

    int insertSelective(T record);

    T selectByPrimaryKey(ID id);

    int updateByPrimaryKey(T record);

    int updateByPrimaryKeySelective(T record);

    int deleteByPrimaryKey(ID id);

    int softDeleteByPrimaryKey(ID id);
}
