package com.lovezy.platform.core.base.service;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 拥有example方法的服务接口
 * Created by jin on 2017/11/6.
 */
public interface ExampleService<T,Example,ID> extends CrudService<T,ID>{
    int countByExample(Example example);

    int deleteByExample(Example example);

    List<T> selectByExample(Example example);

    int updateByExampleSelective(@Param("record") T record, @Param("example") Example example);

    int updateByExample(@Param("record") T record, @Param("example") Example example);
}
