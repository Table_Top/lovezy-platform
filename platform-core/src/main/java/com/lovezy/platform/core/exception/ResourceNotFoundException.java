package com.lovezy.platform.core.exception;

import com.lovezy.platform.core.base.Tuple;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.function.Supplier;

import static com.lovezy.platform.core.exception.DefaultErrorCode.RESOURCE_NOT_FOUND;

/**
 * 资源不存在异常
 * Created by jin on 2017/11/8.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends BaseRuntimeException{

    public ResourceNotFoundException(Supplier<Tuple<String>> errorInfoSupplier) {
        super(errorInfoSupplier.get().getFirst(),errorInfoSupplier.get().getSecond());
    }

    public ResourceNotFoundException(String errorCode, String message) {
        super(errorCode, message);
    }

    public ResourceNotFoundException() {
        super(RESOURCE_NOT_FOUND,"请求的资源不存在");
    }
}
