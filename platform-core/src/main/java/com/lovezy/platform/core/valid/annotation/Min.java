package com.lovezy.platform.core.valid.annotation;


import com.lovezy.platform.core.exception.DefaultErrorCode;
import com.lovezy.platform.core.valid.validators.MinValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 最小值校验
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@ValidatorProvider(MinValidator.class)
public @interface Min {
	/**
	 * 最小值
	 *
	 * @return
	 */
	double min();

	/**
	 * 错误编号
	 *
	 * @return 编号
	 */
	String errorCode() default DefaultErrorCode.VALID_ERROR;

	/**
	 * 错误信息
	 *
	 * @return 信息
	 */
	String errorMsg();
}
