package com.lovezy.platform.core.base.service;

import com.lovezy.platform.core.base.mapper.BaseMapper;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * CURD 基础实现
 * Created by jin on 2017/11/4.
 */
public abstract class MapperService<T,ID,Mapper extends BaseMapper<T,ID>> {

    protected final Log log = LogFactory.getLog(this.getClass());

    protected Mapper mapper;

    @Autowired
    protected void setMapper(Mapper mapper) {
        this.mapper = mapper;
    }

}
