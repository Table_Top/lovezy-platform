package com.lovezy.platform.core.base.enums;

import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.exception.DefaultErrorCode;

/**
 * 前端类别枚举 1-网页 2-APP
 * Created by jin on 2017/11/5.
 */
public enum FrontEnum {
    WEB((byte)1),
    APP((byte)2),
    ;
    byte value;

    FrontEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }

    public static FrontEnum build(byte value) {
        for (FrontEnum frontEnum : FrontEnum.values()) {
            if (frontEnum.value == value) {
                return frontEnum;
            }
        }
        throw new ValidationException(DefaultErrorCode.VALID_ERROR, "前端类别错误");
    }
}
