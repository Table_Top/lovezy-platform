package com.lovezy.platform.core.valid.annotation;


import com.lovezy.platform.core.valid.validators.NumberFormatValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 校验数字格式(只处理double和float类型的数据)
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@ValidatorProvider(NumberFormatValidator.class)
public @interface NumberFormat {
	/**
	 * 错误码
	 * 
	 * @return
	 */
	String errorCode();

	/**
	 * 错误信息
	 * 
	 * @return
	 */
	String errorMsg();

	/**
	 * 格式(只处理double和float类型的数据)
	 * 
	 * return 范本
	 * 
	 */
	String format();
}
