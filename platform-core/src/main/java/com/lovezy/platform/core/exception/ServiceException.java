package com.lovezy.platform.core.exception;

/**
 * 服务端异常
 * Created by jin on 2017/11/4.
 */
public class ServiceException extends BaseRuntimeException {
    public ServiceException(String errorCode,String message) {
        super(errorCode,message);
    }

    public ServiceException(ErrorMessageAware error) {
        super(error.errorCode(),error.errorMsg());
    }

    public ServiceException(String message) {
        super(DefaultErrorCode.BIZ_ERROR, message);
    }
}
