package com.lovezy.platform.core.base;

import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * app 服务信息
 * Created by jin on 2017/11/6.
 */
@Component
@Getter
@ToString
public final class AppServer {

    @Value("${app.domain}")
    private String domain;

    @Value("${filestore.savePath}")
    private String fileSavePath;

    @Value("${filestore.downloadPath}")
    private String downloadPath;

    @Value("${filestore.tempPath}")
    private String fileTempPath;

    @Value("${app.admin}")
    private String admin;

    @Value("${app.viewDomain}")
    private String viewDomain;

}


