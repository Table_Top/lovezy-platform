package com.lovezy.platform.core.valid.validators;


import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.valid.Validator;
import com.lovezy.platform.core.valid.annotation.Numeric;

import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

public class NumericValidator implements Validator {
	@Override
	public void validate(Object value, Annotation annotation) {
		Numeric an = (Numeric) annotation;
		if (!Pattern.matches("[0-9]+", value.toString())) {
			throw new ValidationException(an.errorCode(), an.errorMsg());
		}
	}
}
