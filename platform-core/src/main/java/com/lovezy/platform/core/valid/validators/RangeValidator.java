package com.lovezy.platform.core.valid.validators;


import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.valid.Validator;
import com.lovezy.platform.core.valid.annotation.Range;

import java.lang.annotation.Annotation;

/**
 *
 * 范围校验
 *
 */
public class RangeValidator implements Validator {
	@Override
	public void validate(Object value, Annotation annotation) {
		if (value == null) {
			return;
		}
		Range an = (Range)annotation;
		Integer v ;
		if (value instanceof Byte) {
			v = ((Byte) value).intValue();
		}else{
			v = ((Integer) value);
		}
		boolean contain = false;
		for (int i : an.range()) {
			if (i == v) {
				contain = true;
			}
		}
		if (!contain) {
			throw new ValidationException(an.errorCode(), an.errorMsg());
		}
	}
}
