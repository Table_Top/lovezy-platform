package com.lovezy.platform.core.exception;

/**
 * 错误信息表现
 * Created by jin on 2017/11/5.
 */
public interface ErrorMessageAware {

    String errorCode();

    String errorMsg();
}
