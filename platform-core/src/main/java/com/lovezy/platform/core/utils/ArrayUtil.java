package com.lovezy.platform.core.utils;

import java.util.Collection;

/**
 * 数组工具类
 * Created by jin on 2017/11/7.
 */
public class ArrayUtil {

    public static boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isEmpty(Object[] arr) {
        return arr == null || arr.length == 0;
    }


}
