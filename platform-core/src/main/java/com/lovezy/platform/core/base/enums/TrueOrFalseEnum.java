package com.lovezy.platform.core.base.enums;

/**
 * 数据状态 1-是 2-否
 * Created by jin on 2017/11/5.
 */
public enum TrueOrFalseEnum {
    TRUE ((byte)1),
    FALSE ((byte)2),
    ;
    byte value;

    TrueOrFalseEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
