package com.lovezy.platform.core.base.enums;

/**
 * 数据状态 1-正常 2-删除 3-禁用
 * Created by jin on 2017/11/5.
 */
public enum StatusEnum {
    NORMAL ((byte)1),
    DELETED ((byte)2),
    DISABLED ((byte)3)
    ;
    byte value;

    StatusEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
