package com.lovezy.platform.core.base.query;

/**
 * 排序类别枚举
 * Created by jin on 2017/11/5.
 */
public enum SortTypeEnum {
    ASC(((byte) 1)," asc "),
    DESC(((byte) 2), " desc "),
    ;
    byte type;
    String sort;

    SortTypeEnum(byte type, String sort) {
        this.type = type;
        this.sort = sort;
    }

    public byte getType() {
        return type;
    }

    public String getSort() {
        return sort;
    }
}
