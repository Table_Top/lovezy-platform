package com.lovezy.platform.core.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * 返回结果
 * Created by jin on 2017/11/7.
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    private boolean status = true;

    private Object data;

    public static Response ok(Object o) {
        Response response = new Response();
        response.setData(o);
        return response;
    }

    public static Response ok() {
        return new Response();
    }
}
