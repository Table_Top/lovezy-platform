package com.lovezy.platform.core.valid;

import java.util.regex.Pattern;


/**
 *
 * 正则表达式
 * 
 */
public class RegExp {
	/**
	 * 邮箱的正则表达式
	 */
	public static final String EMAIL = "^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$"; //"^[a-z0-9_\\-]+(\\.[_a-z0-9\\-]+)*@([_a-z0-9\\-]+\\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)$";
	/**
	 * 手机号的正则表达式
	 * 说明：前缀 +、 国家代码、 手机号、之间可以用  -、*、#、,分割  。注：前缀和分割符可以不要。
	 * 国际手机号码格式  011-86-573-82651630 || 0019494501234 || 
	 *           +86-573-82651630 || +61293525212
	 * 国内   18812700656 || +8618812700656 || +86-18812700656
	 * 
	 */
	public static final String MOBILE ="^1[345789][0-9]{9}|[0-9]{3}[\\-\\*#\\,]{0,1}[0-9]{2}[\\-\\*#\\,]{0,1}[0-9]{3}[\\-\\*#\\,]{0,1}[0-9]{8}|((\\+?)[0-9]{2,3}[\\-\\*#\\,]{0,1}[0-9]{3,4}[\\-\\*#\\,]{0,1}[0-9]{6,8})$";
	
	/**
	 * 手机号的正则表达式
	 */
	public static final String MOBILE_STRICT = "^1[345789][0-9]{9}$";
	/**
	 * IPv4的正则表达式
	 */
	public static final String IP_V4 = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
	/**
	 * URL的正则表达式
	 */
	//public static final String URL = "^([a-z]+:\\/\\/)?([a-z]([a-z0-9\\-]*\\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)|(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(:[0-9]{1,5})?(\\/[a-z0-9_\\-\\.~]+)*(\\/([a-z0-9_\\-\\.]*)(\\?[a-z0-9+_\\-\\.%=&amp;]*)?)?(#[a-z][a-z0-9_]*)?$";
    //20150814 范围扩大  
	public static final String URL=
	        "((?:(http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
	        + "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
	        + "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?"
	        + "((?:(?:[a-zA-Z0-9\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-zA-Z0-9\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\\-]{0,64}\\.)+"   // named host
	        + "(?:"
	        + "(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])"
	        + "|(?:biz|b[abdefghijmnorstvwyz])"
	        + "|(?:cat|com|coop|c[acdfghiklmnoruvxyz])"
	        + "|d[ejkmoz]"
	        + "|(?:edu|e[cegrstu])"
	        + "|f[ijkmor]"
	        + "|(?:gov|g[abdefghilmnpqrstuwy])"
	        + "|h[kmnrtu]"
	        + "|(?:info|int|i[delmnoqrst])"
	        + "|(?:jobs|j[emop])"
	        + "|k[eghimnprwyz]"
	        + "|l[abcikrstuvy]"
	        + "|(?:mil|mobi|museum|m[acdeghklmnopqrstuvwxyz])"
	        + "|(?:name|net|n[acefgilopruz])"
	        + "|(?:org|om)"
	        + "|(?:pro|p[aefghklmnrstwy])"
	        + "|qa"
	        + "|r[eosuw]"
	        + "|s[abcdeghijklmnortuvyz]"
	        + "|(?:tel|travel|t[cdfghjklmnoprtvwz])"
	        + "|u[agksyz]"
	        + "|v[aceginu]"
	        + "|w[fs]"
	        + "|(?:\u03b4\u03bf\u03ba\u03b9\u03bc\u03ae|\u0438\u0441\u043f\u044b\u0442\u0430\u043d\u0438\u0435|\u0440\u0444|\u0441\u0440\u0431|\u05d8\u05e2\u05e1\u05d8|\u0622\u0632\u0645\u0627\u06cc\u0634\u06cc|\u0625\u062e\u062a\u0628\u0627\u0631|\u0627\u0644\u0627\u0631\u062f\u0646|\u0627\u0644\u062c\u0632\u0627\u0626\u0631|\u0627\u0644\u0633\u0639\u0648\u062f\u064a\u0629|\u0627\u0644\u0645\u063a\u0631\u0628|\u0627\u0645\u0627\u0631\u0627\u062a|\u0628\u06be\u0627\u0631\u062a|\u062a\u0648\u0646\u0633|\u0633\u0648\u0631\u064a\u0629|\u0641\u0644\u0633\u0637\u064a\u0646|\u0642\u0637\u0631|\u0645\u0635\u0631|\u092a\u0930\u0940\u0915\u094d\u0937\u093e|\u092d\u093e\u0930\u0924|\u09ad\u09be\u09b0\u09a4|\u0a2d\u0a3e\u0a30\u0a24|\u0aad\u0abe\u0ab0\u0aa4|\u0b87\u0ba8\u0bcd\u0ba4\u0bbf\u0baf\u0bbe|\u0b87\u0bb2\u0b99\u0bcd\u0b95\u0bc8|\u0b9a\u0bbf\u0b99\u0bcd\u0b95\u0baa\u0bcd\u0baa\u0bc2\u0bb0\u0bcd|\u0baa\u0bb0\u0bbf\u0b9f\u0bcd\u0b9a\u0bc8|\u0c2d\u0c3e\u0c30\u0c24\u0c4d|\u0dbd\u0d82\u0d9a\u0dcf|\u0e44\u0e17\u0e22|\u30c6\u30b9\u30c8|\u4e2d\u56fd|\u4e2d\u570b|\u53f0\u6e7e|\u53f0\u7063|\u65b0\u52a0\u5761|\u6d4b\u8bd5|\u6e2c\u8a66|\u9999\u6e2f|\ud14c\uc2a4\ud2b8|\ud55c\uad6d|xn\\-\\-0zwm56d|xn\\-\\-11b5bs3a9aj6g|xn\\-\\-3e0b707e|xn\\-\\-45brj9c|xn\\-\\-80akhbyknj4f|xn\\-\\-90a3ac|xn\\-\\-9t4b11yi5a|xn\\-\\-clchc0ea0b2g2a9gcd|xn\\-\\-deba0ad|xn\\-\\-fiqs8s|xn\\-\\-fiqz9s|xn\\-\\-fpcrj9c3d|xn\\-\\-fzc2c9e2c|xn\\-\\-g6w251d|xn\\-\\-gecrj9c|xn\\-\\-h2brj9c|xn\\-\\-hgbk6aj7f53bba|xn\\-\\-hlcj6aya9esc7a|xn\\-\\-j6w193g|xn\\-\\-jxalpdlp|xn\\-\\-kgbechtv|xn\\-\\-kprw13d|xn\\-\\-kpry57d|xn\\-\\-lgbbat1ad8j|xn\\-\\-mgbaam7a8h|xn\\-\\-mgbayh7gpa|xn\\-\\-mgbbh1a71e|xn\\-\\-mgbc0a9azcg|xn\\-\\-mgberp4a5d4ar|xn\\-\\-o3cw4h|xn\\-\\-ogbpf8fl|xn\\-\\-p1ai|xn\\-\\-pgbs0dh|xn\\-\\-s9brj9c|xn\\-\\-wgbh1c|xn\\-\\-wgbl6a|xn\\-\\-xkc2al3hye2a|xn\\-\\-xkc2dl3a5ee0h|xn\\-\\-yfro4i67o|xn\\-\\-ygbi2ammx|xn\\-\\-zckzah|xxx)"
	        + "|y[et]"
	        + "|z[amw]))"
	        + "|(?:(?:25[0-5]|2[0-4]" // or ip address
	        + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(?:25[0-5]|2[0-4][0-9]"
	        + "|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1]"
	        + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
	        + "|[1-9][0-9]|[0-9])))"
	        + "(?:\\:\\d{1,5})?)" // plus option port number
	        + "(\\/(?:(?:[a-zA-Z0-9\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\\;\\/\\?\\:\\@\\&\\=\\#\\~"  // plus option query params
	        + "\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?"
	        + "(?:\\b|$)";
	/**
	 * 中文字符的正则表达式
	 */
	public static final String CN = "^[\u4E00-\u9FA5]+$";

	/**
	 * 匹配中文，英文字母和数字及_
	 */
	public static final String CN_ENGILSH = "^[\u4E00-\u9FA5_a-zA-Z0-9]+$";

	/**
	 * 匹配中文，英文字母和数字及部分字符
	 */	
	public static final String CN_ENGILSH_PART = "^[\\u4E00-\\u9FA5a-zA-Z_0-9，,。.:;_\\-“”\\s《》()\\（\\）\\[\\]\\{\\}\\*\\#~&_-]+$";
	
	/**
	 * 会员通行证帐号的正则表达式
	 */
	public static final String MEMBER_ACCOUNT = "^[a-zA-Z0-9_]{5,20}$";

	/**
	 * 电话的正则表达式
	 */
	public static final String TEL_NUMBER = "(^[0-9]{3,4}[\\-]{0,1}[0-9]{6,9}[\\-]{0,1}[0-9]{0,5}$)";
	/**
	 * 真实姓名的正则表达式
	 */
	public static final String TRUE_NAME = "^[\u4e00-\u9fa5]{1,16}$";
	/**
	 * 联系地址的正则表达式
	 */
	public static final String LINK_ADDRESS = "^.{1,250}$";
	/**
	 * qq号码
	 */
	public static final String QQ = "^[0-9]{5,20}$";

	/**
	 * QQ互联的OpenId
	 */
	public static final String QQ_OPEN_ID = "^[a-zA-Z0-9_]{1,64}$";
	/**
	 * 微信的OpenId
	 */
	public static final String WEIXIN_OPEN_ID = "^[a-zA-Z0-9_-]{1,64}$";

	/**
	 * 邮编
	 */
	public static final String ZIP_CODE = "^[0-9]{6}$";
	/**
	 * 银行卡号
	 */
	public static final String BANK_ACCOUNT_NO = "^[0-9]{16,25}$";
	/**
	 * 两位小数
	 */
	public static final String TWO_POINT_FLOAT = "^([1-9][0-9]*|0)(\\.[0-9]{0,2})?$";
	/**
	 * 两位正整数
	 */
	public static final String TWO_BIT_INTEGER = "^[1-9]([0-9])?$";

	/**
	 * 
	 * 手机或邮箱
	 * 
	 */
	public static final String MOBLIE_OR_EMAIL = "(^1[0-9]{10}$)|(^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$)";

	/**
	 * 中文字符-标题
	 */
	public static final String TITLE_CN = "^[\\u4e00-\\u9fa5a-zA-Z_0-9]{1,100}$";

	/**
	 * 会员名称 - 姓名
	 * ^[\\u4e00-\\u9fa5a-zA-Z_0-9.\\-_\\s]{2,20}$
	 */
	public static final String MEMBER_NAME = "^[\\u4e00-\\u9fa5a-zA-Z_0-9.\\-_]{2,20}$";

	/**
	 * 企业名称
	 */
	public static final String COMPANY_NAME = "^[\\u4e00-\\u9fa5a-zA-Z_0-9，,。.:; 、_\\-“”\\s《》()\\（\\）\\[\\]\\{\\}\\*\\#\\&~!@#\\$%\\^`·￥《》<>|/]{2,20}$";

	/**
	 * 企业全称
	 */
	public static final String COMPANY_FULL_NAME = "^[\\u4e00-\\u9fa5a-zA-Z_0-9，,。.:;_\\-“”\\s《》()\\（\\）\\[\\]\\{\\}\\*\\#\\&~]{2,128}$";
		

	/**
	 * 2位正整数和2位小数
	 */
	public static final String POSITIVE_INTEGER = "^([1-9][0-9]|[1-9]|0)(\\.[0-9]{0,2})?$";

	/**
	 * 用户通行证帐号的正则表达式:由字母、数字、下划线组成，必须以字母开头
	 * 
	 * 通行证帐号支持“字母、数字、下划线，不能为11位以上纯数字”
	 * 
	 * "^(?![\\d_])\\w{4,50}$";
	 * "^([0-9]{4,10})|(?![\\d_])\\w{4,50}$"; 
	 */
	public static final String WEQIA_NO = "^([\\d_]{0,1})\\w{4,50}$"; 
	
	/**
	 * 私有云
	 * 用户通行证帐号的正则表达式:由字母、数字、下划线、点组成，必须以字母开头
	 *  "^[a-zA-Z]{1}[\\w_\\.]{1,49}$";
	 */
	public static final String G_IS_PRIVATE_WEQIA_NO = "^[\\w_\\.]{1,49}$";

	/**
	 * 企业ID的正则表达式
	 */
	public static final String CO_WEQIA_NO = "^(?![\\d_]+)\\w{6,20}$";

	/**
	 * 6位数字验证码
	 */
	public static final String VALID_NO = "^[0-9]{6}$";

	/**
	 * 正整数
	 */
	public static final String INTEGER_NUM = "^[0-9]*[1-9][0-9]*$";

	/**
	 * Long 长正整数,最大值： 9223372036854775807，这里取18位长度
	 */
	public static final String LONG_NUM = "^[0-9]{1,18}$";

	/**
	 * 加入理由，加入了，。,.
	 */
	public static final String JOIN_REASON = "^[\\u4e00-\\u9fa5a-zA-Z_0-9，,。.:;\\-_“”\\s?？《》()\\（\\）\\[\\]\\{\\}\\*\\#~]{1,64}$";
	/**
	 * 拒绝理由
	 */
	public static final String REFUSE_REASON = "^[\\u4e00-\\u9fa5a-zA-Z_0-9，,。.:;\\-_“”\\s？?《》()\\（\\）\\[\\]\\{\\}\\*\\#~]{1,64}$";
	
	/**
	 * 短号
	 */
	public static final String SHORT_CODE = "^[0-9]{3,6}$";
	
	/**
	 * 联系电话
	 * 
	 * 说明： 国家代码、 区号、 电话 号、分机号之间可以用  -、*、#、,分割    或者不添加区分符号
	 * 国际电话格式   国家长途代码(区号)(1-4位) + 国内地区长途代码(区号)(1-4位) + 电话号码(6-11位)
	 * 
	 * 国内座机   0571-88888888 / 057188888888 
	 * 
	 * 国内座机包含分机号  086-0571-88888888-123456 / 086057188888888123456
	 * 
	 * 国际固定电话  86-573-82651630 / 8657382651630
	 * 
	 * 
	 */
	public static final String CONTACT_TEL="(\\(^[0-9]{3,4}[\\-\\*#\\,]{0,1}[0-9]{6,9}[\\-\\*#\\,]{0,1}[0-9]{0,5}$)" +
			                               "|[0-9]{3,8}|(^[0-9]{1,4}[\\-\\*#\\,]{0,1}[0-9]{1,4}[\\-\\*#\\,]{0,1}[0-9]{6,11}[\\-\\*#\\,]{0,1}[0-9]{0,6}$)";
	/**
	 * 数字
	 * 
	 */
	public static final String IS_NUMER="[0-9]*";
	
	/**
	 * 数字通行证帐号
	 * 
	 */
	public static final String WEQIA_NO_NUMBER="^([0-9]{4,10})$";
	
	/**
	 * 判断实名卡号(含十六进制)
	 */
	public static final String TIME_CARD_SIXTEEN = "^[0-9a-fA-F]{8}$";
	
	/**
	 * 判断实名卡号
	 */
	public static final String TIME_CARD = "^[0-9]{8}$";
	
	public static void main(String[] args) {
		boolean b = Pattern.matches(RegExp.TIME_CARD, "12345");
		System.out.println(b);
	}
}
