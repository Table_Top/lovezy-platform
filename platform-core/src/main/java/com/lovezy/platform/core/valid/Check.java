package com.lovezy.platform.core.valid;

/**
 * 
 * 校验接口，用于自己实现校验
 *
 */
public interface Check {
	/**
	 * 校验方法
	 */
	void check();
}
