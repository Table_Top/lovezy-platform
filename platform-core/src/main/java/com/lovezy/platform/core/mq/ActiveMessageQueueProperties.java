package com.lovezy.platform.core.mq;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * MQ 配置
 * Created by jin on 2017/12/17.
 */
@ConfigurationProperties(prefix = "app.mq.activemq")
@Getter
@Setter
public class ActiveMessageQueueProperties {

    private String url;

}
