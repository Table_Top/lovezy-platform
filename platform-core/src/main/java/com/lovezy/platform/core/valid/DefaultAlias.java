package com.lovezy.platform.core.valid;

/**
 * 默认别名
 * Created by jin on 2017/11/5.
 */
public class DefaultAlias {

    public final static String INSERT = "insert";

    public final static String UPDATE = "update";
}
