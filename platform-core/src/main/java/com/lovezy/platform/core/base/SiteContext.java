package com.lovezy.platform.core.base;

import com.lovezy.platform.core.exception.DefaultErrorCode;
import com.lovezy.platform.core.exception.ServiceException;

/**
 * 网站主体
 * Created by jin on 2017/11/14.
 */
public class SiteContext {

    private static ThreadLocal<String> currentMid = new ThreadLocal<>();

    public static void setCurrentMid(String mid) {
        currentMid.set(mid);
    }

    public static String getCurrentMid() {
        String s = currentMid.get();
        if (s == null) {
            throw new ServiceException(DefaultErrorCode.SERVICE_ERROR, "用户信息不存在");
        }
        return s;
    }

    public static String getCurrentMidNullAble() {
        return currentMid.get();
    }
    public static void clearCurrentMid() {
        currentMid.remove();
    }

}
