package com.lovezy.platform.core.valid.annotation;


import com.lovezy.platform.core.valid.validators.MobileValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 手机号码校验
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@ValidatorProvider(MobileValidator.class)
public @interface Mobile {
	/**
	 * 错误码
	 * @return 错误码
	 */
	String errorCode();
	
	/**
	 * 错误信息
	 * @return 信息
	 */
	String errorMsg();

}

