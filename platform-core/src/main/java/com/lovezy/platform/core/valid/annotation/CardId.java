package com.lovezy.platform.core.valid.annotation;


import com.lovezy.platform.core.valid.validators.CardIdValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * 身份证号码校验
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@ValidatorProvider(CardIdValidator.class)
public @interface CardId {
	String errorCode();

	String errorMsg();
}
