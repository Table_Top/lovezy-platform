package com.lovezy.platform.core.email;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 邮箱对象
 * Created by jin on 2017/11/9.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EmailTarget {

    private String address;

    private String subject;

    private String text;


}
