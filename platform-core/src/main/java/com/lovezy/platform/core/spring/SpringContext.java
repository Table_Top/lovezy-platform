package com.lovezy.platform.core.spring;

import com.lovezy.platform.core.exception.DefaultErrorCode;
import com.lovezy.platform.core.exception.ServiceException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * spring context
 * Created by jin on 2017/11/9.
 */
@Component
public final class SpringContext implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        applicationContext = context;
    }

    @NotNull
    public static ApplicationContext getContext() {
        if (applicationContext == null) {
            throw new ServiceException(DefaultErrorCode.SERVICE_ERROR, "应用上下文为空");
        }
        return applicationContext;
    }


}
