package com.lovezy.platform.core.valid.annotation;


import com.lovezy.platform.core.valid.validators.NumericValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 校验数值格式
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@ValidatorProvider(NumericValidator.class)
public @interface Numeric {
	/**
	 * 错误码
	 * @return 错误码
	 */
	String errorCode();
	
	/**
	 * 错误信息
	 * @return 信息
	 */
	String errorMsg();

}
