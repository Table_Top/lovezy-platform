package com.lovezy.platform.core.base.service;

import com.lovezy.platform.core.base.mapper.BaseMapper;

/**
 * 带有mapper的的crud 服务
 * Created by jin on 2017/11/6.
 */
public abstract class CrudMapperService<T,ID,Mapper extends BaseMapper<T,ID>> extends MapperService<T,ID,Mapper>implements CrudService<T,ID>{
    @Override
    public void insert(T t) {
        mapper.insert(t);
    }

    @Override
    public void insertSelective(T t) {
        mapper.insertSelective(t);
    }

    @Override
    public T selectById(ID id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public void update(T t) {
        mapper.updateByPrimaryKey(t);
    }

    @Override
    public void updateSelective(T t) {
        mapper.updateByPrimaryKeySelective(t);
    }

    @Override
    public void delete(ID id) {
        mapper.deleteByPrimaryKey(id);
    }

}
