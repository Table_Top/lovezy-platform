package com.lovezy.platform.core.base.service;

/**
 * CRUD 接口
 * Created by jin on 2017/11/4.
 */
public interface CrudService<T,ID> {

    default void insert(T t){
        throw new UnsupportedOperationException("insert还未被实现");
    }

    default void insertSelective(T t){
        throw new UnsupportedOperationException("insertSelective还未被实现");
    }

    default T selectById(ID id){
        throw new UnsupportedOperationException("selectById还未被实现");
    }

    default void update(T t){
        throw new UnsupportedOperationException("update还未被实现");
    }

    default void updateSelective(T t){
        throw new UnsupportedOperationException("updateSelective还未被实现");
    }

    default void delete(ID id){
        throw new UnsupportedOperationException("delete还未被实现");
    }

    default void physicallyDelete(ID id){
        throw new UnsupportedOperationException("不推荐物理删除");
    }
}
