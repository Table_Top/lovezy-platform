package com.lovezy.platform.core.valid.validators;


import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.valid.Validator;
import com.lovezy.platform.core.valid.annotation.Min;

import java.lang.annotation.Annotation;

public class MinValidator implements Validator {
	@Override
	public void validate(Object value, Annotation annotation) {
		Min an = (Min) annotation;
		double d = new Double(value.toString());
		
		if (d < an.min()) {
			throw new ValidationException(an.errorCode(), an.errorMsg());
		}
	}
}
