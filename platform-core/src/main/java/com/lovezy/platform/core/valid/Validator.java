package com.lovezy.platform.core.valid;

import com.lovezy.platform.core.exception.ValidationException;

import java.lang.annotation.Annotation;

/**
 * 校验器接口
 *
 */
public interface Validator {
	/**
	 * 校验
	 * @param value 被校验的值
	 * @param annotation 注解规则
	 */
	void validate(Object value, Annotation annotation) throws ValidationException;
}
