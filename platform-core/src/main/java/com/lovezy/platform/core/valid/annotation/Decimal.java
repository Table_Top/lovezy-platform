package com.lovezy.platform.core.valid.annotation;


import com.lovezy.platform.core.valid.validators.DecimalValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Decimal 类型校验
 * Created by jin on 2017/2/7.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@ValidatorProvider(DecimalValidator.class)
public @interface Decimal {

    /**
     * 精度（字段长度）
     * @return 精度
     */
    int scale() default 15;

    /**
     * 范围（小数位数）
     * @return 范围
     */
    int precision() default 2;

    /**
     * 是否大于零 默认是
     * @return 是否
     */
    boolean greaterThanZero() default true;

    /**
     * 错误编号
     * @return 编号
     */
    String errorCode();

    /**
     * 错误信息
     * @return 信息
     */
    String errorMsg();
}
