package com.lovezy.platform.core.valid.validators;


import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.valid.Validator;
import com.lovezy.platform.core.valid.annotation.Expression;

import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

/**
 *
 * 正则表达式校验
 *
 */
public class ExpressionValidator implements Validator {

	@Override
	public void validate(Object value, Annotation annotation) {
		Expression exp = (Expression) annotation;
		if (!Pattern.matches(exp.expression(), value.toString())) {
			throw new ValidationException(exp.errorCode(), exp.errorMsg());
		}
	}
}
