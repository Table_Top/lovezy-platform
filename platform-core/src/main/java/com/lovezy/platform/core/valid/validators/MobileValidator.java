package com.lovezy.platform.core.valid.validators;


import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.valid.RegExp;
import com.lovezy.platform.core.valid.Validator;
import com.lovezy.platform.core.valid.annotation.Mobile;

import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

public class MobileValidator implements Validator {
	public void validate(Object value, Annotation annotation) {
		Mobile an = (Mobile) annotation;
		if (!Pattern.matches(RegExp.MOBILE_STRICT, value.toString())) {
			throw new ValidationException(an.errorCode(), an.errorMsg());
		}
	}
}
