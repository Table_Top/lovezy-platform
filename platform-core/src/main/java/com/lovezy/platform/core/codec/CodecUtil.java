package com.lovezy.platform.core.codec;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Random;
import java.util.UUID;

/**
 * MD5生成器
 * Created by jin on 2017/11/5.
 */
public final class CodecUtil {
    private CodecUtil() {

    }
    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String randomDigit(int length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    public static String md5(String source) {
        return DigestUtils.md5Hex(source);
    }

}
