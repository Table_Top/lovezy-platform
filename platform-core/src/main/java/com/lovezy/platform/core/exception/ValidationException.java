package com.lovezy.platform.core.exception;

/**
 * 校验异常
 * Created by jin on 2017/11/4.
 */
public class ValidationException extends BaseRuntimeException{
    public ValidationException(String errorCode,String message) {
        super(errorCode,message);
    }

    public ValidationException(ErrorMessageAware error) {
        super(error.errorCode(),error.errorMsg());
    }
}
