package com.lovezy.platform.core.valid.validators;


import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.valid.RegExp;
import com.lovezy.platform.core.valid.Validator;
import com.lovezy.platform.core.valid.annotation.Email;

import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

/**
 * 
 * 邮箱校验器
 *
 */
public class EmailValidator implements Validator {
	public void validate(Object value, Annotation annotation) {
		Email an = (Email) annotation;
		if (!Pattern.matches(RegExp.EMAIL, value.toString())) {
			throw new ValidationException(an.errorCode(), an.errorMsg());
		}
	}
}
