package com.lovezy.platform.shop.enums;

/**
 * Created by jin on 2017/12/7.
 */
public enum ShopExceptionEnum {
    SHOP_KEY_IS_EXISTS ("000001","商铺号已存在"),
    SHOP_KEY_NOT_FOUND ("000002","商铺号不存在")
    ;
    String errorCode;
    String errorMsg;
    ShopExceptionEnum(String errorCode, String errorMsg) {
        this.errorCode = "SHOP-" + errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
