package com.lovezy.platform.shop.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/11/19.
 */
@Getter
@Setter
@ToString
public class ShopVo {
    /**
     * 主键
     */
    private Integer shopId;

    /**
     * 用户ID
     */
    private String memberId;

    /**
     * 名字
     */
    private String name;

    /**
     * 地址
     */
    private String address;

    /**
     * 联系方式
     */
    private String tel;

    /**
     * 店铺号
     */
    private String shopKey;
}
