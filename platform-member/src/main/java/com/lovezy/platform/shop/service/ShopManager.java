package com.lovezy.platform.shop.service;

import com.lovezy.platform.shop.model.Shop;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 商铺管理员
 * Created by jin on 2017/11/20.
 */
@Getter
@ToString
@AllArgsConstructor
public class ShopManager {

    private String memberId;

    private Integer shopId;

    private Shop shop;

}
