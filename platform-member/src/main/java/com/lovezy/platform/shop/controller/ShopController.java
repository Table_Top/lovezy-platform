package com.lovezy.platform.shop.controller;

import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.shop.model.Shop;
import com.lovezy.platform.shop.service.ShopService;
import com.lovezy.platform.shop.vo.ShopVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.utils.ObjectUtil.copyTo;

/**
 * Created by jin on 2017/11/19.
 */
@RestController
@RequestMapping("/api/shop")
public class ShopController {

    @Autowired
    private ShopService shopService;

    @GetMapping("/myShop")
    public ResponseEntity<Response> findShop() {
        String mid = SiteContext.getCurrentMid();
        Shop shop = shopService.getByMemberId(mid);
        ShopVo shopVo = copyTo(shop, new ShopVo());
        return new ResponseEntity<>(Response.ok(shopVo), HttpStatus.CREATED);
    }

}
