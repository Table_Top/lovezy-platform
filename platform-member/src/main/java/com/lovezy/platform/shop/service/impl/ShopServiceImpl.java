package com.lovezy.platform.shop.service.impl;

import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.member.form.ShopRegisterForm;
import com.lovezy.platform.member.model.Member;
import com.lovezy.platform.shop.mapper.ShopMapper;
import com.lovezy.platform.shop.model.Shop;
import com.lovezy.platform.shop.model.ShopExample;
import com.lovezy.platform.shop.service.ShopManager;
import com.lovezy.platform.shop.service.ShopService;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.Optional;

import static com.lovezy.platform.shop.enums.ShopExceptionEnum.SHOP_KEY_IS_EXISTS;

/**
 * 商户服务实现
 * Created by jin on 2017/11/17.
 */
@Service
public class ShopServiceImpl extends MapperService<Shop,Integer,ShopMapper> implements ShopService{

    @Override
    public Shop getById(@NotNull Integer id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public Shop getByMemberId(@NotNull String mid) {
        ShopExample example = new ShopExample();
        example.createCriteria()
                .andMemberIdEqualTo(mid)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        return Pager.doSingletonNullAble(() -> mapper.selectByExample(example));
    }


    @Override
    public Shop addShop(Member m, ShopRegisterForm form) {
        String shopKey = form.getShopKey();
        if (StringUtils.hasText(shopKey)) {
            checkShopKey(shopKey);
        }

        Shop shop = new Shop();
        Date now = new Date();
        String mId = m.getMemberId();
        shop.setMemberId(mId);
        shop.setName(form.getName());
        shop.setAddress(form.getAddress());
        shop.setTel(form.getTel());
        shop.setCreateId(mId);
        shop.setModifyId(mId);
        shop.setStatus(StatusEnum.NORMAL.value());
        shop.setGmtCreate(now);
        shop.setGmtModify(now);
        shop.setShopKey(shopKey);
        mapper.insert(shop);
        return shop;
    }

    private synchronized void checkShopKey(String key) {
        ShopExample example = new ShopExample();
        example.createCriteria().andShopKeyEqualTo(key)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        int count = mapper.countByExample(example);
        if (count > 0) {
            throw new ServiceException(SHOP_KEY_IS_EXISTS.getErrorCode(), SHOP_KEY_IS_EXISTS.getErrorMsg());
        }
    }

    @Override
    public Optional<Shop> findByShopKey(String key) {
        ShopExample example = new ShopExample();
        example.createCriteria()
                .andShopKeyEqualTo(key)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        return Pager.doSingleton(() -> mapper.selectByExample(example));
    }

    @Override
    public Optional<ShopManager> isShopManager(Integer shopId, String mid) {
        Shop holder = new Shop();
        ShopExample example = new ShopExample();
        example.createCriteria()
                .andShopIdEqualTo(shopId)
                .andMemberIdEqualTo(mid)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        Optional<Shop> o = Pager.doSingleton(() -> mapper.selectByExample(example));
        if (o.isPresent()) {
            return Optional.of(new ShopManager(mid, shopId, holder));
        }else{
            return Optional.empty();
        }
    }




}
