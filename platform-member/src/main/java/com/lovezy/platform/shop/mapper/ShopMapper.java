package com.lovezy.platform.shop.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.shop.model.Shop;
import com.lovezy.platform.shop.model.ShopExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShopMapper extends BaseExampleMapper<Shop, ShopExample, Integer> {

}