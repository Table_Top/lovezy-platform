package com.lovezy.platform.shop.service;

import com.lovezy.platform.member.form.ShopRegisterForm;
import com.lovezy.platform.member.model.Member;
import com.lovezy.platform.shop.model.Shop;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * 商铺服务接口
 * Created by jin on 2017/11/17.
 */
public interface ShopService {
    Shop getById(@NotNull Integer id);

    Shop getByMemberId(@NotNull String mid);

    Shop addShop(Member m, ShopRegisterForm form);

    Optional<Shop> findByShopKey(String key);

    Optional<ShopManager> isShopManager(Integer shopId, String mid);
}
