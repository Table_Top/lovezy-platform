package com.lovezy.platform.member.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.member.model.MemberRole;
import com.lovezy.platform.member.model.MemberRoleExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MemberRoleMapper extends BaseExampleMapper<MemberRole, MemberRoleExample, String> {

}