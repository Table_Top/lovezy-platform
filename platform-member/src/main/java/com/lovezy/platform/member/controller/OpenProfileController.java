package com.lovezy.platform.member.controller;

import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.member.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 用户信息控制器
 * Created by jin on 2017/11/29.
 */
@RestController
@RequestMapping("/api/open/profile")
public class OpenProfileController {

    @Autowired
    private ProfileService profileService;

    @PutMapping("/password/sms")
    public ResponseEntity<Response> editPasswordBySms(@RequestBody Map<String, String> param) {
        String code = param.get("code");
        String password = param.get("password");

        profileService.editPassword(password, code);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/password/forget")
    public ResponseEntity<Response> forgetPassword(@RequestBody Map<String, String> param) {
        String mid = SiteContext.getCurrentMidNullAble();
        String mobile = param.get("mobile");
        profileService.sendEditPasswordSms(mid, mobile);
        return ResponseEntity.ok(ok());

    }
}
