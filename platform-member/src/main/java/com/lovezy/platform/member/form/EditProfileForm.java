package com.lovezy.platform.member.form;

import com.lovezy.platform.core.valid.annotation.Alias;
import com.lovezy.platform.core.valid.annotation.Length;
import com.lovezy.platform.core.valid.annotation.Min;
import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 编辑表单
 * Created by jin on 2017/11/5.
 */
@Getter
@Setter
@ToString
public class EditProfileForm {

    public static final String EDIT_PASSWORD = "password";
    public static final String EDIT_AVATAR = "avatar";
    public static final String EDIT_NICKNAME = "nickname";

    @Alias({EDIT_PASSWORD,EDIT_AVATAR,EDIT_NICKNAME})
    @Required(errorMsg = "用户ID不能为空")
    private String memberId;

    @Alias({EDIT_PASSWORD})
    @Required(errorMsg = "密码不能为空")
    private String newPassword;

    private String oldPassword;

    @Alias({EDIT_AVATAR})
    @Required(errorMsg = "头像不能为空")
    private String avatar;

    @Alias({EDIT_NICKNAME})
    @Required(errorMsg = "昵称不能为空")
    @Length(maxLength = 16, errorMsg = "昵称过长")
    private String nickname;


}

