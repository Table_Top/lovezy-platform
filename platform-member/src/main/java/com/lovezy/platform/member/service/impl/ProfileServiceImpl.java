package com.lovezy.platform.member.service.impl;

import com.lovezy.platform.common.extend.ExtendService;
import com.lovezy.platform.common.prop.Constant;
import com.lovezy.platform.core.codec.CodecUtil;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.jwt.JwtUtil;
import com.lovezy.platform.core.mq.MessageProducer;
import com.lovezy.platform.member.enums.MemberRoleEnum;
import com.lovezy.platform.member.exception.MemberExceptionEnum;
import com.lovezy.platform.member.form.EditProfileForm;
import com.lovezy.platform.member.model.Member;
import com.lovezy.platform.member.service.MemberService;
import com.lovezy.platform.member.service.ProfileService;
import com.lovezy.platform.member.vo.MemberRoleVo;
import com.lovezy.platform.message.mobile.enums.SmsIllegalEnum;
import com.lovezy.platform.message.mobile.enums.SmsTypeEnum;
import com.lovezy.platform.message.mobile.model.Mobile;
import com.lovezy.platform.message.mobile.service.SmsSenderService;
import com.lovezy.platform.message.mobile.service.SmsServiceTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.lovezy.platform.core.utils.ObjectUtil.nonNull;

/**
 * 个人信息服务实现
 * Created by jin on 2017/11/29.
 */
@Service
public class ProfileServiceImpl implements ProfileService{

    @Autowired
    private MemberService memberService;

    @Autowired
    private ExtendService extendService;

    @Autowired
    SmsSenderService smsSenderService;

    @Autowired @Qualifier("smsMessageProducer")
    MessageProducer messageProducer;

    @Override
    public String changePassword(EditProfileForm form) {
        memberService.editPassword(form);
        String memberId = form.getMemberId();
        String tokenId =  extendService.modifyToken(memberId);
        return JwtUtil.createToken(memberId, tokenId);
    }

    @Override
    public void changeAvatar(String newAvatarUrl, String mid) {
        EditProfileForm form = new EditProfileForm();
        form.setAvatar(newAvatarUrl);
        form.setMemberId(mid);
        memberService.editAvatar(form);
    }

    @Override
    public void changeNickname(String nickname, String mid) {
        EditProfileForm form = new EditProfileForm();
        form.setNickname(nickname);
        form.setMemberId(mid);
        memberService.editNickname(form);
    }

    @Override
    public MemberRoleVo findMemberRole(String mid) {
        MemberRoleVo role = new MemberRoleVo();
        Member member = memberService.findByMemberId(mid);
        Byte mRole = member.getRole();
        if (mRole == MemberRoleEnum.ADMIN.value()) {
            role.setAdmin(true);
        }
        if (mRole == MemberRoleEnum.BUSINESS.value()) {
            role.setShop(true);
        }
        return role;
    }

    @Override
    //TODO 优化发送短信逻辑代码
    public void sendEditPasswordSms(String mid, String mobile) {
        nonNull(mobile);
        SmsServiceTarget target = new SmsServiceTarget();
        target.setMobile(mobile);
        String authKey = CodecUtil.randomDigit(6);
        target.setText(authKey);
        target.setMemberId(mid);
        target.setTemplateCode(Constant.REGISTER_TEMPLATE_CODE);
        target.setAuthKey(authKey);
        target.setType(SmsTypeEnum.EDIT_PASSWORD);
        messageProducer.send(target);
    }

    @Override
    public void editPassword(String password, String code) {
        smsSenderService.verifyMobile(code,SmsTypeEnum.EDIT_PASSWORD,v -> {
            if (v.isSuccess()) {
                Mobile mobile = v.getMobile();
                Optional<Member> mem = memberService.findByMobile(mobile.getMobileNumber());
                if (!mem.isPresent()) {
                    throw new ServiceException(MemberExceptionEnum.MEMBER_NOT_FOUND.getErrorCode(),
                            MemberExceptionEnum.MEMBER_NOT_FOUND.getErrorMsg());
                }
                memberService.resetPassword(mem.get().getMemberId(),password);
                return true;
            }else{
                SmsIllegalEnum illegal = v.getIllegal();
                throw new ServiceException(illegal.errorMsg());
            }
        });
    }
}
