package com.lovezy.platform.member.form;


import com.lovezy.platform.core.valid.annotation.Length;
import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 商户注册表单
 * Created by jin on 2017/11/5.
 */
@Getter
@Setter
@ToString
public class ShopRegisterForm {

    @Required(errorMsg = "密码不能为空")
    private String password;

    @Required(errorMsg = "用户名不能为空")
    @Length(maxLength = 16, errorMsg = "用户名过长")
    private String username;

    @Length(maxLength = 512,errorMsg = "地址过长")
    private String address;

    @Length(maxLength = 128,errorMsg = "联系方式过长")
    private String tel;

    private String email;

    private String mobile;

    @Required(errorMsg = "店铺名字不能为空")
    @Length(maxLength = 256,errorMsg = "店铺名字过长")
    private String name;

//    @Required(errorMsg = "店铺号不能为空")
    @Length(maxLength = 64,errorMsg = "店铺号过长")
    private String shopKey;

}

