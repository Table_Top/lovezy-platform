package com.lovezy.platform.member.service.impl;

import com.lovezy.platform.core.base.enums.TrueOrFalseEnum;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.member.mapper.MemberRoleMapper;
import com.lovezy.platform.member.model.MemberRole;
import com.lovezy.platform.member.service.MemberRoleService;
import com.lovezy.platform.member.vo.MemberRoleVo;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 用户权限服务实现
 * Created by jin on 2018/1/23.
 */
@Service
public class MemberRoleServiceImpl extends MapperService<MemberRole,String,MemberRoleMapper> implements MemberRoleService{

    @Override
    public MemberRoleVo findMemberRole(String mid) {
        MemberRole role = getOrCreate(mid);
        MemberRoleVo roleVo = new MemberRoleVo();
        roleVo.setAdmin(isEnable(role.getSuperAdmin()));
        roleVo.setShop(isEnable(role.getShopManager()));
        return roleVo;
    }

    private boolean isEnable(Byte value) {
        return value != null && value.equals(TrueOrFalseEnum.TRUE.value());
    }

    private synchronized MemberRole getOrCreate(String mid) {
        MemberRole memberRole = mapper.selectByPrimaryKey(mid);
        if (memberRole == null) {
            memberRole = new MemberRole();
            memberRole.setMemberId(mid);
            Date date = new Date();
            memberRole.setGmtCreate(date);
            memberRole.setGmtModify(date);
            mapper.insert(memberRole);
        }
        return memberRole;
    }
}
