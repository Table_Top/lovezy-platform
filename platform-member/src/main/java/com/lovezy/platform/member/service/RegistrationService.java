package com.lovezy.platform.member.service;

import com.lovezy.platform.member.form.RegisterForm;
import com.lovezy.platform.member.form.ShopRegisterForm;
import com.lovezy.platform.member.vo.MemberVo;
import com.lovezy.platform.member.vo.ShopMemberVo;
import org.springframework.transaction.annotation.Transactional;

/**
 * 注册服务
 * Created by jin on 2017/11/9.
 */
public interface RegistrationService {
    void sendAuthKey(String address);

    @Transactional
    MemberVo regMember(RegisterForm form);

    ShopMemberVo regShopMember(ShopRegisterForm form);
}
