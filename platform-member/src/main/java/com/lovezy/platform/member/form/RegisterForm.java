package com.lovezy.platform.member.form;


import com.lovezy.platform.core.valid.annotation.Max;
import com.lovezy.platform.core.valid.annotation.Min;
import com.lovezy.platform.core.valid.annotation.Range;
import com.lovezy.platform.core.valid.annotation.Required;
import com.lovezy.platform.member.enums.MemberRoleEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 注册表单
 * Created by jin on 2017/11/5.
 */
@Getter
@Setter
@ToString
public class RegisterForm {

    @Required(errorMsg = "密码不能为空")
    private String password;

    @Required(errorMsg = "昵称不能为空")
    @Max(max = 16, errorMsg = "昵称过长")
    @Min(min = 4, errorMsg = "昵称过短")
    private String nickname;

    private String username;

    private String avatar;

    @Range(errorMsg = "性别格式错误")
    private Byte sex;

    @Required(errorMsg = "信息校验错误")
    private String authKey;

    @Range(errorMsg = "用户角色类型错误")
    private Byte role = MemberRoleEnum.NORMAL_MEMBER.value();

}

