package com.lovezy.platform.member.service;

import com.lovezy.platform.member.form.EditProfileForm;
import com.lovezy.platform.member.vo.MemberRoleVo;

/**
 * 个人信息服务
 * Created by jin on 2017/11/29.
 */
public interface ProfileService {
    String changePassword(EditProfileForm form);

    void changeAvatar(String newAvatarUrl, String mid);

    void changeNickname(String nickname, String mid);

    MemberRoleVo findMemberRole(String mid);

    void sendEditPasswordSms(String mid, String mobile);

    void editPassword(String password, String code);
}
