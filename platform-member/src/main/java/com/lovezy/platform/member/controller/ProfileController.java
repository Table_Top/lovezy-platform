package com.lovezy.platform.member.controller;

import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.common.extend.ExtendService;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.member.form.EditProfileForm;
import com.lovezy.platform.member.service.MemberRoleService;
import com.lovezy.platform.member.service.ProfileService;
import com.lovezy.platform.member.vo.MemberRoleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 用户信息控制器
 * Created by jin on 2017/11/29.
 */
@RestController
@RequestMapping("/api/profile")
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @Autowired
    private ExtendService extendService;

    @Autowired
    MemberRoleService memberRoleService;


    @PostMapping("/password")
    public ResponseEntity<Response> newPassword(@RequestBody EditProfileForm form) {
        form.setMemberId(SiteContext.getCurrentMid());
        String token = profileService.changePassword(form);
        return ResponseEntity.ok(ok(ImmutableMap.of("token", token)));
    }

    @PutMapping("/avatar")
    public ResponseEntity<Response> avatar(@RequestBody Map<String, String> param) {
        profileService.changeAvatar(param.get("url"), SiteContext.getCurrentMid());
        return ResponseEntity.ok(ok());
    }

    @PutMapping("/nickname")
    public ResponseEntity<Response> nickname(@RequestBody Map<String, String> param) {
        profileService.changeNickname(param.get("nickname"), SiteContext.getCurrentMid());
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/initPush")
    public ResponseEntity<Response> initPush(@RequestBody Map<String, String> param) {
        extendService.registerDevice(SiteContext.getCurrentMid(), param.get("clientId"));
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/role")
    public ResponseEntity<Response> role() {
        String mid = SiteContext.getCurrentMid();
        MemberRoleVo memberRole = memberRoleService.findMemberRole(mid);
        return ResponseEntity.ok(ok(memberRole));
    }

    @PutMapping("/password/sms")
    public ResponseEntity<Response> editPasswordBySms(@RequestBody Map<String, String> param) {
        String code = param.get("code");
        String password = param.get("password");

        profileService.editPassword(password, code);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/password/forget")
    public ResponseEntity<Response> forgetPassword(@RequestBody Map<String, String> param) {
        String mid = SiteContext.getCurrentMidNullAble();
        String mobile = param.get("mobile");
        profileService.sendEditPasswordSms(mid, mobile);
        return ResponseEntity.ok(ok());

    }
}
