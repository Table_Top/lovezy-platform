package com.lovezy.platform.member.service;

import com.lovezy.platform.core.base.enums.FrontEnum;
import com.lovezy.platform.member.form.EditProfileForm;
import com.lovezy.platform.member.model.Member;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * 用户服务接口
 * Created by jin on 2017/11/5.
 */
public interface MemberService {
    Optional<Member> findDiscuzMember(Integer dzId);

    Member addMember(Member member);

    Member addMemberForOtherSystem(Member member, Consumer<Member> beforeSave);

    Member findByMemberId(String mid);

    List<String> findNames(List<String> mids);

    Optional<Member> findByMobile(@NotNull String mobile);

    Member findByUserNameAndPassword(String loginName, String password);

    void editPassword(@NotNull EditProfileForm form);

    void resetPassword(String mid, String password);

    void editAvatar(EditProfileForm form);

    void editNickname(EditProfileForm form);

    void tokenInvalid(String memberId);

    Optional<Member> findMemberByToken(String token, FrontEnum front);
}
