package com.lovezy.platform.member.service;

import com.lovezy.platform.member.form.LoginForm;
import com.lovezy.platform.member.form.SmsLoginForm;
import org.jetbrains.annotations.NotNull;

/**
 * 登陆服务
 * Created by jin on 2017/11/5.
 */
public interface LoginService {
    LoginResponse login(LoginForm loginForm);

    LoginResponse loginByDz(LoginForm loginForm);

    void sendLoginSms(@NotNull String mobile);

    LoginResponse loginByMobileSms(SmsLoginForm loginForm);

    String refreshToken(String mid);
}
