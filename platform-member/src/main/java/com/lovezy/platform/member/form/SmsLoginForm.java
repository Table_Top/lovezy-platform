package com.lovezy.platform.member.form;

import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 短信登陆表单
 * Created by jin on 2017/11/29.
 */
@Getter
@Setter
@ToString
public class SmsLoginForm {

    @Required(errorMsg = "手机号不能为空")
    private String mobile;

    @Required(errorMsg = "验证码不能为空")
    private String code;

}
