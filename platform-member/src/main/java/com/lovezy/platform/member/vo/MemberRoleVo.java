package com.lovezy.platform.member.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户权限
 * Created by jin on 2018/1/2.
 */
@Getter
@Setter
public class MemberRoleVo {

    private boolean shop = false;

    private boolean admin = false;

}
