package com.lovezy.platform.member.common;

import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.member.exception.MemberExceptionEnum;

/**
 * 用户模块异常生成器
 * Created by jin on 2017/11/5.
 */
public class MemberServiceExceptionBuilder {

    public static void validException(MemberExceptionEnum exceptionEnum) throws ValidationException{
        throw new ValidationException(exceptionEnum.getErrorCode(), exceptionEnum.getErrorMsg());
    }

    public static void validException(MemberExceptionEnum exceptionEnum,String msg) throws ValidationException{
        throw new ValidationException(exceptionEnum.getErrorCode(), msg);
    }

    public static void serviceException(MemberExceptionEnum exceptionEnum) throws ServiceException{
        throw new ServiceException(exceptionEnum.getErrorCode(), exceptionEnum.getErrorMsg());
    }

    public static void serviceException(MemberExceptionEnum exceptionEnum,String msg) throws ServiceException{
        throw new ServiceException(exceptionEnum.getErrorCode(), msg);
    }

}
