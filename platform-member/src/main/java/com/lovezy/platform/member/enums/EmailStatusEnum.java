package com.lovezy.platform.member.enums;

/**
 * 邮箱状态枚举 1-未认证 2-已认证
 * Created by jin on 2017/11/5.
 */
public enum EmailStatusEnum {
    UN_AUTHENTICATED((byte)1),
    AUTHENTICATED((byte)2)
    ;

    byte value;

    EmailStatusEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
