package com.lovezy.platform.member.form;

import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 修改密码表单
 * Created by jin on 2017/11/29.
 */
@Getter
@Setter
@ToString
public class ChangePasswordForm {

    @Required(errorMsg = "原密码不能为空")
    private String oldPassword;

    @Required(errorMsg = "新密码不能为空")
    private String newPassword;

    private String optId;
}
