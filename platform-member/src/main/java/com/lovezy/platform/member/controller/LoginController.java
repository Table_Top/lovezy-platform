package com.lovezy.platform.member.controller;

import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.member.form.LoginForm;
import com.lovezy.platform.member.form.SmsLoginForm;
import com.lovezy.platform.member.service.LoginResponse;
import com.lovezy.platform.member.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 登陆相关
 * Created by jin on 2017/11/5.
 */
@RequestMapping("/api/login")
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping({"/site","/mobile"})
    public ResponseEntity<Response> login(@RequestBody LoginForm loginForm) {
        LoginResponse response = loginService.login(loginForm);
        return ResponseEntity.ok(ok(response));
    }

    @PostMapping("/discuz")
    public ResponseEntity<Response> loginDz(@RequestBody LoginForm loginForm) {
        LoginResponse response = loginService.loginByDz(loginForm);
        return ResponseEntity.ok(ok(response));
    }

    @PostMapping("/msgCode")
    public ResponseEntity<Response> loginSms(@RequestBody SmsLoginForm loginForm) {
        LoginResponse response = loginService.loginByMobileSms(loginForm);
        return ResponseEntity.ok(ok(response));
    }

    @PostMapping("/mobileVerification")
    public ResponseEntity<Response> applySmsForLogin(@RequestBody Map<String,String> params) {
        loginService.sendLoginSms(params.get("mobile"));
        return ResponseEntity.ok(ok());
    }
}
