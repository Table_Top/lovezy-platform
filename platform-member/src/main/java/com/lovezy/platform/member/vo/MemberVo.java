package com.lovezy.platform.member.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 用户VO类
 * Created by jin on 2017/11/5.
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberVo {

    private String memberId;

    /**
     * 头像
     */
    private Integer avatar;

    /**
     * 用户号
     */
    private Integer memberNo;

    /**
     * 登陆名称
     */
    private String username;

    /**
     * 用户名称
     */
    private String nickname;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱验证状态 1-未认证 2-已认证
     */
    private Byte emailStatus;

    /**
     * 性别 1-男 2-女
     */
    private Byte sex;

    /**
     * 网页token
     */
    private String token;

    /**
     * 角色 1-普通 2-管理员
     */
    private Byte role;

    /**
     * 客户端token
     */
    private String mobileToken;
}
