package com.lovezy.platform.member.controller;

import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.member.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2017/12/27.
 */
@RestController
@RequestMapping("/api/token")
public class TokenController {

    @Autowired
    LoginService loginService;

    @PostMapping("/refresh")
    public ResponseEntity<Response> refresh() {
        String mid = SiteContext.getCurrentMid();
        String refreshToken = loginService.refreshToken(mid);
        return ResponseEntity.ok(ok(ImmutableMap.of("token", refreshToken)));
    }

}
