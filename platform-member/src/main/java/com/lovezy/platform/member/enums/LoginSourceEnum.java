package com.lovezy.platform.member.enums;

/**
 * 登陆来源 1-网站 2-论坛 3-QQ
 * Created by jin on 2017/11/5.
 */
public enum LoginSourceEnum {
    SITE((byte)1),
    DISCUZ((byte)2),
    QQ((byte)3)
    ;
    byte value;

    LoginSourceEnum(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }
}
