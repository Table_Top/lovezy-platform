package com.lovezy.platform.member.exception;

/**
 * 用户信息模块异常
 * Created by jin on 2017/11/5.
 */
public enum ProfileExceptionEnum {
    VALID_OLD_PASSWORD_ERROR ("000001","原密码不正确"),
    ;
    String errorCode;
    String errorMsg;
    ProfileExceptionEnum(String errorCode, String errorMsg) {
        this.errorCode = "PROFILE-" + errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
