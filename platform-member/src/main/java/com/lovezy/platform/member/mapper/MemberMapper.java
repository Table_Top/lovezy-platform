package com.lovezy.platform.member.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.member.model.Member;
import com.lovezy.platform.member.model.MemberExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MemberMapper extends BaseExampleMapper<Member, MemberExample, String> {

}