package com.lovezy.platform.member.form;

import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/** 登陆表单
 * Created by jin on 2017/11/5.
 */
@Getter
@Setter
@ToString
public class LoginForm {

    @Required(errorMsg = "登录名不能为空")
    private String loginName;

    @Required(errorMsg = "密码不能为空")
    private String password;

    private String checkCode;

}
