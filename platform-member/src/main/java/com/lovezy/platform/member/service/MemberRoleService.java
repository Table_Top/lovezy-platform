package com.lovezy.platform.member.service;

import com.lovezy.platform.member.vo.MemberRoleVo;

/**
 * 用户权限服务
 * Created by jin on 2018/1/23.
 */
public interface MemberRoleService {
    MemberRoleVo findMemberRole(String mid);
}
