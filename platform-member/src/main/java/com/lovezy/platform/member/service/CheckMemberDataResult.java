package com.lovezy.platform.member.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 检查用户数据是否合法结果
 * Created by jin on 2017/11/5.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CheckMemberDataResult {

    public final static CheckMemberDataResult SUCCESS = new CheckMemberDataResult();

    private boolean result = true;

    private String msg;
}
