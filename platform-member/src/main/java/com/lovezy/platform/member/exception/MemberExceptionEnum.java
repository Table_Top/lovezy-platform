package com.lovezy.platform.member.exception;

/**
 * 用户模块异常
 * Created by jin on 2017/11/5.
 */
public enum MemberExceptionEnum {
    VALID_MEMBER_INFO_IS_EXISTS ("000001","校验用户失败"),
    MEMBER_NOT_FOUND    ("000002","用户不存在"),
    MEMBER_PASSWORD_WRONG ("000003","用户密码错误"),
    OLD_PASSWORD_WRONG  ("000004","用户原密码错误"),
    PHONE_ERROR  ("000005","手机格式错误"),
    PHONE_MEMBER_NOT_EXISTS  ("000006","此手机号还未注册"),
    MEMBER_ROLE_NOT_BUSINESS  ("000007","此用户不为商户"),
    MEMBER_ROLE_LIMIT  ("000008","用户权限不足"),
    ;
    String errorCode;
    String errorMsg;
    MemberExceptionEnum(String errorCode, String errorMsg) {
        this.errorCode = "MEMBER-" + errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
