package com.lovezy.platform.member.service.impl;

import com.lovezy.platform.common.prop.Constant;
import com.lovezy.platform.core.codec.CodecUtil;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.exception.ValidationException;
import com.lovezy.platform.core.mq.MessageProducer;
import com.lovezy.platform.core.valid.RegExp;
import com.lovezy.platform.member.form.RegisterForm;
import com.lovezy.platform.member.form.ShopRegisterForm;
import com.lovezy.platform.member.model.Member;
import com.lovezy.platform.member.service.MemberService;
import com.lovezy.platform.member.service.RegistrationService;
import com.lovezy.platform.member.vo.MemberVo;
import com.lovezy.platform.member.vo.ShopMemberVo;
import com.lovezy.platform.message.mobile.enums.SmsIllegalEnum;
import com.lovezy.platform.message.mobile.enums.SmsTypeEnum;
import com.lovezy.platform.message.mobile.exception.IllegalMobileException;
import com.lovezy.platform.message.mobile.service.SmsSenderService;
import com.lovezy.platform.message.mobile.service.SmsServiceTarget;
import com.lovezy.platform.message.mobile.service.VerifyMobileResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.regex.Pattern;

import static com.lovezy.platform.core.utils.ObjectUtil.copyTo;
import static com.lovezy.platform.member.exception.MemberExceptionEnum.PHONE_ERROR;

/**
 * Created by jin on 2017/11/29.
 */
@Service("mobileRegistrationService")
public class MobileRegistrationServiceImpl implements RegistrationService{

    @Autowired
    private SmsSenderService smsSenderService;

    @Autowired
    private MemberService memberService;

    @Autowired
    @Qualifier("smsMessageProducer")
    private MessageProducer smsMessageProducer;

    @Override
    public void sendAuthKey(String phone) {
        boolean isMatch = Pattern.matches(RegExp.MOBILE, phone);
        if (!isMatch) {
            throw new ValidationException(PHONE_ERROR.getErrorCode(), PHONE_ERROR.getErrorMsg());
        }

        Optional<Member> memberOptional = memberService.findByMobile(phone);
        if (memberOptional.isPresent()) {
            throw new ServiceException("该手机号已注册");
        }

        SmsServiceTarget target = new SmsServiceTarget();
        target.setMobile(phone);
        String authKey = CodecUtil.randomDigit(6);
        target.setText(authKey);
        target.setTemplateCode(Constant.REGISTER_TEMPLATE_CODE);
        target.setAuthKey(authKey);
        target.setType(SmsTypeEnum.MEMBER_REGISTER);
        smsMessageProducer.send(target);
//        smsSenderService.sendSms(target);
    }

    @Override
    @Transactional
    public MemberVo regMember(RegisterForm form) {
        Member m = copyTo(form, new Member());
        String authKey = form.getAuthKey();
        smsSenderService.verifyMobile(authKey,SmsTypeEnum.MEMBER_REGISTER, r -> resultHandler(r,m));
        return copyTo(m, new MemberVo());
    }

    private boolean resultHandler(VerifyMobileResult result, Member m) {
        if (result.isSuccess()) {
            String mobileNumber = result.getMobile().getMobileNumber();
            m.setMobile(mobileNumber);
            m.setNickname(mobileNumber);
            memberService.addMember(m);
            return true;
        }else{
            SmsIllegalEnum illegal = result.getIllegal();
            throw new IllegalMobileException(illegal);
        }
    }

    @Override
    public ShopMemberVo regShopMember(ShopRegisterForm form) {
        return null;
    }
}
