package com.lovezy.platform.member.enums;

/**
 * 用户角色枚举 -1-管理员 1-普通用户 2-商户
 * Created by jin on 2017/11/9.
 */
public enum MemberRoleEnum {
    ADMIN((byte)-1),
    NORMAL_MEMBER((byte)1),
    BUSINESS((byte)2);

    byte value;

    MemberRoleEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
