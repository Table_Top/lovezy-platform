package com.lovezy.platform.member.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 商铺用户VO
 * Created by jin on 2017/11/17.
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class ShopMemberVo {

    @JsonUnwrapped
    private MemberVo memberVo;

    private String address;

    private String tel;

}
