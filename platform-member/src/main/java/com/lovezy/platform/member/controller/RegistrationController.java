package com.lovezy.platform.member.controller;

import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.utils.JsonUtil;
import com.lovezy.platform.member.form.RegisterForm;
import com.lovezy.platform.member.form.ShopRegisterForm;
import com.lovezy.platform.member.service.RegistrationService;
import com.lovezy.platform.member.vo.MemberVo;
import com.lovezy.platform.member.vo.ShopMemberVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 注册
 * Created by jin on 2017/11/9.
 */
@RestController
@RequestMapping("/api/reg")
@Slf4j
public class RegistrationController {

    @Autowired
    @Qualifier("emailRegistrationService")
    private RegistrationService emailRegistrationService;

    @Autowired
    @Qualifier("mobileRegistrationService")
    private RegistrationService mobileRegistrationService;

    @PostMapping("/applyEmail")
    public ResponseEntity<Response> applyEmail(String email) {
        emailRegistrationService.sendAuthKey(email);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/email")
    public ResponseEntity<Response> regByEmail(@RequestBody RegisterForm form) {
        MemberVo memberVo = emailRegistrationService.regMember(form);
        return new ResponseEntity<>(ok(memberVo), HttpStatus.CREATED);
    }

    @PostMapping("/shop/email")
    public ResponseEntity<Response> regShopMemberByEmail(@RequestBody ShopRegisterForm form) {
        log.error("length value is -> {}", JsonUtil.toJson(form));
        ShopMemberVo memberVo = emailRegistrationService.regShopMember(form);
        return new ResponseEntity<>(ok(memberVo), HttpStatus.CREATED);
    }

    @PostMapping("/mobileVerification")
    public ResponseEntity<Response> applySmsForLogin(@RequestBody Map<String,String> param) {
        mobileRegistrationService.sendAuthKey(param.get("mobile"));
        return ResponseEntity.ok(ok());
    }

}
