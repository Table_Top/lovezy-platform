package com.lovezy.platform.discuz.mapper;

import com.lovezy.platform.discuz.DiscuzConfiguration;
import com.lovezy.platform.discuz.model.DiscuzMember;
import com.lovezy.platform.discuz.service.impl.DiscuzMemberServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by jin on 2017/11/3.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DiscuzConfiguration.class)
public class DiscuzMemberMapperTest {

    @Autowired
    private DiscuzMemberServiceImpl discuzMemberService;

    @Test
    public void testDiscuzMember() {
        DiscuzMember member = discuzMemberService.selectById(1);
        Assert.assertNotNull(member);

    }

}