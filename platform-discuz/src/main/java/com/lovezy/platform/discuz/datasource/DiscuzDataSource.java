package com.lovezy.platform.discuz.datasource;

import org.apache.ibatis.session.SqlSessionFactory;

import javax.sql.DataSource;

/**
 * Created by jin on 2017/11/3.
 */
public class DiscuzDataSource {

    private DataSource dataSource;

    private SqlSessionFactory sqlSessionFactory;

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }

    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

}
