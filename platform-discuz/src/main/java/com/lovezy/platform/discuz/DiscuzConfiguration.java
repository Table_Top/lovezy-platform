package com.lovezy.platform.discuz;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jin on 2017/11/4.
 */
@Configuration
@ComponentScan
public class DiscuzConfiguration {
}
