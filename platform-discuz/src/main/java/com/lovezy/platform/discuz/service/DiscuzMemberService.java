package com.lovezy.platform.discuz.service;

import com.lovezy.platform.discuz.vo.DiscuzMemberVo;

/**
 * 论坛用户服务接口
 * Created by jin on 2017/11/4.
 */
public interface DiscuzMemberService {
    DiscuzMemberVo login(String username, String password);
}
