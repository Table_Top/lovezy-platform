package com.lovezy.platform.discuz.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 论坛用户VO
 * Created by jin on 2017/11/5.
 */
@Getter
@Setter
@ToString
public class DiscuzMemberVo {
    private Integer uid;

    private String username;

    private String password;

    private String email;

    private Integer regdate;

}
