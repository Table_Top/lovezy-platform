package com.lovezy.platform.discuz.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.discuz.model.DiscuzMember;
import com.lovezy.platform.discuz.model.DiscuzMemberExample;

public interface DiscuzMemberMapper extends BaseExampleMapper<DiscuzMember, DiscuzMemberExample, Integer> {

}