package com.lovezy.platform.discuz.datasource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by jin on 2017/11/4.
 */
@Component
public class DiscuzDataProperties {

    @Value("${spring.datasource.discuz.driver-class-name}")
    private String driverClassName;

    @Value("${spring.datasource.discuz.url}")
    private String url;

    @Value("${spring.datasource.discuz.username}")
    private String username;

    @Value("${spring.datasource.discuz.password}")
    private String password;

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DiscuzDataProperties{");
        sb.append("driverClassName='").append(driverClassName).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
