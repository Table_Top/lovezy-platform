package com.lovezy.platform.core.cache;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by jin on 2017/12/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class RedisCacheTest {

    @Autowired
    private Cache cache;

    @Test
    public void testOps() {
        String key = "test_value";
        cache.put(key, new CacheTestPoJo("value"));
        CacheTestPoJo o = cache.getIfPresent(key);
        Assert.assertNotNull(o);
        Assert.assertEquals(o.a,"value");
        CacheTestPoJo cacheTestPoJo = cache.get("none", () -> new CacheTestPoJo("none"));
        Assert.assertNotNull(o);
        Assert.assertNotNull(cache.getIfPresent("none"));
        CacheTestPoJo inCache = cache.getIfPresent("none");
        Assert.assertEquals(cacheTestPoJo.a, inCache.a);
        cache.invalidate(key);
        cache.invalidate("none");

    }



}
