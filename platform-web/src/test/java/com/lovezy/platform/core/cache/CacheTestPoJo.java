package com.lovezy.platform.core.cache;

import java.io.Serializable;

/**
 * Created by jin on 2017/12/23.
 */
public class CacheTestPoJo implements Serializable{
    public CacheTestPoJo(String a) {
        this.a = a;
    }

    String a;

    @Override
    public String toString() {
        return "CacheTestPojo -> " + a;
    }
}
