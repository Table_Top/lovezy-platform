package com.lovezy.platform.email;

import com.lovezy.platform.core.email.EmailSender;
import com.lovezy.platform.core.email.EmailTarget;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by jin on 2017/11/9.
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class EmailSenderTest {

    @Autowired
    EmailSender emailSender;

    @Test
    public void testSendEmail() {
        EmailTarget target = new EmailTarget("951987962@qq.com","subject","text");
        emailSender.send(target);
    }

}
