package com.lovezy.platform.wiki;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.wiki.query.WikiQuery;
import com.lovezy.platform.wiki.service.GameService;
import com.lovezy.platform.wiki.vo.WikiVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Created by jin on 2017/11/5.
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class GameServiceTest {

    @Autowired
    GameService gameService;

    @Test
    public void selectList() {
        WikiQuery query = new WikiQuery();
        PageInfo<WikiVo> wikiVos = gameService.selectList(query);
        List<WikiVo> list = wikiVos.getList();
        Assert.notEmpty(list,"wikiVo 为空");
        list.forEach(System.out::print);
    }
}
