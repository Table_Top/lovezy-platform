package com.lovezy.platform.wiki;

import com.lovezy.platform.wiki.mapper.GameMapper;
import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.service.impl.GameServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

/**
 * Created by jin on 2017/11/4.
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class WikiServiceTest {


    @Autowired
    GameMapper gameMapper;

    @Test
    public void testGame() {
        Game game = gameMapper.selectByPrimaryKey(1);
        Assert.notNull(game);
    }
}
