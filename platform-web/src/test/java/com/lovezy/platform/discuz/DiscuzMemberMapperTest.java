package com.lovezy.platform.discuz;

import com.lovezy.platform.discuz.model.DiscuzMember;
import com.lovezy.platform.discuz.service.impl.DiscuzMemberServiceImpl;
import com.lovezy.platform.wiki.mapper.GameMapper;
import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.service.GameService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by jin on 2017/11/4.
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class DiscuzMemberMapperTest {

    @Autowired
    DiscuzMemberServiceImpl discuzMemberService;

    @Autowired
    GameMapper gameMapper;

    @Test
    public void testDiscuz() {
        DiscuzMember member = discuzMemberService.selectById(1);
        Assert.assertNotNull(member);
        Game game = gameMapper.selectByPrimaryKey(1);
        Assert.assertNotNull(game);
    }

}
