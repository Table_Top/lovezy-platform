package com.lovezy.platform.mobile;

import com.lovezy.platform.common.prop.Constant;
import com.lovezy.platform.core.cache.RedisCacheTest;
import com.lovezy.platform.message.mobile.enums.SmsTypeEnum;
import com.lovezy.platform.message.mobile.service.SmsSenderService;
import com.lovezy.platform.message.mobile.service.SmsServiceTarget;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by jin on 2017/11/10.
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAsync
public class SmsServiceTest {

    @Autowired
    SmsSenderService smsSenderService;

//    @Autowired
//    TestProvider provider;

    @Test
    public void testAlidayuSms() throws Exception{
        SmsServiceTarget target = new SmsServiceTarget();
        target.setMobile("18405814695");//4673
        target.setText("test");
        target.setTemplateCode(Constant.REGISTER_TEMPLATE_CODE);
        target.setType(SmsTypeEnum.MEMBER_REGISTER);
        smsSenderService.sendSms(target);

        Thread.sleep(Integer.MAX_VALUE);
    }
}
