package com.lovezy.platform.controller;

import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.core.valid.ValidatorUtil;
import com.lovezy.platform.core.valid.annotation.Required;
import com.lovezy.platform.member.service.LoginService;
import com.lovezy.platform.member.service.ProfileService;
import com.lovezy.platform.member.service.RegistrationService;
import com.lovezy.platform.message.mobile.enums.SmsTypeEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 短信发送controller
 * Created by jin on 2018/1/14.
 */
@RestController
@RequestMapping("/api/sms")
public class SmsController {

    @Autowired
    @Qualifier("mobileRegistrationService")
    RegistrationService registrationService;

    @Autowired
    LoginService loginService;

    @Autowired
    ProfileService profileService;


    @PostMapping({"", "/"})
    public ResponseEntity<Response> sendSms(@RequestBody SmsParam smsParam) {
        ValidatorUtil.validate(smsParam);
        SmsTypeEnum smsTypeEnum = SmsTypeEnum.valueOf(smsParam.getType());
        switch (smsTypeEnum) {
            case MEMBER_REGISTER:
                registrationService.sendAuthKey(smsParam.getMobile());
                break;
            case MEMBER_LOGIN:
                loginService.sendLoginSms(smsParam.getMobile());
                break;
            case EDIT_PASSWORD:
                profileService.sendEditPasswordSms(SiteContext.getCurrentMidNullAble(),smsParam.getMobile());
                break;
        }
        return ResponseEntity.ok(ok());
    }

}
@Getter
@Setter
@ToString
class SmsParam{

    @Required(errorMsg = "手机号不能为空")
    private String mobile;

    @Required(errorMsg = "短信类型不能为空")
    private Byte type;
}
