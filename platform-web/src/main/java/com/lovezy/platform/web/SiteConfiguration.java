package com.lovezy.platform.web;

import com.lovezy.platform.interceptor.FrontInterceptor;
import com.lovezy.platform.interceptor.TokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * web 配置
 * Created by jin on 2017/11/14.
 */
@Configuration
public class SiteConfiguration extends WebMvcConfigurerAdapter {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(new TokenInterceptor())
                .addPathPatterns("/api/wiki/{id}")
                .addPathPatterns("/api/wiki/detail")
                .addPathPatterns("/api/activity/join")
                .addPathPatterns("/api/activity/list")
                .addPathPatterns("/api/activity/joinList")
                .addPathPatterns("/api/store/search")
                .addPathPatterns("/api/topic/list")
                .addPathPatterns("/api/topic/detail")
                .addPathPatterns("/api/topic/reply/list")
                .addPathPatterns("/api/sms")
                .addPathPatterns("/api/open/**")
        ;

        registry.addInterceptor(new FrontInterceptor())
                .addPathPatterns("/api/wiki/follow")
                .addPathPatterns("/api/borrow/**")
                .addPathPatterns("/api/store/**")
                .addPathPatterns("/api/shop/**")
                .addPathPatterns("/api/profile/**")
                .addPathPatterns("/api/activity/**")
                .addPathPatterns("/api/topic/**")
                .addPathPatterns("/api/collection/**")
                .addPathPatterns("/api/notice/**")
                .addPathPatterns("/api/filestore/upload")
                .addPathPatterns("/api/token/**")
                .addPathPatterns("/api/play/**")
                .addPathPatterns("/api/back/game/**")
                .addPathPatterns("/api/back/play/**")
                .excludePathPatterns("/api/activity/join")
                .excludePathPatterns("/api/activity/list")
                .excludePathPatterns("/api/activity/joinList")
                .excludePathPatterns("/api/store/search")
                .excludePathPatterns("/api/topic/list")
                .excludePathPatterns("/api/topic/detail")
                .excludePathPatterns("/api/topic/reply/list")
        ;
        super.addInterceptors(registry);
    }
}
