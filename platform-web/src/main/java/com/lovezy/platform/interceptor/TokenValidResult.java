package com.lovezy.platform.interceptor;

import lombok.Getter;

/**
 * Token 校验信息
 * Created by jin on 2017/12/24.
 */
@Getter
public class TokenValidResult {

    private boolean success = true;

    private String mid;

    private String errorCode;

    private String errorMsg;

    public TokenValidResult(String mid) {
        this.mid = mid;
    }

    public TokenValidResult(String errorCode, String errorMsg) {
        this.success = false;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }
}
