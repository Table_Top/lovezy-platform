package com.lovezy.platform.play.query;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

import java.util.Date;
import java.util.List;

/**
 * Created by jin on 2018/1/27.
 */
@Getter
@Setter
@Alias("playCharacterDetail")
public class PlayCharacterDetail {

    private Integer characterId;

    private Integer playId;

    private String avatar;

    private String name;

    private String introduce;

    private String description;

    private String createId;

    private String modifyId;

    private Date gmtCreate;

    private Date gmtModify;

    private List<PlayCharacterSkillVo> skill;
}
