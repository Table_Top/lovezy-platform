package com.lovezy.platform.play.scene.service.impl;

import com.github.pagehelper.Page;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.enums.TrueOrFalseEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.play.model.PlayItem;
import com.lovezy.platform.play.scene.mapper.PlaySceneCharacterItemMapper;
import com.lovezy.platform.play.scene.model.PlaySceneCharacter;
import com.lovezy.platform.play.scene.model.PlaySceneCharacterItem;
import com.lovezy.platform.play.scene.model.PlaySceneCharacterItemExample;
import com.lovezy.platform.play.scene.vo.ItemSimpleInfo;
import com.lovezy.platform.play.scene.vo.SimpleItemMeta;
import com.lovezy.platform.play.service.PlayItemService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.lovezy.platform.core.utils.ObjectUtil.byteToBoolean;
import static java.util.stream.Collectors.toList;

/**
 * Created by jin on 2018/1/30.
 */
@Service
public class PlaySceneCharacterItemService extends MapperService<PlaySceneCharacterItem,Integer,PlaySceneCharacterItemMapper>{

    @Autowired
    PlaySceneCharacterService playSceneCharacterService;

    @Autowired
    PlaySceneLogService playSceneLogService;

    @Autowired
    PlayItemService playItemService;

    public List<SimpleItemMeta> simpleMeta(Integer sceneId) {
        return Pager.doPageAll(() -> mapper.simpleMeta(sceneId));
    }

    public List<ItemSimpleInfo> findItemSimpleInfoBySceneCharId(Integer sceneCharacterId) {
        PlaySceneCharacterItemExample example = new PlaySceneCharacterItemExample();
        example.createCriteria()
                .andSceneCharacterIdEqualTo(sceneCharacterId)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        Page<PlaySceneCharacterItem> items = Pager.doPageAll(() -> mapper.selectByExample(example));
        return items.stream()
                .map(s -> {
                    ItemSimpleInfo itemSimpleInfo = new ItemSimpleInfo();
                    itemSimpleInfo.setItemId(s.getItemId());
                    itemSimpleInfo.setEnable(byteToBoolean(s.getEnableStatus()));
                    return itemSimpleInfo;
                }).collect(toList());
    }

    @Transactional
    public void transfer(Integer sceneId,@Nullable Integer fromSceneCharId, @NotNull Integer toSceneCharId, @NotNull Integer item) {
        if (fromSceneCharId != null) {
            Optional<PlaySceneCharacterItem> itemOptional = hasThisItem(fromSceneCharId, item);
            if (!itemOptional.isPresent()) {
                throw new ServiceException("没有此道具");
            }
            PlaySceneCharacterItem item1 = itemOptional.get();
            item1.setStatus(StatusEnum.DELETED.value());
            mapper.updateByPrimaryKeySelective(item1);
        }

        newItem(sceneId, toSceneCharId, item);
        PlayItem playItem = playItemService.findById(item, true);
        transferLog(sceneId, fromSceneCharId, toSceneCharId, playItem.getName());
    }

    private void transferLog(Integer sceneId,Integer fromSceneCharacterId,Integer toSceneCharacterId,String itemName) {
        String contentTemplate;
        Integer[] characterIds;
        PlaySceneCharacter to = playSceneCharacterService.findCharacter(sceneId, toSceneCharacterId);
        if (fromSceneCharacterId != null) {
            PlaySceneCharacter from = playSceneCharacterService.findCharacter(sceneId, fromSceneCharacterId);
            contentTemplate = "%s从%s处得到了道具" + itemName;
            characterIds = new Integer[]{from.getCharacterId(), to.getCharacterId()};
        }else{
            contentTemplate = "%s得到了道具" + itemName;
            characterIds = new Integer[]{to.getCharacterId()};
        }
        playSceneLogService.logSceneWithCharacter(sceneId, contentTemplate, characterIds);
    }

    public void change(Integer sceneId, Integer sceneCharId, Integer itemId,StatusEnum status,TrueOrFalseEnum enable) {
        PlaySceneCharacterItemExample example = new PlaySceneCharacterItemExample();
        example.createCriteria()
                .andSceneIdEqualTo(sceneId)
                .andSceneCharacterIdEqualTo(sceneCharId)
                .andItemIdEqualTo(itemId);
        PlaySceneCharacterItem update = new PlaySceneCharacterItem();
        update.setStatus(status.value());
        update.setEnableStatus(enable.value());
        mapper.updateByExampleSelective(update, example);
    }

    private Optional<PlaySceneCharacterItem> hasThisItem(Integer charId, Integer item) {
        PlaySceneCharacterItemExample example = new PlaySceneCharacterItemExample();
        example.createCriteria()
                .andSceneCharacterIdEqualTo(charId)
                .andItemIdEqualTo(item);
        return Pager.doSingleton(() -> mapper.selectByExample(example));
    }

    private PlaySceneCharacterItem newItem(Integer sceneId, Integer sceneCharId, Integer itemid) {
        PlaySceneCharacterItem item = new PlaySceneCharacterItem();
        item.setSceneCharacterId(sceneCharId);
        item.setEnableStatus(TrueOrFalseEnum.TRUE.value());
        item.setStatus(StatusEnum.NORMAL.value());
        item.setItemId(itemid);
        item.setSceneId(sceneId);
        mapper.insert(item);
        return item;
    }


}
