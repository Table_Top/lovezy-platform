package com.lovezy.platform.play.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.model.PlayItem;
import com.lovezy.platform.play.model.PlayItemExample;
import com.lovezy.platform.play.query.PlayItemVo;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.scene.vo.SimpleItemMeta;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlayItemMapper extends BaseExampleMapper<PlayItem, PlayItemExample, Integer> {
    List<PlayItemVo> findVoList(PlayQuery query);

    List<SimpleItemMeta> simpleMeta(Integer playId);
}