package com.lovezy.platform.play.controller;

import com.github.pagehelper.Page;
import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.play.model.PlayCharacterSkill;
import com.lovezy.platform.play.query.PlayCharacterSkillForm;
import com.lovezy.platform.play.query.PlayCharacterSkillVo;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.service.PlayCharacterSkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2018/1/24.
 */
@RestController
@RequestMapping("/api/back/play/{playId}/character/{characterId}/skill")
public class BackPlayCharacterSkillController {

    @Autowired
    PlayCharacterSkillService playCharacterSkillService;


    @PostMapping({"", "/"})
    public ResponseEntity<Response> newPlayCharacterSkill(@RequestBody PlayCharacterSkillForm skill, @PathVariable Integer characterId) {
        String mid = SiteContext.getCurrentMid();
        skill.setCreateId(mid);
        skill.setCharacterId(characterId);
        PlayCharacterSkill newSkill = playCharacterSkillService.saveCharacterSkill(skill);
        return ResponseEntity.ok(ok(ImmutableMap.of("skillId", newSkill.getSkillId())));
    }

    @GetMapping("/{skillId}")
    public ResponseEntity<Response> playCharacterSkill(@PathVariable Integer skillId) {
        PlayCharacterSkillVo skill = playCharacterSkillService.findVoById(skillId);
        return ResponseEntity.ok(ok(skill));
    }

    @GetMapping("/list")
    public ResponseEntity<Response> skillList(PlayQuery query,@PathVariable Integer characterId) {
        query.setCharacterId(characterId);
        Page<PlayCharacterSkillVo> skills = playCharacterSkillService.itemList(query);
        return ResponseEntity.ok(ok(skills));
    }

    @DeleteMapping("/{skillId}")
    public ResponseEntity<Response> deleteCharacter(@PathVariable Integer skillId) {
        String mid = SiteContext.getCurrentMid();
        PlayCharacterSkill skill = new PlayCharacterSkill();
        skill.setSkillId(skillId);
        skill.setModifyId(mid);
        playCharacterSkillService.deleteSkill(skill);
        return ResponseEntity.ok(ok());
    }

    @PutMapping({"/{skillId}"})
    public ResponseEntity<Response> editCharacter(@PathVariable Integer skillId,@RequestBody PlayCharacterSkillForm skill) {
        String mid = SiteContext.getCurrentMid();
        skill.setModifyId(mid);
        skill.setSkillId(skillId);
        playCharacterSkillService.editPlayCharacterSkill(skill);
        return ResponseEntity.ok(ok());
    }

}
