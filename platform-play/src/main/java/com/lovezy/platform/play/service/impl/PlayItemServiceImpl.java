package com.lovezy.platform.play.service.impl;

import com.github.pagehelper.Page;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.enums.TrueOrFalseEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.filestore.model.File;
import com.lovezy.platform.filestore.service.FileService;
import com.lovezy.platform.play.mapper.PlayItemMapper;
import com.lovezy.platform.play.model.PlayItem;
import com.lovezy.platform.play.query.PlayItemForm;
import com.lovezy.platform.play.query.PlayItemVo;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.scene.vo.SimpleItemMeta;
import com.lovezy.platform.play.service.PlayItemService;
import com.lovezy.platform.play.service.PlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

import static com.lovezy.platform.core.utils.ObjectUtil.copyTo;

/**
 * 剧本道具服务
 * Created by jin on 2018/1/24.
 */
@Service
public class PlayItemServiceImpl extends MapperService<PlayItem,Integer,PlayItemMapper> implements PlayItemService {

    @Autowired
    PlayService playService;

    @Autowired
    FileService fileService;

    @Override
    public PlayItem savePlayItem(PlayItemForm form) {
        PlayItem playItem = copyTo(form, new PlayItem());

        Date now = new Date();
        playItem.setStatus(StatusEnum.NORMAL.value());
        playItem.setGmtModify(now);
        playItem.setGmtCreate(now);
        playItem.setModifyId(playItem.getCreateId());
        if (playItem.getUseable() == null) {
            playItem.setUseable(TrueOrFalseEnum.FALSE.value());
        }
        String pic = form.getPicUrl();
        if (StringUtils.hasText(pic)) {
            Integer picId = findFileId(pic);
            playItem.setPic(picId);
        }
        mapper.insert(playItem);
        return playItem;
    }

    private Integer findFileId(String fileUrl) {
        File file = fileService.findFileByUrl(fileUrl);
        return file.getFileId();
    }

    @Override
    public PlayItem editPlayItem(PlayItemForm form) {
        PlayItem playItem = copyTo(form, new PlayItem());
        Integer playId = playItem.getPlayId();
        playService.findById(playId, true);
        findById(playItem.getItemId(), true);
        String pic = form.getPicUrl();
        if (StringUtils.hasText(pic)) {
            Integer picId = findFileId(pic);
            playItem.setPic(picId);
        }
        mapper.updateByPrimaryKeySelective(playItem);
        return playItem;
    }

    @Override
    public PlayItemVo findVoById(Integer itemId) {
        PlayItem item = findById(itemId, true);
        PlayItemVo itemVo = copyTo(item, new PlayItemVo());
        Integer pic = item.getPic();
        if (pic != null) {
            String picDownloadUrl = fileService.downloadId(pic);
            itemVo.setPicUrl(picDownloadUrl);
        }
        return itemVo;
    }

    @Override
    public PlayItem findById(Integer itemId, boolean throwIfNotExist) {
        PlayItem item = mapper.selectByPrimaryKey(itemId);
        if (throwIfNotExist && item == null) {
            throw new ServiceException("剧本道具不存在");
        }
        return item;
    }

    @Override
    public void deletePlayItem(PlayItem item) {
        Integer itemId = item.getItemId();
        findById(itemId, true);
        PlayItem playItem = new PlayItem();
        playItem.setItemId(itemId);
        playItem.setStatus(StatusEnum.DELETED.value());
        playItem.setGmtModify(new Date());
        playItem.setModifyId(item.getModifyId());
        mapper.updateByPrimaryKeySelective(playItem);
    }

    @Override
    public Page<PlayItemVo> itemList(PlayQuery query) {
        Page<PlayItemVo> voList = Pager.doPage(query, () -> mapper.findVoList(query));
        voList.forEach(item -> {
            Integer pic = item.getPic();
            if (pic != null) {
                item.setPicUrl(fileService.downloadId(pic));
            }
        });
        return voList;
    }

    @Override
    public List<SimpleItemMeta> simpleMeta(Integer playId) {
        return Pager.doPageAll(() -> mapper.simpleMeta(playId));
    }

}
