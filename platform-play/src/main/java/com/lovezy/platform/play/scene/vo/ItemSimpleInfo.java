package com.lovezy.platform.play.scene.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemSimpleInfo{
    private Integer itemId;

    private boolean enable;
}
