package com.lovezy.platform.play.scene.model;

public class PlaySceneCharacterSkill {
    private Integer sceneSkillId;

    private Integer sceneCharacterId;

    private Integer sceneId;

    private Integer skillId;

    private Byte enableStatus;

    private Byte status;

    public Integer getSceneSkillId() {
        return sceneSkillId;
    }

    public void setSceneSkillId(Integer sceneSkillId) {
        this.sceneSkillId = sceneSkillId;
    }

    public Integer getSceneCharacterId() {
        return sceneCharacterId;
    }

    public void setSceneCharacterId(Integer sceneCharacterId) {
        this.sceneCharacterId = sceneCharacterId;
    }

    public Integer getSceneId() {
        return sceneId;
    }

    public void setSceneId(Integer sceneId) {
        this.sceneId = sceneId;
    }

    public Integer getSkillId() {
        return skillId;
    }

    public void setSkillId(Integer skillId) {
        this.skillId = skillId;
    }

    public Byte getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(Byte enableStatus) {
        this.enableStatus = enableStatus;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}