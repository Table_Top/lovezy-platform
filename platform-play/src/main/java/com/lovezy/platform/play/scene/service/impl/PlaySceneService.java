package com.lovezy.platform.play.scene.service.impl;

import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.member.service.MemberRoleService;
import com.lovezy.platform.member.vo.MemberRoleVo;
import com.lovezy.platform.play.model.PlayProcess;
import com.lovezy.platform.play.scene.mapper.PlaySceneMapper;
import com.lovezy.platform.play.scene.model.PlayScene;
import com.lovezy.platform.play.scene.model.PlaySceneCharacter;
import com.lovezy.platform.play.service.PlayProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.function.Consumer;

/**
 * Created by jin on 2018/1/30.
 */
@Service
public class PlaySceneService extends MapperService<PlayScene,Integer,PlaySceneMapper>{

    @Autowired
    MemberRoleService memberRoleService;

    @Autowired
    PlaySceneCharacterService playSceneCharacterService;

    @Autowired
    PlayProcessService playProcessService;

    private PlayScene getById(Integer sceneId) {
        PlayScene playScene = mapper.selectByPrimaryKey(sceneId);
        if (playScene == null) {
            throw new ServiceException("此剧本实例不存在");
        }
        return playScene;
    }

    public PlayScene findPlaySceneByMember(Integer sceneId, String mid) {
        return findPlaySceneByMember(sceneId, mid, null);
    }

    public PlayScene findPlaySceneByMember(Integer sceneId, String mid, Consumer<PlaySceneCharacter> playSceneCharacterConsumer) {
        MemberRoleVo memberRole = memberRoleService.findMemberRole(mid);
        if (!memberRole.isAdmin() && !memberRole.isShop()) {
            PlaySceneCharacter character = playSceneCharacterService.findCharacter(sceneId, mid);
            if (playSceneCharacterConsumer != null) {
                playSceneCharacterConsumer.accept(character);
            }
        }
        return getById(sceneId);
    }

    public PlayScene findPlaySceneByShoper(Integer sceneId, String mid) {
        MemberRoleVo memberRole = memberRoleService.findMemberRole(mid);
        if (!memberRole.isAdmin() && !memberRole.isShop()) {
            throw new ServiceException("您的权限不足");
        }
        return getById(sceneId);
    }

    public PlayScene newPlayScene(Integer playId, String createId) {
        PlayProcess firstProcess = playProcessService.findFirstProcess(playId);
        Date now = new Date();
        PlayScene scene = new PlayScene();
        scene.setPlayId(playId);
        scene.setProcessId(firstProcess.getProcessId());
        scene.setGmtCreate(now);
        scene.setGmtModify(now);
        scene.setStatus(StatusEnum.NORMAL.value());
        scene.setCreateId(createId);
        mapper.insert(scene);
        return scene;
    }

    public void sceneProcessAction(PlayScene scene, Integer action) {
        PlayProcess playProcess = playProcessService.operatingProcess(scene.getProcessId(), action);
        PlayScene updateScene = new PlayScene();
        updateScene.setSceneId(scene.getSceneId());
        updateScene.setGmtModify(new Date());
        updateScene.setProcessId(playProcess.getProcessId());
        mapper.updateByPrimaryKeySelective(updateScene);
    }



}
