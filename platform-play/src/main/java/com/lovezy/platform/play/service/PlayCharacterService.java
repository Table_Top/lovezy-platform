package com.lovezy.platform.play.service;

import com.github.pagehelper.Page;
import com.lovezy.platform.play.model.PlayCharacter;
import com.lovezy.platform.play.query.PlayCharacterDetail;
import com.lovezy.platform.play.query.PlayCharacterForm;
import com.lovezy.platform.play.query.PlayQuery;

import java.util.List;

/**
 * 剧本角色服务
 * Created by jin on 2018/1/24.
 */
public interface PlayCharacterService {
    PlayCharacter saveCharacter(PlayCharacterForm character);

    PlayCharacter editPlayCharacter(PlayCharacterForm playCharacter);

    PlayCharacter findById(Integer characterId, boolean throwIfNotExist);

    PlayCharacterDetail findDetailById(Integer characterId);

    void deletePlayCharacter(PlayCharacter character);

    Page<PlayCharacterDetail> characterList(PlayQuery query);

    List<PlayCharacter> findAllCharacters(Integer playId);
}
