package com.lovezy.platform.play.scene.service.impl;

import com.github.pagehelper.Page;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.enums.TrueOrFalseEnum;
import com.lovezy.platform.core.exception.RequestErrorException;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.member.service.MemberRoleService;
import com.lovezy.platform.member.vo.MemberRoleVo;
import com.lovezy.platform.play.model.Play;
import com.lovezy.platform.play.scene.model.PlayScene;
import com.lovezy.platform.play.scene.model.PlaySceneCharacter;
import com.lovezy.platform.play.scene.model.PlaySceneLog;
import com.lovezy.platform.play.scene.query.ItemChangeForm;
import com.lovezy.platform.play.scene.query.ItemTransferForm;
import com.lovezy.platform.play.scene.query.SceneQuery;
import com.lovezy.platform.play.scene.query.SkillChangeForm;
import com.lovezy.platform.play.scene.query.SkillTransferForm;
import com.lovezy.platform.play.scene.service.SceneService;
import com.lovezy.platform.play.scene.vo.CharacterData;
import com.lovezy.platform.play.scene.vo.CharacterMemberForm;
import com.lovezy.platform.play.scene.vo.SceneCharacter;
import com.lovezy.platform.play.scene.vo.SceneLogVo;
import com.lovezy.platform.play.scene.vo.ScenePlayMetaData;
import com.lovezy.platform.play.service.PlayCharacterSkillService;
import com.lovezy.platform.play.service.PlayItemService;
import com.lovezy.platform.play.service.PlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

import static com.lovezy.platform.core.utils.ObjectUtil.nonNull;

/**
 * Created by jin on 2018/1/30.
 */
@Service
public class SceneServiceImpl implements SceneService{

    @Autowired
    PlayService playService;

    @Autowired
    PlaySceneService playSceneService;

    @Autowired
    PlayItemService playItemService;

    @Autowired
    PlayCharacterSkillService playCharacterSkillService;

    @Autowired
    MemberRoleService memberRoleService;

    @Autowired
    PlaySceneCharacterService playSceneCharacterService;

    @Autowired
    PlaySceneCharacterItemService playSceneCharacterItemService;

    @Autowired
    PlaySceneCharacterSkillService playSceneCharacterSkillService;

    @Autowired
    PlaySceneLogService playSceneLogService;

    @Transactional
    @Override
    public PlayScene shoperCreateScene(Integer playId, String mid) {
        playService.findById(playId, true);
        MemberRoleVo memberRole = memberRoleService.findMemberRole(mid);
        if (!memberRole.isShop() && !memberRole.isAdmin()) {
            throw new ServiceException("无权限新开剧本");
        }
        PlayScene scene = playSceneService.newPlayScene(playId, mid);
        playSceneCharacterService.copySceneCharacters(playId, scene.getSceneId());
        return scene;
    }

    @Override
    public ScenePlayMetaData playMeta(Integer sceneId, String mid) {
        PlayScene playScene = playSceneService.findPlaySceneByMember(sceneId, mid);
        ScenePlayMetaData metaData = new ScenePlayMetaData();
        metaData.setCharacters(playSceneCharacterService.simpleMeta(sceneId));
        Integer playId = playScene.getPlayId();
        metaData.setSkills(playCharacterSkillService.simpleMeta(playId));
        metaData.setItems(playItemService.simpleMeta(playId));
        Play play = playService.findById(playScene.getPlayId(), true);
        metaData.setPlay(play);
        return metaData;
    }

    @Override
    public void processAction(Integer senceId, String mid, Integer action) {
        PlayScene scene = playSceneService.findPlaySceneByShoper(senceId, mid);
        playSceneService.sceneProcessAction(scene, action);
    }

    @Override
    public void putCharacterMember(CharacterMemberForm form) {
        playSceneCharacterService.putMemberCharacter(form.getMemberId(), form.getSceneId(), form.getCharacterId());

    }

    @Override
    public SceneCharacter sceneCharacters(Integer sceneId, String mid) {
        List<CharacterData> chars = playSceneCharacterService.findAllCharacterFullInfo(sceneId);
        SceneCharacter sc = new SceneCharacter();
        sc.setSceneId(sceneId);
        sc.setCharacters(chars);
        return sc;
    }

    @Override
    public CharacterData findCharacterData(Integer sceneId, String memberId) {
        return playSceneCharacterService.findCharacterDateByMemberId(sceneId, memberId);
    }


    @Override
    public void itemTransfer(ItemTransferForm itemTransferForm) {
        Integer sceneId = itemTransferForm.getSceneId();
        Integer fromId = itemTransferForm.getFrom();
        Integer to = itemTransferForm.getTo();
        Integer itemId = itemTransferForm.getItemId();
        Integer formCharId = null;
        Integer toCharId;
        if (fromId != null) {
            PlaySceneCharacter character = playSceneCharacterService.findCharacter(sceneId, fromId);
            formCharId = character.getSceneCharacterId();
        }

        PlaySceneCharacter toChar = playSceneCharacterService.findCharacter(sceneId, to);
        toCharId = toChar.getSceneCharacterId();
        playSceneCharacterItemService.transfer(sceneId, formCharId, toCharId, itemId);
    }

    @Override
    public void itemChange(ItemChangeForm change) {
        Integer characterId = change.getCharacterId();
        Integer sceneId = change.getSceneId();
        Integer itemId = change.getItemId();
        TrueOrFalseEnum enable = change.getEnableStatus() == (byte) 1 ? TrueOrFalseEnum.TRUE : TrueOrFalseEnum.FALSE;
        StatusEnum status = change.getStatus() == (byte) 1 ? StatusEnum.NORMAL : StatusEnum.DELETED;
        PlaySceneCharacter character = playSceneCharacterService.findCharacter(sceneId, characterId);
        playSceneCharacterItemService.change(sceneId, character.getSceneCharacterId(), itemId, status, enable);
    }

    @Override
    public void skillTransfer(SkillTransferForm transferForm) {
        Integer sceneId = transferForm.getSceneId();
        Integer fromId = transferForm.getFrom();
        Integer to = transferForm.getTo();
        Integer skillId = transferForm.getSkillId();
        Integer formCharId = null;
        Integer toCharId;
        if (fromId != null) {
            PlaySceneCharacter character = playSceneCharacterService.findCharacter(sceneId, fromId);
            formCharId = character.getSceneCharacterId();
        }

        PlaySceneCharacter toChar = playSceneCharacterService.findCharacter(sceneId, to);
        toCharId = toChar.getSceneCharacterId();
        playSceneCharacterSkillService.transfer(sceneId, formCharId, toCharId, skillId);
    }

    @Override
    public void skillChange(SkillChangeForm change) {
        Integer characterId = change.getCharacterId();
        Integer sceneId = change.getSceneId();
        Integer skillId = change.getSkillId();
        TrueOrFalseEnum enable = change.getEnableStatus() == (byte) 1 ? TrueOrFalseEnum.TRUE : TrueOrFalseEnum.FALSE;
        StatusEnum status = change.getStatus() == (byte) 1 ? StatusEnum.NORMAL : StatusEnum.DELETED;
        PlaySceneCharacter character = playSceneCharacterService.findCharacter(sceneId, characterId);
        playSceneCharacterSkillService.change(sceneId, character.getSceneCharacterId(), skillId, status, enable);
    }

    @Override
    public Page<SceneLogVo> sceneLogs(SceneQuery query) {
        nonNull(query.getMemberId(), query.getSceneId());
        return playSceneLogService.logs(query);
    }

    @Override
    public PlaySceneLog logScene(Integer sceneId, String content, String mid) {
        if (!StringUtils.hasText(content)) {
            throw new RequestErrorException("内容不能为空");
        }
        MemberRoleVo memberRole = memberRoleService.findMemberRole(mid);
        if (!memberRole.isAdmin() && !memberRole.isShop()) {
            throw new ServiceException("没有权限记录");
        }
        return playSceneLogService.logScene(sceneId, content);
    }

}
