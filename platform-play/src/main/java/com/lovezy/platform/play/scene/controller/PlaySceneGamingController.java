package com.lovezy.platform.play.scene.controller;

import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.play.scene.query.ItemChangeForm;
import com.lovezy.platform.play.scene.query.ItemTransferForm;
import com.lovezy.platform.play.scene.query.SkillChangeForm;
import com.lovezy.platform.play.scene.query.SkillTransferForm;
import com.lovezy.platform.play.scene.service.SceneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;


/**
 * Created by jin on 2018/1/30.
 */
@RestController
@RequestMapping("/api/play/scene/{sceneId}")
public class PlaySceneGamingController {

    @Autowired
    SceneService sceneService;

    @PostMapping("/item/transfer")
    public ResponseEntity<Response> itemTransfer(@PathVariable Integer sceneId, @RequestBody ItemTransferForm itemTransfer) {
        String mid = SiteContext.getCurrentMid();
        itemTransfer.setModifyId(mid);
        itemTransfer.setSceneId(sceneId);
        sceneService.itemTransfer(itemTransfer);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/item/change")
    public ResponseEntity<Response> itemChange(@PathVariable Integer sceneId, @RequestBody ItemChangeForm changeForm) {
        String mid = SiteContext.getCurrentMid();
        changeForm.setModifyId(mid);
        changeForm.setSceneId(sceneId);
        sceneService.itemChange(changeForm);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/skill/transfer")
    public ResponseEntity<Response> skillTransfer(@PathVariable Integer sceneId, @RequestBody SkillTransferForm transfer) {
        String mid = SiteContext.getCurrentMid();
        transfer.setModifyId(mid);
        transfer.setSceneId(sceneId);
        sceneService.skillTransfer(transfer);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/skill/change")
    public ResponseEntity<Response> skillChange(@PathVariable Integer sceneId, @RequestBody SkillChangeForm changeForm) {
        String mid = SiteContext.getCurrentMid();
        changeForm.setModifyId(mid);
        changeForm.setSceneId(sceneId);
        sceneService.skillChange(changeForm);
        return ResponseEntity.ok(ok());
    }




}
