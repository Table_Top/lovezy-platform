package com.lovezy.platform.play.controller;

import com.github.pagehelper.Page;
import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.play.model.PlayProcess;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.service.PlayProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2018/1/24.
 */
@RestController
@RequestMapping("/api/back/play/{playId}/process")
public class BackPlayProcessController {

    @Autowired
    PlayProcessService processService;


    @PostMapping({"", "/"})
    public ResponseEntity<Response> newPlayProcess(@RequestBody PlayProcess playProcess,@PathVariable Integer playId) {
        String mid = SiteContext.getCurrentMid();
        playProcess.setCreateId(mid);
        playProcess.setPlayId(playId);
        PlayProcess newProcess = processService.savePlayProcess(playProcess);
        return ResponseEntity.ok(ok(ImmutableMap.of("processId", newProcess.getProcessId())));
    }

    @GetMapping("/{processId}")
    public ResponseEntity<Response> playProcessDetail(@PathVariable Integer processId) {
        PlayProcess play = processService.findById(processId, true);
        return ResponseEntity.ok(ok(play));
    }

    @GetMapping("/list")
    public ResponseEntity<Response> processList(PlayQuery query,@PathVariable Integer playId) {
        query.setPlayId(playId);
        Page<PlayProcess> processes = processService.processList(query);
        return ResponseEntity.ok(ok(processes));
    }

    @DeleteMapping("/{processId}")
    public ResponseEntity<Response> deletePlay(@PathVariable Integer processId) {
        String mid = SiteContext.getCurrentMid();
        PlayProcess process = new PlayProcess();
        process.setProcessId(processId);
        process.setModifyId(mid);
        processService.deletePlayProcess(process);
        return ResponseEntity.ok(ok());
    }

    @PutMapping({"/{processId}"})
    public ResponseEntity<Response> editPlay(@PathVariable Integer processId,@RequestBody PlayProcess process,@PathVariable Integer playId) {
        String mid = SiteContext.getCurrentMid();
        process.setModifyId(mid);
        process.setProcessId(processId);
        process.setPlayId(playId);
        processService.editPlayProcess(process);
        return ResponseEntity.ok(ok());
    }

}
