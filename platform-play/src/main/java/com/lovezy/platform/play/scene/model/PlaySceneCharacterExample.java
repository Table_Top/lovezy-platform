package com.lovezy.platform.play.scene.model;

import java.util.ArrayList;
import java.util.List;

public class PlaySceneCharacterExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PlaySceneCharacterExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSceneCharacterIdIsNull() {
            addCriterion("sceneCharacterId is null");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdIsNotNull() {
            addCriterion("sceneCharacterId is not null");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdEqualTo(Integer value) {
            addCriterion("sceneCharacterId =", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdNotEqualTo(Integer value) {
            addCriterion("sceneCharacterId <>", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdGreaterThan(Integer value) {
            addCriterion("sceneCharacterId >", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("sceneCharacterId >=", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdLessThan(Integer value) {
            addCriterion("sceneCharacterId <", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdLessThanOrEqualTo(Integer value) {
            addCriterion("sceneCharacterId <=", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdIn(List<Integer> values) {
            addCriterion("sceneCharacterId in", values, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdNotIn(List<Integer> values) {
            addCriterion("sceneCharacterId not in", values, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdBetween(Integer value1, Integer value2) {
            addCriterion("sceneCharacterId between", value1, value2, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdNotBetween(Integer value1, Integer value2) {
            addCriterion("sceneCharacterId not between", value1, value2, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneIdIsNull() {
            addCriterion("sceneId is null");
            return (Criteria) this;
        }

        public Criteria andSceneIdIsNotNull() {
            addCriterion("sceneId is not null");
            return (Criteria) this;
        }

        public Criteria andSceneIdEqualTo(Integer value) {
            addCriterion("sceneId =", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdNotEqualTo(Integer value) {
            addCriterion("sceneId <>", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdGreaterThan(Integer value) {
            addCriterion("sceneId >", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("sceneId >=", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdLessThan(Integer value) {
            addCriterion("sceneId <", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdLessThanOrEqualTo(Integer value) {
            addCriterion("sceneId <=", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdIn(List<Integer> values) {
            addCriterion("sceneId in", values, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdNotIn(List<Integer> values) {
            addCriterion("sceneId not in", values, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdBetween(Integer value1, Integer value2) {
            addCriterion("sceneId between", value1, value2, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdNotBetween(Integer value1, Integer value2) {
            addCriterion("sceneId not between", value1, value2, "sceneId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdIsNull() {
            addCriterion("characterId is null");
            return (Criteria) this;
        }

        public Criteria andCharacterIdIsNotNull() {
            addCriterion("characterId is not null");
            return (Criteria) this;
        }

        public Criteria andCharacterIdEqualTo(Integer value) {
            addCriterion("characterId =", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdNotEqualTo(Integer value) {
            addCriterion("characterId <>", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdGreaterThan(Integer value) {
            addCriterion("characterId >", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("characterId >=", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdLessThan(Integer value) {
            addCriterion("characterId <", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdLessThanOrEqualTo(Integer value) {
            addCriterion("characterId <=", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdIn(List<Integer> values) {
            addCriterion("characterId in", values, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdNotIn(List<Integer> values) {
            addCriterion("characterId not in", values, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdBetween(Integer value1, Integer value2) {
            addCriterion("characterId between", value1, value2, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdNotBetween(Integer value1, Integer value2) {
            addCriterion("characterId not between", value1, value2, "characterId");
            return (Criteria) this;
        }

        public Criteria andMemberIdIsNull() {
            addCriterion("memberId is null");
            return (Criteria) this;
        }

        public Criteria andMemberIdIsNotNull() {
            addCriterion("memberId is not null");
            return (Criteria) this;
        }

        public Criteria andMemberIdEqualTo(String value) {
            addCriterion("memberId =", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotEqualTo(String value) {
            addCriterion("memberId <>", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdGreaterThan(String value) {
            addCriterion("memberId >", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdGreaterThanOrEqualTo(String value) {
            addCriterion("memberId >=", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdLessThan(String value) {
            addCriterion("memberId <", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdLessThanOrEqualTo(String value) {
            addCriterion("memberId <=", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdLike(String value) {
            addCriterion("memberId like", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotLike(String value) {
            addCriterion("memberId not like", value, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdIn(List<String> values) {
            addCriterion("memberId in", values, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotIn(List<String> values) {
            addCriterion("memberId not in", values, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdBetween(String value1, String value2) {
            addCriterion("memberId between", value1, value2, "memberId");
            return (Criteria) this;
        }

        public Criteria andMemberIdNotBetween(String value1, String value2) {
            addCriterion("memberId not between", value1, value2, "memberId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}