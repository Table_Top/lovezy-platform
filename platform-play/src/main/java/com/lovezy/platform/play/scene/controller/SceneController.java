package com.lovezy.platform.play.scene.controller;

import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.play.scene.model.PlayScene;
import com.lovezy.platform.play.scene.service.SceneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2018/1/30.
 */
@RestController
@RequestMapping("/api/play/{playId}")
public class SceneController {

    @Autowired
    SceneService sceneService;

    @PostMapping("/scene")
    public ResponseEntity<Response> newScene(@PathVariable Integer playId) {
        String mid = SiteContext.getCurrentMid();
        PlayScene scene = sceneService.shoperCreateScene(playId, mid);
        return ResponseEntity.ok(ok(ImmutableMap.of("sceneId", scene.getSceneId())));
    }

}
