package com.lovezy.platform.play.scene.model;

import java.util.Date;

public class PlaySceneLog {
    private Integer sceneLogId;

    private Integer sceneId;

    private String content;

    private Date gmtCreate;

    public Integer getSceneLogId() {
        return sceneLogId;
    }

    public void setSceneLogId(Integer sceneLogId) {
        this.sceneLogId = sceneLogId;
    }

    public Integer getSceneId() {
        return sceneId;
    }

    public void setSceneId(Integer sceneId) {
        this.sceneId = sceneId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }
}