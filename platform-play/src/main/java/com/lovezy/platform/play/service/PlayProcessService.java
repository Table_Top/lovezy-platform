package com.lovezy.platform.play.service;

import com.github.pagehelper.Page;
import com.lovezy.platform.play.model.PlayProcess;
import com.lovezy.platform.play.query.PlayQuery;

/**
 * 剧本流程服务
 * Created by jin on 2018/1/24.
 */
public interface PlayProcessService {
    PlayProcess savePlayProcess(PlayProcess playProcess);

    PlayProcess editPlayProcess(PlayProcess playProcess);

    PlayProcess findById(Integer processId, boolean throwIfNotExist);

    void deletePlayProcess(PlayProcess playProcess);

    Page<PlayProcess> processList(PlayQuery query);

    PlayProcess findFirstProcess(Integer playId);

    PlayProcess operatingProcess(Integer currentProcessId, int action);
}
