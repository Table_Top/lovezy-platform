package com.lovezy.platform.play.query;

import com.lovezy.platform.core.base.query.BaseQuery;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

/**
 * Created by jin on 2018/1/24.
 */
@Getter
@Setter
@Component
@Scope(SCOPE_PROTOTYPE)
@Alias("playQuery")
public class PlayQuery extends BaseQuery{

    private Integer playId;

    private Integer characterId;
}
