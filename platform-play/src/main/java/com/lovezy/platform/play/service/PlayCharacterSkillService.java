package com.lovezy.platform.play.service;

import com.github.pagehelper.Page;
import com.lovezy.platform.play.model.PlayCharacterSkill;
import com.lovezy.platform.play.query.PlayCharacterSkillForm;
import com.lovezy.platform.play.query.PlayCharacterSkillVo;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.scene.vo.SimpleSkillMeta;

import java.util.List;

/**
 * 剧本技能服务
 * Created by jin on 2018/1/24.
 */
public interface PlayCharacterSkillService {
    PlayCharacterSkill saveCharacterSkill(PlayCharacterSkillForm skill);

    void saveCharacterSkill(List<PlayCharacterSkillForm> forms, Integer characterId);

    PlayCharacterSkill editPlayCharacterSkill(PlayCharacterSkillForm skill);

    PlayCharacterSkillVo findVoById(Integer skillId);

    PlayCharacterSkill findById(Integer skillId, boolean throwIfNotExist);

    void deleteSkill(PlayCharacterSkill skill);

    Page<PlayCharacterSkillVo> itemList(PlayQuery query);

    List<PlayCharacterSkill> findCharacterSkills(Integer characterId);

    List<SimpleSkillMeta> simpleMeta(Integer playId);
}
