package com.lovezy.platform.play.service.impl;

import com.github.pagehelper.Page;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.utils.ArrayUtil;
import com.lovezy.platform.play.mapper.PlayCharacterMapper;
import com.lovezy.platform.play.model.PlayCharacter;
import com.lovezy.platform.play.model.PlayCharacterExample;
import com.lovezy.platform.play.query.PlayCharacterDetail;
import com.lovezy.platform.play.query.PlayCharacterForm;
import com.lovezy.platform.play.query.PlayCharacterSkillForm;
import com.lovezy.platform.play.query.PlayCharacterSkillVo;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.service.PlayCharacterService;
import com.lovezy.platform.play.service.PlayCharacterSkillService;
import com.lovezy.platform.play.service.PlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static com.lovezy.platform.core.utils.ObjectUtil.copyTo;

/**
 * 剧本角色服务
 * Created by jin on 2018/1/24.
 */
@Service
public class PlayCharacterServiceImpl extends MapperService<PlayCharacter,Integer,PlayCharacterMapper> implements PlayCharacterService{
    @Autowired
    PlayService playService;

    @Autowired
    PlayCharacterSkillService playCharacterSkillService;

    @Override
    @Transactional
    public PlayCharacter saveCharacter(PlayCharacterForm form) {
        PlayCharacter character = copyTo(form, new PlayCharacter());
        character.setStatus(StatusEnum.NORMAL.value());
        Date now = new Date();
        character.setGmtModify(now);
        character.setGmtCreate(now);
        character.setModifyId(character.getCreateId());
        mapper.insert(character);
        List<PlayCharacterSkillForm> skill = form.getSkill();
        if (!ArrayUtil.isEmpty(skill)) {
            skill.forEach(s -> s.setCreateId(character.getCreateId()));
        }
        playCharacterSkillService.saveCharacterSkill(skill,character.getCharacterId());
        return character;
    }

    @Override
    public PlayCharacter editPlayCharacter(PlayCharacterForm form) {
        PlayCharacter playCharacter = copyTo(form, new PlayCharacter());
        Integer playId = playCharacter.getPlayId();
        playService.findById(playId, true);
        findById(playCharacter.getCharacterId(), true);
        mapper.updateByPrimaryKeySelective(playCharacter);
        List<PlayCharacterSkillForm> skill = form.getSkill();
        if (!ArrayUtil.isEmpty(skill)) {
            skill.forEach(s -> s.setCreateId(form.getModifyId()));
        }
        playCharacterSkillService.saveCharacterSkill(skill,playCharacter.getCharacterId());
        return playCharacter;
    }

    @Override
    public PlayCharacter findById(Integer characterId, boolean throwIfNotExist) {
        PlayCharacter character = mapper.selectByPrimaryKey(characterId);
        if (throwIfNotExist && character == null) {
            throw new ServiceException("剧本角色不存在");
        }
        return character;
    }

    @Override
    public PlayCharacterDetail findDetailById(Integer characterId) {
        PlayCharacter character = findById(characterId, true);
        return playCharacterDetail(character);
    }

    private PlayCharacterDetail playCharacterDetail(PlayCharacter character) {
        PlayCharacterDetail characterDetail = new PlayCharacterDetail();
        copyTo(character, characterDetail);
        findDetailSkill(characterDetail);
        return characterDetail;
    }

    private void findDetailSkill(PlayCharacterDetail characterDetail) {
        PlayQuery query = new PlayQuery();
        query.setCharacterId(characterDetail.getCharacterId());
        query.setPageSize(0);
        Page<PlayCharacterSkillVo> skills = playCharacterSkillService.itemList(query);
        characterDetail.setSkill(skills);
    }

    @Override
    public void deletePlayCharacter(PlayCharacter character) {
        Integer characterId = character.getCharacterId();
        PlayCharacter playCharacter = findById(characterId, true);
        playCharacter.setStatus(StatusEnum.DELETED.value());
        playCharacter.setGmtModify(new Date());
        playCharacter.setModifyId(character.getModifyId());
        mapper.updateByPrimaryKeySelective(playCharacter);
    }

    @Override
    public Page<PlayCharacterDetail> characterList(PlayQuery query) {
        Page<PlayCharacterDetail> characters = Pager.doPage(query, () -> mapper.findVoList(query));
        characters.forEach(this::findDetailSkill);
        return characters;
    }

    @Override
    public List<PlayCharacter> findAllCharacters(Integer playId) {
        PlayCharacterExample example = new PlayCharacterExample();
        example.createCriteria()
                .andPlayIdEqualTo(playId)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        return Pager.doPageAll(() -> mapper.selectByExample(example));
    }

}
