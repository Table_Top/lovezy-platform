package com.lovezy.platform.play.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.model.PlayProcess;
import com.lovezy.platform.play.model.PlayProcessExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PlayProcessMapper extends BaseExampleMapper<PlayProcess, PlayProcessExample, Integer> {
}