package com.lovezy.platform.play.scene.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jin on 2018/1/30.
 */
@Getter
@Setter
public class CharacterMemberForm {

    private Integer characterId;

    private String memberId;

    private Integer sceneId;

}
