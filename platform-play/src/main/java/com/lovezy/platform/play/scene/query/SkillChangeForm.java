package com.lovezy.platform.play.scene.query;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jin on 2018/2/1.
 */
@Getter
@Setter
public class SkillChangeForm extends ChangeForm{
    private Integer skillId;
}
