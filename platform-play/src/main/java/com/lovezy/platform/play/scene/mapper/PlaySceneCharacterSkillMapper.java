package com.lovezy.platform.play.scene.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.scene.model.PlaySceneCharacterSkill;
import com.lovezy.platform.play.scene.model.PlaySceneCharacterSkillExample;
import com.lovezy.platform.play.scene.vo.SimpleSkillMeta;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlaySceneCharacterSkillMapper extends BaseExampleMapper<PlaySceneCharacterSkill, PlaySceneCharacterSkillExample, Integer> {

    List<SimpleSkillMeta> simpleMeta(Integer sceneId);
}