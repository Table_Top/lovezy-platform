package com.lovezy.platform.play.scene.service;

import com.github.pagehelper.Page;
import com.lovezy.platform.play.scene.model.PlayScene;
import com.lovezy.platform.play.scene.model.PlaySceneLog;
import com.lovezy.platform.play.scene.query.ItemChangeForm;
import com.lovezy.platform.play.scene.query.ItemTransferForm;
import com.lovezy.platform.play.scene.query.SceneQuery;
import com.lovezy.platform.play.scene.query.SkillChangeForm;
import com.lovezy.platform.play.scene.query.SkillTransferForm;
import com.lovezy.platform.play.scene.vo.CharacterData;
import com.lovezy.platform.play.scene.vo.CharacterMemberForm;
import com.lovezy.platform.play.scene.vo.SceneCharacter;
import com.lovezy.platform.play.scene.vo.SceneLogVo;
import com.lovezy.platform.play.scene.vo.ScenePlayMetaData;

/**
 * Created by jin on 2018/1/30.
 */
public interface SceneService {
    PlayScene shoperCreateScene(Integer playId, String mid);

    ScenePlayMetaData playMeta(Integer sceneId, String mid);

    void processAction(Integer senceId, String mid, Integer action);

    void putCharacterMember(CharacterMemberForm form);

    SceneCharacter sceneCharacters(Integer sceneId, String mid);

    CharacterData findCharacterData(Integer sceneId, String memberId);

    void itemTransfer(ItemTransferForm itemTransferForm);

    void itemChange(ItemChangeForm change);

    void skillTransfer(SkillTransferForm transferForm);

    void skillChange(SkillChangeForm change);

    Page<SceneLogVo> sceneLogs(SceneQuery query);

    PlaySceneLog logScene(Integer sceneId, String content, String mid);
}
