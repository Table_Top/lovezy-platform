package com.lovezy.platform.play.scene.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by jin on 2018/2/1.
 */
@Getter
@Setter
public class SceneCharacter {

    private Integer sceneId;


    private List<CharacterData> characters;

}
