package com.lovezy.platform.play.controller;

import com.github.pagehelper.Page;
import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.play.model.PlayCharacter;
import com.lovezy.platform.play.query.PlayCharacterDetail;
import com.lovezy.platform.play.query.PlayCharacterForm;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.service.PlayCharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2018/1/24.
 */
@RestController
@RequestMapping("/api/back/play/{playId}/character")
public class BackPlayCharacterController {

    @Autowired
    PlayCharacterService playCharacterService;


    @PostMapping({"", "/"})
    public ResponseEntity<Response> newPlayCharacter(@RequestBody PlayCharacterForm character, @PathVariable Integer playId) {
        String mid = SiteContext.getCurrentMid();
        character.setCreateId(mid);
        character.setPlayId(playId);
        PlayCharacter playCharacter = playCharacterService.saveCharacter(character);
        return ResponseEntity.ok(ok(ImmutableMap.of("itemId", playCharacter.getCharacterId())));
    }

    @GetMapping("/{characterId}")
    public ResponseEntity<Response> playCharacter(@PathVariable Integer characterId) {
        PlayCharacterDetail character = playCharacterService.findDetailById(characterId);
        return ResponseEntity.ok(ok(character));
    }

    @GetMapping("/list")
    public ResponseEntity<Response> characterList(PlayQuery query,@PathVariable Integer playId) {
        query.setPlayId(playId);
        Page<PlayCharacterDetail> characters = playCharacterService.characterList(query);
        return ResponseEntity.ok(ok(characters));
    }

    @DeleteMapping("/{characterId}")
    public ResponseEntity<Response> deleteCharacter(@PathVariable Integer characterId) {
        String mid = SiteContext.getCurrentMid();
        PlayCharacter character = new PlayCharacter();
        character.setCharacterId(characterId);
        character.setModifyId(mid);
        playCharacterService.deletePlayCharacter(character);
        return ResponseEntity.ok(ok());
    }

    @PutMapping({"/{characterId}"})
    public ResponseEntity<Response> editCharacter(@PathVariable Integer characterId,@RequestBody PlayCharacterForm character) {
        String mid = SiteContext.getCurrentMid();
        character.setModifyId(mid);
        character.setCharacterId(characterId);
        playCharacterService.editPlayCharacter(character);
        return ResponseEntity.ok(ok());
    }

}
