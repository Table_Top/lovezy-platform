package com.lovezy.platform.play.scene.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

/**
 * Created by jin on 2018/1/30.
 */
@Getter
@Setter
@ToString
@Alias("simpleItemMeta")
public class SimpleItemMeta implements SimpleMeta{

    private String name;

    private String description;

    private Integer itemId;

}
