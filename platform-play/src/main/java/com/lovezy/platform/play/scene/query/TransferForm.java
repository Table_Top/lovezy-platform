package com.lovezy.platform.play.scene.query;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jin on 2018/2/1.
 */
@Getter
@Setter
public abstract class TransferForm {

    private Integer sceneId;

    private Integer to;

    private Integer from;

    private String modifyId;
}
