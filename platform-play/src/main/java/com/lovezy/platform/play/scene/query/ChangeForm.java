package com.lovezy.platform.play.scene.query;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jin on 2018/2/1.
 */
@Getter
@Setter
public abstract class ChangeForm {

    private Integer characterId;

    private Byte status;

    private Byte enableStatus;

    private Integer sceneId;

    private String modifyId;
}
