package com.lovezy.platform.play.scene.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.scene.model.PlaySceneCharacterItem;
import com.lovezy.platform.play.scene.model.PlaySceneCharacterItemExample;
import com.lovezy.platform.play.scene.vo.SimpleItemMeta;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlaySceneCharacterItemMapper extends BaseExampleMapper<PlaySceneCharacterItem, PlaySceneCharacterItemExample, Integer> {

    List<SimpleItemMeta> simpleMeta(Integer sceneId);

}