package com.lovezy.platform.play.query;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by jin on 2018/1/27.
 */
@Getter
@Setter
public class PlayCharacterForm {

    private Integer characterId;

    private Integer playId;

    private String avatar;

    private String name;

    private String description;

    private String createId;

    private String modifyId;

    private List<PlayCharacterSkillForm> skill;


}
