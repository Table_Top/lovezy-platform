package com.lovezy.platform.play.scene.query;

import com.lovezy.platform.core.base.query.BaseQuery;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by jin on 2018/2/3.
 */
@Getter
@Setter
public class SceneQuery extends BaseQuery{

    private Integer sceneId;

    private String memberId;

}
