package com.lovezy.platform.play.scene.service.impl;

import com.github.pagehelper.Page;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.play.scene.mapper.PlaySceneLogMapper;
import com.lovezy.platform.play.scene.model.PlaySceneLog;
import com.lovezy.platform.play.scene.model.PlaySceneLogExample;
import com.lovezy.platform.play.scene.query.SceneQuery;
import com.lovezy.platform.play.scene.vo.SceneLogVo;
import com.lovezy.platform.play.service.PlayCharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by jin on 2018/2/3.
 */
@Service
public class PlaySceneLogService extends MapperService<PlaySceneLog,Integer,PlaySceneLogMapper>{

    @Autowired
    PlayCharacterService playCharacterService;

    public Page<SceneLogVo> logs(SceneQuery query) {
        PlaySceneLogExample example = new PlaySceneLogExample();
        Integer sceneId = query.getSceneId();
        example.createCriteria()
                .andSceneIdEqualTo(sceneId);
        return Pager.doPage(query, () -> mapper.selectByExample(example));

    }

    public PlaySceneLog logScene(Integer sceneId,String content) {
        PlaySceneLog log = new PlaySceneLog();
        log.setSceneId(sceneId);
        log.setContent(content);
        log.setGmtCreate(new Date());
        mapper.insert(log);
        return log;
    }

    public PlaySceneLog logSceneWithCharacter(Integer sceneId,String contentTemplate,Integer... characterIds) {
        String[] characters = new String[characterIds.length];
        for (int i = 0; i < characterIds.length; i++) {
            characters[i] = playCharacterService.findById(characterIds[i], true).getName();
        }
        String content = String.format(contentTemplate,(Object[]) characters);
        return logScene(sceneId, content);
    }


}
