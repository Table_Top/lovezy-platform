package com.lovezy.platform.play.controller;

import com.github.pagehelper.Page;
import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.play.model.Play;
import com.lovezy.platform.play.model.PlayAttachment;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.service.PlayService;
import com.lovezy.platform.play.service.impl.PlayAttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2018/1/24.
 */
@RestController
@RequestMapping("/api/back/play")
public class BackPlayController {

    @Autowired
    PlayService playService;

    @Autowired
    PlayAttachmentService playAttachmentService;


    @PostMapping({"", "/"})
    public ResponseEntity<Response> newPlay(@RequestBody Play play) {
        String mid = SiteContext.getCurrentMid();
        play.setCreateId(mid);
        Play newPlay = playService.savePlay(play);
        return ResponseEntity.ok(ok(ImmutableMap.of("playId", newPlay.getPlayId())));
    }

    @GetMapping("/{playId}")
    public ResponseEntity<Response> playDetail(@PathVariable Integer playId) {
        Play play = playService.findById(playId, true);
        return ResponseEntity.ok(ok(play));
    }

    @GetMapping("/list")
    public ResponseEntity<Response> playList(PlayQuery query) {
        Page<Play> plays = playService.playList(query);
        return ResponseEntity.ok(ok(plays));
    }

    @DeleteMapping("/{playId}")
    public ResponseEntity<Response> deletePlay(@PathVariable Integer playId) {
        String mid = SiteContext.getCurrentMid();
        Play play = new Play();
        play.setPlayId(playId);
        play.setModifyId(mid);
        playService.deletePlay(play);
        return ResponseEntity.ok(ok());
    }

    @PutMapping({"","/"})
    public ResponseEntity<Response> editPlay(@RequestBody Play play) {
        String mid = SiteContext.getCurrentMid();
        play.setModifyId(mid);
        playService.editPlay(play);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/{playId}/attachment")
    public ResponseEntity<Response> newAttachment(@RequestBody PlayAttachment playAttachment,@PathVariable Integer playId) {
        playAttachment.setPlayId(playId);
        playAttachmentService.newAttachment(playAttachment);
        return ResponseEntity.ok(ok());
    }

    @DeleteMapping("/{playId}/attachment")
    public ResponseEntity<Response> deleteAttachment(@RequestBody PlayAttachment playAttachment) {
        playAttachmentService.deleteAttachment(playAttachment);
        return ResponseEntity.ok(ok());
    }

    @PutMapping("/{playId}/attachment")
    public ResponseEntity<Response> editAttachment(@RequestBody PlayAttachment playAttachment) {
        playAttachmentService.editAttachment(playAttachment);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/{playId}/attachment/list")
    public ResponseEntity<Response> attachments(PlayQuery query,@PathVariable Integer playId) {
        query.setPlayId(playId);
        Page<PlayAttachment> attachments = playAttachmentService.findAttachments(query);
        return ResponseEntity.ok(ok(attachments));
    }

}
