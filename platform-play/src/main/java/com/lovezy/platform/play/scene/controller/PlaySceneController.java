package com.lovezy.platform.play.scene.controller;

import com.github.pagehelper.Page;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.play.scene.query.SceneQuery;
import com.lovezy.platform.play.scene.service.SceneService;
import com.lovezy.platform.play.scene.vo.CharacterData;
import com.lovezy.platform.play.scene.vo.CharacterMemberForm;
import com.lovezy.platform.play.scene.vo.SceneCharacter;
import com.lovezy.platform.play.scene.vo.SceneLogVo;
import com.lovezy.platform.play.scene.vo.ScenePlayMetaData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;


/**
 * Created by jin on 2018/1/30.
 */
@RestController
@RequestMapping("/api/play/scene/{sceneId}")
public class PlaySceneController {

    @Autowired
    SceneService sceneService;

    @GetMapping("/meta")
    public ResponseEntity<Response> meta(@PathVariable Integer sceneId) {
        String mid = SiteContext.getCurrentMid();
        ScenePlayMetaData metaData = sceneService.playMeta(sceneId, mid);
        return ResponseEntity.ok(ok(metaData));
    }

    @PutMapping("/process")
    public ResponseEntity<Response> operatingProcess(@PathVariable Integer sceneId, @RequestBody Integer action) {
        String mid = SiteContext.getCurrentMid();
        sceneService.processAction(sceneId, mid, action);
        return ResponseEntity.ok(ok());
    }


    @PostMapping("/characterMember")
    public ResponseEntity<Response> characterMember(@RequestBody CharacterMemberForm form) {
        sceneService.putCharacterMember(form);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/characters")
    public ResponseEntity<Response> characters(@PathVariable Integer sceneId) {
        String mid = SiteContext.getCurrentMid();
        SceneCharacter sceneCharacter = sceneService.sceneCharacters(sceneId, mid);
        return ResponseEntity.ok(ok(sceneCharacter));
    }

    @GetMapping("/myCharacter")
    public ResponseEntity<Response> myCharacter(@PathVariable Integer sceneId) {
        String mid = SiteContext.getCurrentMid();
        CharacterData cd = sceneService.findCharacterData(sceneId, mid);
        return ResponseEntity.ok(ok(cd));
    }

    @GetMapping("/log")
    public ResponseEntity<Response> logs(SceneQuery query,@PathVariable Integer sceneId) {
        String mid = SiteContext.getCurrentMid();
        query.setSceneId(sceneId);
        query.setMemberId(mid);
        Page<SceneLogVo> logs = sceneService.sceneLogs(query);
        return ResponseEntity.ok(ok(logs));
    }

    @PostMapping("/log")
    public ResponseEntity<Response> log(@RequestBody Map<String,String> param, @PathVariable Integer sceneId) {
        String mid = SiteContext.getCurrentMid();
        String content = param.get("content");
        sceneService.logScene(sceneId, content, mid);
        return ResponseEntity.ok(ok());
    }
}
