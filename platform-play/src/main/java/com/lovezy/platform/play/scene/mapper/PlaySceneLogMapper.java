package com.lovezy.platform.play.scene.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.scene.model.PlaySceneLog;
import com.lovezy.platform.play.scene.model.PlaySceneLogExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PlaySceneLogMapper extends BaseExampleMapper<PlaySceneLog, PlaySceneLogExample, Integer> {
}