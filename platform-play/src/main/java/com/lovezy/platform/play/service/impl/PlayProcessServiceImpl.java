package com.lovezy.platform.play.service.impl;

import com.github.pagehelper.Page;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.play.mapper.PlayProcessMapper;
import com.lovezy.platform.play.model.PlayProcess;
import com.lovezy.platform.play.model.PlayProcessExample;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.service.PlayProcessService;
import com.lovezy.platform.play.service.PlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

import static com.lovezy.platform.core.utils.ObjectUtil.nonNull;

/**
 * 剧本流程服务
 * Created by jin on 2018/1/24.
 */
@Service
public class PlayProcessServiceImpl extends MapperService<PlayProcess,Integer,PlayProcessMapper> implements PlayProcessService{

    @Autowired
    PlayService playService;

    @Override
    public PlayProcess savePlayProcess(PlayProcess playProcess) {
        Date now = new Date();
        playProcess.setStatus(StatusEnum.NORMAL.value());
        playProcess.setGmtCreate(now);
        playProcess.setGmtModify(now);
        playProcess.setModifyId(playProcess.getCreateId());
        mapper.insert(playProcess);
        return playProcess;
    }

    @Override
    public PlayProcess editPlayProcess(PlayProcess playProcess) {
        Integer playId = playProcess.getPlayId();
        playService.findById(playId, true);
        findById(playProcess.getProcessId(), true);
        mapper.updateByPrimaryKeySelective(playProcess);
        return playProcess;
    }

    @Override
    public PlayProcess findById(Integer processId, boolean throwIfNotExist) {
        PlayProcess play = mapper.selectByPrimaryKey(processId);
        if (throwIfNotExist && play == null) {
            throw new ServiceException("剧本流程不存在");
        }
        return play;
    }

    @Override
    public void deletePlayProcess(PlayProcess playProcess) {
        Integer processId = playProcess.getProcessId();
        PlayProcess dbPlayProcess = findById(processId, true);
        dbPlayProcess.setStatus(StatusEnum.DELETED.value());
        dbPlayProcess.setGmtModify(new Date());
        dbPlayProcess.setModifyId(playProcess.getModifyId());
        mapper.updateByPrimaryKeySelective(dbPlayProcess);
    }

    @Override
    public Page<PlayProcess> processList(PlayQuery query) {
        PlayProcessExample example = new PlayProcessExample();
        example.setOrderByClause("orderNum is null ,orderNum, gmtCreate DESC");
        Integer playId = query.getPlayId();
        nonNull(playId);
        example.createCriteria()
                .andStatusEqualTo(StatusEnum.NORMAL.value())
                .andPlayIdEqualTo(playId);
        return Pager.doPage(query,() -> {
            mapper.selectByExample(example);
        });
    }

    @Override
    public PlayProcess findFirstProcess(Integer playId) {
        return doOperationgProcess(null, playId, null);
    }

    @Override
    public PlayProcess operatingProcess(Integer currentProcessId, int action) {
       return doOperationgProcess(currentProcessId, null, action);
    }

    private PlayProcess doOperationgProcess(Integer currentProcessId, Integer playId, Integer action) {
        PlayProcessExample example = new PlayProcessExample();
        example.setOrderByClause("orderNum is null ,orderNum, gmtCreate DESC");
        PlayProcessExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo(StatusEnum.NORMAL.value());
        if (currentProcessId == null) {
            if(playId == null) throw new ServiceException("剧本ID不能为空");
            criteria.andPlayIdEqualTo(playId);
        } else {
            PlayProcess currentProcess = mapper.selectByPrimaryKey(currentProcessId);
            if (currentProcess == null) {
                throw new ServiceException("此剧本进程不存在");
            }
            criteria.andPlayIdEqualTo(currentProcess.getPlayId());
            if (action > 0) {
                criteria.andOrderNumGreaterThan(currentProcess.getOrderNum());
            }else{
                criteria.andOrderNumLessThan(currentProcess.getOrderNum());
            }
        }
        Optional<PlayProcess> processOptional = Pager.doSingleton(() -> mapper.selectByExample(example));
        if (!processOptional.isPresent()) {
            throw new ServiceException("剧本进程已经结束");
        }
        return processOptional.get();
    }


}
