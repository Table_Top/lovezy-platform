package com.lovezy.platform.play.service;

import com.github.pagehelper.Page;
import com.lovezy.platform.play.model.PlayItem;
import com.lovezy.platform.play.query.PlayItemForm;
import com.lovezy.platform.play.query.PlayItemVo;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.scene.vo.SimpleItemMeta;

import java.util.List;

/**
 * 剧本道具服务
 * Created by jin on 2018/1/24.
 */
public interface PlayItemService {
    PlayItem savePlayItem(PlayItemForm playItem);

    PlayItem editPlayItem(PlayItemForm playItem);

    PlayItemVo findVoById(Integer itemId);

    PlayItem findById(Integer itemId, boolean throwIfNotExist);

    void deletePlayItem(PlayItem item);

    Page<PlayItemVo> itemList(PlayQuery query);

    List<SimpleItemMeta> simpleMeta(Integer playId);
}
