package com.lovezy.platform.play.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.model.PlayAttachment;
import com.lovezy.platform.play.model.PlayAttachmentExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PlayAttachmentMapper extends BaseExampleMapper<PlayAttachment, PlayAttachmentExample, Integer> {

}