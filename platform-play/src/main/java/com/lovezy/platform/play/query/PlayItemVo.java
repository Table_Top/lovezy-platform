package com.lovezy.platform.play.query;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * Created by jin on 2018/1/26.
 */
@Getter
@Setter
@Alias("playItemVo")
public class PlayItemVo {
    private Integer itemId;

    private Integer playId;

    private String name;

    private String description;

    @JsonIgnore
    private Integer pic;

    private String picUrl;

    private Byte useable;

    private String createId;

    private String modifyId;

    private Date gmtCreate;

    private Date gmtModify;

    private Byte status;
}
