package com.lovezy.platform.play.controller;

import com.github.pagehelper.Page;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.play.model.Play;
import com.lovezy.platform.play.model.PlayAttachment;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.service.PlayService;
import com.lovezy.platform.play.service.impl.PlayAttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2018/1/29.
 */
@RestController
@RequestMapping("/api/play")
public class PlayController {

    @Autowired
    PlayService playService;

    @Autowired
    PlayAttachmentService playAttachmentService;

    @GetMapping("/{playId}")
    public ResponseEntity<Response> playDetail(@PathVariable Integer playId) {
        Play play = playService.findById(playId, true);
        return ResponseEntity.ok(ok(play));
    }

    @GetMapping("/{playId}/attachment/list")
    public ResponseEntity<Response> attachments(@PathVariable Integer playId, @RequestBody PlayQuery query) {
        query.setPlayId(playId);
        Page<PlayAttachment> attachments = playAttachmentService.findAttachments(query);
        return ResponseEntity.ok(ok(attachments));
    }

}
