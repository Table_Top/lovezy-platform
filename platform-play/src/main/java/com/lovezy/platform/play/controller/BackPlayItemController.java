package com.lovezy.platform.play.controller;

import com.github.pagehelper.Page;
import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.play.model.PlayItem;
import com.lovezy.platform.play.query.PlayItemForm;
import com.lovezy.platform.play.query.PlayItemVo;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.service.PlayItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2018/1/24.
 */
@RestController
@RequestMapping("/api/back/play/{playId}/item")
public class BackPlayItemController {

    @Autowired
    PlayItemService playItemService;


    @PostMapping({"", "/"})
    public ResponseEntity<Response> newPlayItem(@RequestBody PlayItemForm playItem, @PathVariable Integer playId) {
        String mid = SiteContext.getCurrentMid();
        playItem.setCreateId(mid);
        playItem.setPlayId(playId);
        PlayItem item = playItemService.savePlayItem(playItem);
        return ResponseEntity.ok(ok(ImmutableMap.of("itemId", item.getItemId())));
    }

    @GetMapping("/{itemId}")
    public ResponseEntity<Response> playItem(@PathVariable Integer itemId) {
        PlayItemVo item = playItemService.findVoById(itemId);
        return ResponseEntity.ok(ok(item));
    }

    @GetMapping("/list")
    public ResponseEntity<Response> itemList(PlayQuery query,@PathVariable Integer playId) {
        query.setPlayId(playId);
        Page<PlayItemVo> items = playItemService.itemList(query);
        return ResponseEntity.ok(ok(items));
    }

    @DeleteMapping("/{itemId}")
    public ResponseEntity<Response> deletePlay(@PathVariable Integer itemId) {
        String mid = SiteContext.getCurrentMid();
        PlayItem item = new PlayItem();
        item.setItemId(itemId);
        item.setModifyId(mid);
        playItemService.deletePlayItem(item);
        return ResponseEntity.ok(ok());
    }

    @PutMapping({"/{itemId}"})
    public ResponseEntity<Response> editPlay(@PathVariable Integer itemId,@RequestBody PlayItemForm item) {
        String mid = SiteContext.getCurrentMid();
        item.setModifyId(mid);
        item.setItemId(itemId);
        playItemService.editPlayItem(item);
        return ResponseEntity.ok(ok());
    }

}
