package com.lovezy.platform.play.scene.model;

public class PlaySceneCharacter {
    private Integer sceneCharacterId;

    private Integer sceneId;

    private Integer characterId;

    private String memberId;

    public Integer getSceneCharacterId() {
        return sceneCharacterId;
    }

    public void setSceneCharacterId(Integer sceneCharacterId) {
        this.sceneCharacterId = sceneCharacterId;
    }

    public Integer getSceneId() {
        return sceneId;
    }

    public void setSceneId(Integer sceneId) {
        this.sceneId = sceneId;
    }

    public Integer getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Integer characterId) {
        this.characterId = characterId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }
}