package com.lovezy.platform.play.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.model.PlayCharacter;
import com.lovezy.platform.play.model.PlayCharacterExample;
import com.lovezy.platform.play.query.PlayCharacterDetail;
import com.lovezy.platform.play.query.PlayQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlayCharacterMapper extends BaseExampleMapper<PlayCharacter, PlayCharacterExample, Integer> {
    List<PlayCharacterDetail> findVoList(PlayQuery query);
}