package com.lovezy.platform.play.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.model.PlayCharacterSkill;
import com.lovezy.platform.play.model.PlayCharacterSkillExample;
import com.lovezy.platform.play.query.PlayCharacterSkillVo;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.scene.vo.SimpleSkillMeta;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlayCharacterSkillMapper extends BaseExampleMapper<PlayCharacterSkill, PlayCharacterSkillExample, Integer> {
    List<PlayCharacterSkillVo> findVoList(PlayQuery query);

    List<SimpleSkillMeta> simpleMeta(Integer playId);
}