package com.lovezy.platform.play.query;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jin on 2018/1/26.
 */
@Getter
@Setter
public class PlayCharacterSkillForm {

    private Integer skillId;

    private Integer characterId;

    private String name;

    private String description;

    private String picUrl;

    private String createId;

    private String modifyId;

}
