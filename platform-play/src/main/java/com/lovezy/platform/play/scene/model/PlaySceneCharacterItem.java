package com.lovezy.platform.play.scene.model;

public class PlaySceneCharacterItem {
    private Integer sceneItemId;

    private Integer sceneCharacterId;

    private Integer sceneId;

    private Integer itemId;

    private Byte enableStatus;

    private Byte status;

    public Integer getSceneItemId() {
        return sceneItemId;
    }

    public void setSceneItemId(Integer sceneItemId) {
        this.sceneItemId = sceneItemId;
    }

    public Integer getSceneCharacterId() {
        return sceneCharacterId;
    }

    public void setSceneCharacterId(Integer sceneCharacterId) {
        this.sceneCharacterId = sceneCharacterId;
    }

    public Integer getSceneId() {
        return sceneId;
    }

    public void setSceneId(Integer sceneId) {
        this.sceneId = sceneId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Byte getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(Byte enableStatus) {
        this.enableStatus = enableStatus;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}