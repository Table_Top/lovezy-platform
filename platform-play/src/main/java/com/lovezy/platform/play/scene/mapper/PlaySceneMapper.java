package com.lovezy.platform.play.scene.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.scene.model.PlayScene;
import com.lovezy.platform.play.scene.model.PlaySceneExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PlaySceneMapper extends BaseExampleMapper<PlayScene, PlaySceneExample, Integer> {

}