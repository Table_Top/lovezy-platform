package com.lovezy.platform.play.service.impl;

import com.github.pagehelper.Page;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.filestore.model.File;
import com.lovezy.platform.filestore.service.FileService;
import com.lovezy.platform.play.mapper.PlayCharacterSkillMapper;
import com.lovezy.platform.play.model.PlayCharacterSkill;
import com.lovezy.platform.play.model.PlayCharacterSkillExample;
import com.lovezy.platform.play.query.PlayCharacterSkillForm;
import com.lovezy.platform.play.query.PlayCharacterSkillVo;
import com.lovezy.platform.play.query.PlayQuery;
import com.lovezy.platform.play.scene.vo.SimpleSkillMeta;
import com.lovezy.platform.play.service.PlayCharacterService;
import com.lovezy.platform.play.service.PlayCharacterSkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

import static com.lovezy.platform.core.utils.ObjectUtil.copyTo;

/**
 * 剧本技能服务
 * Created by jin on 2018/1/24.
 */
@Service
public class PlayCharacterSkillServiceImpl extends MapperService<PlayCharacterSkill,Integer, PlayCharacterSkillMapper> implements PlayCharacterSkillService{

    @Autowired
    PlayCharacterService playCharacterService;

    @Autowired
    FileService fileService;

    @Override
    public PlayCharacterSkill saveCharacterSkill(PlayCharacterSkillForm form) {
        PlayCharacterSkill skill = copyTo(form, new PlayCharacterSkill());
        skill.setStatus(StatusEnum.NORMAL.value());
        Date now = new Date();
        skill.setGmtCreate(now);
        skill.setGmtModify(now);
        skill.setModifyId(skill.getCreateId());
        String picUrl = form.getPicUrl();
        if (StringUtils.hasText(picUrl)) {
            skill.setPic(findFileId(picUrl));
        }
        mapper.insert(skill);
        return skill;
    }

    private void clearCharacterSkill(Integer characterId) {
        PlayCharacterSkillExample example = new PlayCharacterSkillExample();
        example.createCriteria()
                .andCharacterIdEqualTo(characterId)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        PlayCharacterSkill skill = new PlayCharacterSkill();
        skill.setStatus(StatusEnum.DELETED.value());
        mapper.updateByExample(skill, example);
    }

    @Override
    @Transactional
    public void saveCharacterSkill(List<PlayCharacterSkillForm> forms, Integer characterId) {
        clearCharacterSkill(characterId);
        forms.forEach(form -> {
            form.setCharacterId(characterId);
            saveCharacterSkill(form);
        });
    }

    @Override
    public PlayCharacterSkill editPlayCharacterSkill(PlayCharacterSkillForm form) {
        PlayCharacterSkill skill = copyTo(form, new PlayCharacterSkill());
        Integer characterId = skill.getCharacterId();
        playCharacterService.findById(characterId, true);
        findById(skill.getSkillId(), true);
        String picUrl = form.getPicUrl();
        if (StringUtils.hasText(picUrl)) {
            skill.setPic(findFileId(picUrl));
        }
        mapper.updateByPrimaryKeySelective(skill);
        return skill;
    }


    private Integer findFileId(String fileUrl) {
        File file = fileService.findFileByUrl(fileUrl);
        return file.getFileId();
    }

    @Override
    public PlayCharacterSkillVo findVoById(Integer skillId) {
        PlayCharacterSkill skill = findById(skillId, true);
        Integer pic = skill.getPic();
        PlayCharacterSkillVo skillVo = copyTo(skill, new PlayCharacterSkillVo());
        if (pic != null) {
            skillVo.setPicUrl(fileService.downloadId(pic));
        }
        return skillVo;
    }

    @Override
    public PlayCharacterSkill findById(Integer skillId, boolean throwIfNotExist) {
        PlayCharacterSkill skill = mapper.selectByPrimaryKey(skillId);
        if (throwIfNotExist && skill == null) {
            throw new ServiceException("剧本角色技能不存在");
        }
        return skill;
    }

    @Override
    public void deleteSkill(PlayCharacterSkill skill) {
        Integer skillId = skill.getSkillId();
        PlayCharacterSkill dbSkill = findById(skillId, true);
        dbSkill.setStatus(StatusEnum.DELETED.value());
        dbSkill.setGmtModify(new Date());
        dbSkill.setModifyId(skill.getModifyId());
        mapper.updateByPrimaryKeySelective(dbSkill);
    }

    @Override
    public Page<PlayCharacterSkillVo> itemList(PlayQuery query) {
        Page<PlayCharacterSkillVo> skillList = Pager.doPage(query, () -> mapper.findVoList(query));
        skillList.forEach(skill -> {
            Integer pic = skill.getPic();
            if (pic != null) {
                skill.setPicUrl(fileService.downloadId(pic));
            }
        });
        return skillList;
    }

    @Override
    public List<PlayCharacterSkill> findCharacterSkills(Integer characterId) {
        PlayCharacterSkillExample example = new PlayCharacterSkillExample();
        example.createCriteria()
                .andCharacterIdEqualTo(characterId)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        return Pager.doPageAll(() -> mapper.selectByExample(example));
    }

    @Override
    public List<SimpleSkillMeta> simpleMeta(Integer playId) {
        return Pager.doPageAll(() -> mapper.simpleMeta(playId));
    }

}
