package com.lovezy.platform.play.scene.vo;

import com.lovezy.platform.play.model.Play;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 剧本实例元数据
 * Created by jin on 2018/1/30.
 */
@Getter
@Setter
public class ScenePlayMetaData {

    List<SimpleCharacterMeta> characters;

    List<SimpleSkillMeta> skills;

    List<SimpleItemMeta> items;

    Play play;

}
