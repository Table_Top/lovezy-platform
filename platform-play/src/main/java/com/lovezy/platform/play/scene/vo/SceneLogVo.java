package com.lovezy.platform.play.scene.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by jin on 2018/2/3.
 */
@Getter
@Setter
public class SceneLogVo {

    private String content;

    private Date gmtCreate;

}
