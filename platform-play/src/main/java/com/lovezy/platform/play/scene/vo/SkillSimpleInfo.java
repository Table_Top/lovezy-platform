package com.lovezy.platform.play.scene.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SkillSimpleInfo{
    private Integer skillId;

    private boolean enable;
}
