package com.lovezy.platform.play.service;

import com.github.pagehelper.Page;
import com.lovezy.platform.play.model.Play;
import com.lovezy.platform.play.query.PlayQuery;

/**
 * 剧本服务
 * Created by jin on 2018/1/24.
 */
public interface PlayService {
    Play savePlay(Play play);

    Play editPlay(Play play);

    Play findById(Integer playId, boolean throwIfNotExist);

    void deletePlay(Play play);

    Page<Play> playList(PlayQuery query);
}
