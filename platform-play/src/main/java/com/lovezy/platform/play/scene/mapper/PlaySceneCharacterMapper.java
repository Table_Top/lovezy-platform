package com.lovezy.platform.play.scene.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.play.scene.model.PlaySceneCharacter;
import com.lovezy.platform.play.scene.model.PlaySceneCharacterExample;
import com.lovezy.platform.play.scene.vo.SimpleCharacterMeta;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlaySceneCharacterMapper extends BaseExampleMapper<PlaySceneCharacter, PlaySceneCharacterExample, Integer> {

    List<SimpleCharacterMeta> simpleMeta(Integer sceneId);

}