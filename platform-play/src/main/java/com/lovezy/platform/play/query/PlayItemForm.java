package com.lovezy.platform.play.query;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jin on 2018/1/26.
 */
@Getter
@Setter
public class PlayItemForm {

    private Integer itemId;

    private Integer playId;

    private String name;

    private String description;

    private String picUrl;

    private Byte useable;

    private String createId;

    private String modifyId;
}
