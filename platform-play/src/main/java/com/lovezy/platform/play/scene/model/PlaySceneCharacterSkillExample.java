package com.lovezy.platform.play.scene.model;

import java.util.ArrayList;
import java.util.List;

public class PlaySceneCharacterSkillExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PlaySceneCharacterSkillExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSceneSkillIdIsNull() {
            addCriterion("sceneSkillId is null");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdIsNotNull() {
            addCriterion("sceneSkillId is not null");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdEqualTo(Integer value) {
            addCriterion("sceneSkillId =", value, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdNotEqualTo(Integer value) {
            addCriterion("sceneSkillId <>", value, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdGreaterThan(Integer value) {
            addCriterion("sceneSkillId >", value, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("sceneSkillId >=", value, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdLessThan(Integer value) {
            addCriterion("sceneSkillId <", value, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdLessThanOrEqualTo(Integer value) {
            addCriterion("sceneSkillId <=", value, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdIn(List<Integer> values) {
            addCriterion("sceneSkillId in", values, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdNotIn(List<Integer> values) {
            addCriterion("sceneSkillId not in", values, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdBetween(Integer value1, Integer value2) {
            addCriterion("sceneSkillId between", value1, value2, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneSkillIdNotBetween(Integer value1, Integer value2) {
            addCriterion("sceneSkillId not between", value1, value2, "sceneSkillId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdIsNull() {
            addCriterion("sceneCharacterId is null");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdIsNotNull() {
            addCriterion("sceneCharacterId is not null");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdEqualTo(Integer value) {
            addCriterion("sceneCharacterId =", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdNotEqualTo(Integer value) {
            addCriterion("sceneCharacterId <>", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdGreaterThan(Integer value) {
            addCriterion("sceneCharacterId >", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("sceneCharacterId >=", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdLessThan(Integer value) {
            addCriterion("sceneCharacterId <", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdLessThanOrEqualTo(Integer value) {
            addCriterion("sceneCharacterId <=", value, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdIn(List<Integer> values) {
            addCriterion("sceneCharacterId in", values, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdNotIn(List<Integer> values) {
            addCriterion("sceneCharacterId not in", values, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdBetween(Integer value1, Integer value2) {
            addCriterion("sceneCharacterId between", value1, value2, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneCharacterIdNotBetween(Integer value1, Integer value2) {
            addCriterion("sceneCharacterId not between", value1, value2, "sceneCharacterId");
            return (Criteria) this;
        }

        public Criteria andSceneIdIsNull() {
            addCriterion("sceneId is null");
            return (Criteria) this;
        }

        public Criteria andSceneIdIsNotNull() {
            addCriterion("sceneId is not null");
            return (Criteria) this;
        }

        public Criteria andSceneIdEqualTo(Integer value) {
            addCriterion("sceneId =", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdNotEqualTo(Integer value) {
            addCriterion("sceneId <>", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdGreaterThan(Integer value) {
            addCriterion("sceneId >", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("sceneId >=", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdLessThan(Integer value) {
            addCriterion("sceneId <", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdLessThanOrEqualTo(Integer value) {
            addCriterion("sceneId <=", value, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdIn(List<Integer> values) {
            addCriterion("sceneId in", values, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdNotIn(List<Integer> values) {
            addCriterion("sceneId not in", values, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdBetween(Integer value1, Integer value2) {
            addCriterion("sceneId between", value1, value2, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSceneIdNotBetween(Integer value1, Integer value2) {
            addCriterion("sceneId not between", value1, value2, "sceneId");
            return (Criteria) this;
        }

        public Criteria andSkillIdIsNull() {
            addCriterion("skillId is null");
            return (Criteria) this;
        }

        public Criteria andSkillIdIsNotNull() {
            addCriterion("skillId is not null");
            return (Criteria) this;
        }

        public Criteria andSkillIdEqualTo(Integer value) {
            addCriterion("skillId =", value, "skillId");
            return (Criteria) this;
        }

        public Criteria andSkillIdNotEqualTo(Integer value) {
            addCriterion("skillId <>", value, "skillId");
            return (Criteria) this;
        }

        public Criteria andSkillIdGreaterThan(Integer value) {
            addCriterion("skillId >", value, "skillId");
            return (Criteria) this;
        }

        public Criteria andSkillIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("skillId >=", value, "skillId");
            return (Criteria) this;
        }

        public Criteria andSkillIdLessThan(Integer value) {
            addCriterion("skillId <", value, "skillId");
            return (Criteria) this;
        }

        public Criteria andSkillIdLessThanOrEqualTo(Integer value) {
            addCriterion("skillId <=", value, "skillId");
            return (Criteria) this;
        }

        public Criteria andSkillIdIn(List<Integer> values) {
            addCriterion("skillId in", values, "skillId");
            return (Criteria) this;
        }

        public Criteria andSkillIdNotIn(List<Integer> values) {
            addCriterion("skillId not in", values, "skillId");
            return (Criteria) this;
        }

        public Criteria andSkillIdBetween(Integer value1, Integer value2) {
            addCriterion("skillId between", value1, value2, "skillId");
            return (Criteria) this;
        }

        public Criteria andSkillIdNotBetween(Integer value1, Integer value2) {
            addCriterion("skillId not between", value1, value2, "skillId");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusIsNull() {
            addCriterion("enbleStatus is null");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusIsNotNull() {
            addCriterion("enbleStatus is not null");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusEqualTo(Byte value) {
            addCriterion("enbleStatus =", value, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusNotEqualTo(Byte value) {
            addCriterion("enbleStatus <>", value, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusGreaterThan(Byte value) {
            addCriterion("enbleStatus >", value, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("enbleStatus >=", value, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusLessThan(Byte value) {
            addCriterion("enbleStatus <", value, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusLessThanOrEqualTo(Byte value) {
            addCriterion("enbleStatus <=", value, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusIn(List<Byte> values) {
            addCriterion("enbleStatus in", values, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusNotIn(List<Byte> values) {
            addCriterion("enbleStatus not in", values, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusBetween(Byte value1, Byte value2) {
            addCriterion("enbleStatus between", value1, value2, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andEnbleStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("enbleStatus not between", value1, value2, "enbleStatus");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}