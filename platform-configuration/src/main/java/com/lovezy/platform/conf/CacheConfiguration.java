package com.lovezy.platform.conf;

import com.lovezy.platform.core.cache.Cache;
import com.lovezy.platform.core.cache.GuavaCache;
import com.lovezy.platform.core.cache.RedisCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Created by jin on 2017/12/23.
 */
@Configuration
@Lazy
public class CacheConfiguration {

    @Primary
    @Bean("redisCache")
    public Cache redisCache(@Lazy RedisTemplate redisTemplate) {
        return new RedisCache(redisTemplate);
    }


//    @Bean
//    public RedisTemplate redisTemplate(RedisConnectionFactory redisConnectionFactory) {
//        RedisTemplate<String, Object> template = new RedisTemplate<>();
//        template.setConnectionFactory(redisConnectionFactory);
//        template.setKeySerializer(new StringRedisSerializer());
//        template.setDefaultSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
//        template.afterPropertiesSet();
//        return template;
//    }

    @Bean("guavaCache")
    public Cache guavaCache() {
        return new GuavaCache();
    }




}
