package com.lovezy.platform.conf;

import com.lovezy.platform.core.sms.SmsProvider;
import com.lovezy.platform.core.sms.SmsSender;
import com.lovezy.platform.core.sms.provider.AliDayuSmsProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * 短信配置
 * Created by jin on 2017/11/10.
 */
@Configuration
public class SmsConfiguration {

    @Bean("alidayuSmsSender")
    @Lazy
    public SmsSender alidayuSmsSender(@Qualifier("alidayuSmsProvider") SmsProvider aliDayuSmsProvider) {
        return new SmsSender(aliDayuSmsProvider);
    }

    @Bean("alidayuSmsProvider")
    @Lazy
    public SmsProvider alidayuSmsProvider() {
        return new AliDayuSmsProvider();
    }




}
