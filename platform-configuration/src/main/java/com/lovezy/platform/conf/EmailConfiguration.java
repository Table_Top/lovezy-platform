package com.lovezy.platform.conf;

import com.lovezy.platform.core.email.EmailSender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 邮件配置
 * Created by jin on 2017/11/9.
 */
@Configuration
public class EmailConfiguration {

    @Bean
    public EmailSender emailSender() {
        return new EmailSender();
    }

}
