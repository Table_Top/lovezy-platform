package com.lovezy.platform.conf;

import com.lovezy.platform.common.prop.MessageQueueName;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import javax.jms.Queue;

/** 消息队列配置
 * Created by jin on 2017/12/17.
 */
@Configuration
public class MessageQueueConfiguration {

    @Bean("smsQueue")
    public Queue smsQueue() {
        return new ActiveMQQueue(MessageQueueName.smsQueue);
    }

    @Bean("pushQueue")
    public Queue pushQueue() {
        return new ActiveMQQueue(MessageQueueName.pushQueue);
    }

    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }
}
