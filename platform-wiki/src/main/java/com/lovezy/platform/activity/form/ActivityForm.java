package com.lovezy.platform.activity.form;

import com.lovezy.platform.core.valid.annotation.Alias;
import com.lovezy.platform.core.valid.annotation.Length;
import com.lovezy.platform.core.valid.annotation.Range;
import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/12/5.
 */
@Getter
@Setter
@ToString
public class ActivityForm {

    public static final String EDIT = "edit";

    public static final String ADD = "add";

    @Alias({EDIT})
    @Required(errorMsg = "活动ID不能为空")
    private Integer activityId;

    @Alias({ADD,EDIT})
    @Required(errorMsg = "活动名称不能为空")
    @Length(maxLength = 1024,errorMsg = "活动名称过长")
    private String title;

    @Alias({ADD,EDIT})
    @Required(errorMsg = "活动主体不能为空")
    @Length(maxLength = 10240,errorMsg = "活动主体过长")
    private String content;

    @Alias({ADD,EDIT})
    @Required(errorMsg = "用户ID不能为空")
    private String mid;

    @Range(errorMsg = "活动状态错误")
    private Byte status;
}
