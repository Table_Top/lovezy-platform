package com.lovezy.platform.activity.controller;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.activity.form.ActivityForm;
import com.lovezy.platform.activity.form.ActivityJoinForm;
import com.lovezy.platform.activity.query.ActivityJoinQuery;
import com.lovezy.platform.activity.query.ActivityQuery;
import com.lovezy.platform.activity.service.ActivityJoinService;
import com.lovezy.platform.activity.service.ActivityService;
import com.lovezy.platform.activity.vo.ActivityVo;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.core.base.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2017/12/5.
 */
@RestController
@RequestMapping("/api/activity")
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ActivityJoinService activityJoinService;

    @PostMapping({"","/"})
    public ResponseEntity<Response> createActivity(@RequestBody ActivityForm form) {
        String currentMid = SiteContext.getCurrentMid();
        form.setMid(currentMid);
        activityService.createActivity(form);
        return ResponseEntity.ok(ok());
    }

    @PutMapping({"","/"})
    public ResponseEntity<Response> editActivity(@RequestBody ActivityForm form) {
        String currentMid = SiteContext.getCurrentMid();
        form.setMid(currentMid);
        activityService.editActivity(form);
        return ResponseEntity.ok(ok());
    }

    @DeleteMapping("/")
    public ResponseEntity<Response> deleteActivity(@RequestBody ActivityForm form) {
        String currentMid = SiteContext.getCurrentMid();
        form.setMid(currentMid);
        form.setStatus(StatusEnum.DELETED.value());
        activityService.editActivity(form);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/list")
    public ResponseEntity<Response> activityList(ActivityQuery query) {
        String currentMid = SiteContext.getCurrentMidNullAble();
        query.setMid(currentMid);
        PageInfo<ActivityVo> activityList = activityService.findActivityList(query);
        return ResponseEntity.ok(ok(activityList));
    }

    @PostMapping("/join")
    public ResponseEntity<Response> joinActivity(@RequestBody ActivityJoinForm form) {
        activityJoinService.joinActivity(form);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/joinList")
    public ResponseEntity<Response> joinList(ActivityJoinQuery query) {
        String currentMid = SiteContext.getCurrentMidNullAble();
        query.setMid(currentMid);
        PageInfo<Object> list = activityJoinService.joinList(query);
        return ResponseEntity.ok(ok(list));
    }
}