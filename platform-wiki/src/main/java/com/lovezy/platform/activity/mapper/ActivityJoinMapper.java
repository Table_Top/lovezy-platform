package com.lovezy.platform.activity.mapper;

import com.lovezy.platform.activity.model.ActivityJoin;
import com.lovezy.platform.activity.model.ActivityJoinExample;
import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ActivityJoinMapper extends BaseExampleMapper<ActivityJoin, ActivityJoinExample, Integer> {
}