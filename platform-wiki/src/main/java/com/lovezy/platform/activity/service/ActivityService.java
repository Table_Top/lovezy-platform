package com.lovezy.platform.activity.service;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.activity.form.ActivityForm;
import com.lovezy.platform.activity.mapper.ActivityMapper;
import com.lovezy.platform.activity.model.Activity;
import com.lovezy.platform.activity.model.ActivityExample;
import com.lovezy.platform.activity.query.ActivityQuery;
import com.lovezy.platform.activity.vo.ActivityVo;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.utils.ObjectUtil;
import com.lovezy.platform.core.valid.ValidatorUtil;
import com.lovezy.platform.shop.model.Shop;
import com.lovezy.platform.shop.service.ShopService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

import static com.lovezy.platform.activity.exception.ActivityExceptionEnum.ACTIVITY_NOT_EXIST;
import static com.lovezy.platform.activity.exception.ActivityExceptionEnum.ACTIVITY_NO_AUTH_OPT;
import static com.lovezy.platform.core.utils.ObjectUtil.nonNull;
import static com.lovezy.platform.member.exception.MemberExceptionEnum.MEMBER_ROLE_NOT_BUSINESS;

/**
 * Created by jin on 2017/12/5.
 */
@Service
public class ActivityService extends MapperService<Activity, Integer, ActivityMapper> {

    @Autowired
    private ShopService shopService;

    public Activity createActivity(ActivityForm form) {
        ValidatorUtil.validate(form,ActivityForm.ADD);
        String mid = form.getMid();
        Shop shop = shopService.getByMemberId(mid);
        if (shop == null) {
            throw new ServiceException(MEMBER_ROLE_NOT_BUSINESS.getErrorCode(), MEMBER_ROLE_NOT_BUSINESS.getErrorMsg());
        }

        Activity activity = new Activity();
        activity.setTitle(form.getTitle());
        activity.setContent(form.getContent());
        activity.setShopId(shop.getShopId());
        activity.setCreateId(mid);
        activity.setModifyId(mid);
        Date now = new Date();
        activity.setGmtCreate(now);
        activity.setGmtModify(now);
        activity.setStatus(StatusEnum.NORMAL.value());
        mapper.insert(activity);
        return activity;
    }

    public Activity editActivity(ActivityForm form) {
        ValidatorUtil.validate(form,ActivityForm.EDIT);
        Integer activityId = form.getActivityId();
        String mid = form.getMid();
        Activity activity = mapper.selectByPrimaryKey(activityId);
        if (activity == null) {
            throw new ServiceException(ACTIVITY_NOT_EXIST.getErrorCode(), ACTIVITY_NOT_EXIST.getErrorMsg());
        }
        Integer shopId = activity.getShopId();
        shopService.isShopManager(shopId, mid).orElseThrow(() -> new ServiceException(ACTIVITY_NO_AUTH_OPT.getErrorCode(), ACTIVITY_NO_AUTH_OPT.getErrorMsg()));

        Activity act = new Activity();
        act.setActivityId(activityId);
        act.setTitle(form.getTitle());
        act.setContent(form.getContent());
        act.setModifyId(mid);
        act.setGmtModify(new Date());
        act.setStatus(form.getStatus());
        mapper.updateByPrimaryKeySelective(act);
        return act;
    }

    public PageInfo<ActivityVo> findActivityList(ActivityQuery query) {
        String shopKey = query.getShopKey();
        nonNull(shopKey);
        Shop shop = shopService.findByShopKey(shopKey).orElseThrow(() -> new ServiceException(MEMBER_ROLE_NOT_BUSINESS.getErrorCode(), MEMBER_ROLE_NOT_BUSINESS.getErrorMsg()));

        ActivityExample example = new ActivityExample();
        example.setOrderByClause("gmtCreate desc");
        example.createCriteria()
                .andShopIdEqualTo(shop.getShopId())
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        return Pager.doPageInfo(query, () ->
            mapper.selectByExample(example).stream().map(a -> ObjectUtil.copyTo(a, new ActivityVo()))
        );
    }

    public Activity findActivityById(@NotNull Integer id) {
        Activity activity = mapper.selectByPrimaryKey(id);
        if (activity == null || activity.getStatus() != StatusEnum.NORMAL.value()) {
            throw new ServiceException(ACTIVITY_NOT_EXIST.getErrorCode(), ACTIVITY_NOT_EXIST.getErrorMsg());
        }
        return activity;
    }




}
