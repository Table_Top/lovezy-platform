package com.lovezy.platform.activity.service;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.activity.form.ActivityJoinForm;
import com.lovezy.platform.activity.mapper.ActivityJoinMapper;
import com.lovezy.platform.activity.model.Activity;
import com.lovezy.platform.activity.model.ActivityJoin;
import com.lovezy.platform.activity.model.ActivityJoinExample;
import com.lovezy.platform.activity.query.ActivityJoinQuery;
import com.lovezy.platform.activity.vo.ActivityJoinVo;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.utils.ObjectUtil;
import com.lovezy.platform.core.valid.ValidatorUtil;
import com.lovezy.platform.shop.enums.ShopExceptionEnum;
import com.lovezy.platform.shop.model.Shop;
import com.lovezy.platform.shop.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.lovezy.platform.activity.exception.ActivityExceptionEnum.ACTIVITY_NO_AUTH_OPT;
import static com.lovezy.platform.member.exception.MemberExceptionEnum.MEMBER_ROLE_NOT_BUSINESS;
import static java.util.stream.Collectors.toList;

/**
 * Created by jin on 2017/12/5.
 */
@Service
public class ActivityJoinService extends MapperService<ActivityJoin, Integer, ActivityJoinMapper> {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ShopService shopService;

    public ActivityJoin joinActivity(ActivityJoinForm form) {
        ValidatorUtil.validate(form);
        Integer activityId = form.getActivityId();
        activityService.findActivityById(activityId);

        ActivityJoin join = new ActivityJoin();
        join.setActivityId(activityId);
        join.setName(form.getName());
        join.setReason(form.getReason());
        join.setTime(form.getTime());
        Date now = new Date();
        join.setGmtCreate(now);
        join.setGmtModify(now);
        join.setNum(form.getNum());
        join.setStatus(StatusEnum.NORMAL.value());

        mapper.insert(join);
        return join;
    }

    public PageInfo<Object> joinList(ActivityJoinQuery query) {
        Shop shop ;
        String shopKey = query.getShopKey();
        if (shopKey != null) {
            shop = shopService.findByShopKey(shopKey).orElseThrow(() -> new ServiceException(ShopExceptionEnum.SHOP_KEY_NOT_FOUND.getErrorCode(), ShopExceptionEnum.SHOP_KEY_NOT_FOUND.getErrorMsg()));
        } else {
            shop = shopService.getByMemberId(query.getMid());
        }
        if (shop == null) {
            throw new ServiceException(MEMBER_ROLE_NOT_BUSINESS.getErrorCode(), MEMBER_ROLE_NOT_BUSINESS.getErrorMsg());
        }
        Activity activity = activityService.findActivityById(query.getActivityId());
        if (activity == null || !activity.getShopId().equals(shop.getShopId())) {
            throw new ServiceException(ACTIVITY_NO_AUTH_OPT.getErrorCode(), ACTIVITY_NO_AUTH_OPT.getErrorMsg());
        }

        ActivityJoinExample example = new ActivityJoinExample();
        example.setOrderByClause("gmtCreate desc");
        example.createCriteria()
                .andActivityIdEqualTo(activity.getActivityId())
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        PageInfo<Object> joins = Pager.doPageInfo(query, () ->
                mapper.selectByExample(example)
        );
        List<Object> voList = joins.getList().stream().map(j -> ObjectUtil.copyTo(j, new ActivityJoinVo()))
                .collect(toList());
        joins.setList(voList);
        return joins;
    }

}
