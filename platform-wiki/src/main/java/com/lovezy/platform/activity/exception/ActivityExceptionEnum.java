package com.lovezy.platform.activity.exception;

/**
 * 用户模块异常
 * Created by jin on 2017/11/5.
 */
public enum ActivityExceptionEnum {
    ACTIVITY_NOT_EXIST ("000001","活动不存在"),
    ACTIVITY_NO_AUTH_OPT ("000002","无权限操作此活动"),
    ;
    String errorCode;
    String errorMsg;
    ActivityExceptionEnum(String errorCode, String errorMsg) {
        this.errorCode = "ACTIVITY-" + errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
