package com.lovezy.platform.activity.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * Created by jin on 2017/12/5.
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActivityVo {
    private Integer activityId;

    private Integer shopId;

    private String title;

    private String content;

    private Date gmtCreate;
}
