package com.lovezy.platform.activity.form;

import com.lovezy.platform.core.valid.annotation.Length;
import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/12/5.
 */
@Getter
@Setter
@ToString
public class ActivityJoinForm {

    @Required(errorMsg = "活动ID不能为空")
    private Integer activityId;

    @Required(errorMsg = "名称不能为空")
    @Length(maxLength = 16,errorMsg = "名称过长")
    private String name;

    @Length(maxLength = 2048,errorMsg = "理由过长")
    private String reason;

    @Length(maxLength = 2048,errorMsg = "时间描述过长")
    private String time;

    private Integer num;
}
