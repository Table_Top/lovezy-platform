package com.lovezy.platform.activity.query;

import com.lovezy.platform.core.base.query.BaseQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/12/5.
 */
@Getter
@Setter
@ToString
public class ActivityQuery extends BaseQuery{

    private String mid;

    private Integer shopId;

    private String shopKey;
}
