package com.lovezy.platform.activity.mapper;

import com.lovezy.platform.activity.model.Activity;
import com.lovezy.platform.activity.model.ActivityExample;
import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ActivityMapper extends BaseExampleMapper<Activity, ActivityExample, Integer> {
}