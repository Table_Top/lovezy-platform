package com.lovezy.platform.collection.controller;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.collection.form.CollectionForm;
import com.lovezy.platform.collection.query.CollectionQuery;
import com.lovezy.platform.collection.service.CollectionService;
import com.lovezy.platform.collection.vo.CollectionVo;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2017/12/8.
 */
@RestController
@RequestMapping("/api/collection")
public class CollectionController {

    @Autowired
    private CollectionService collectionService;

    @PostMapping("/collect")
    public ResponseEntity<Response> newCollection(@RequestBody CollectionForm form) {
        form.setMid(SiteContext.getCurrentMid());
        collectionService.newCollection(form);
        return ResponseEntity.ok(ok());
    }

    @DeleteMapping("/collect")
    public ResponseEntity<Response> removeCollection(Integer collectId) {
        CollectionForm form = new CollectionForm();
        form.setMid(SiteContext.getCurrentMid());
        form.setCollectId(collectId);
        collectionService.removeCollection(form);
        return ResponseEntity.ok(ok());
    }


    @GetMapping("/list")
    public ResponseEntity<Response> collectionList(CollectionQuery query) {
        query.setMid(SiteContext.getCurrentMid());
        PageInfo<CollectionVo> collections = collectionService.findCollections(query);
        return ResponseEntity.ok(ok(collections));
    }
}
