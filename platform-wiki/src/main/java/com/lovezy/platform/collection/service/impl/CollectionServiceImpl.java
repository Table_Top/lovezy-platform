package com.lovezy.platform.collection.service.impl;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.collection.enums.CollectionSourceType;
import com.lovezy.platform.collection.form.CollectionForm;
import com.lovezy.platform.collection.mapper.CollectionMapper;
import com.lovezy.platform.collection.model.Collection;
import com.lovezy.platform.collection.model.CollectionExample;
import com.lovezy.platform.collection.query.CollectionQuery;
import com.lovezy.platform.collection.service.CollectionService;
import com.lovezy.platform.collection.vo.CollectionVo;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.valid.ValidatorUtil;
import com.lovezy.platform.topic.service.TopicService;
import com.lovezy.platform.topic.vo.TopicVo;
import com.lovezy.platform.wiki.service.GameService;
import com.lovezy.platform.wiki.vo.WikiVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by jin on 2017/12/8.
 */
@Service
public class CollectionServiceImpl extends MapperService<Collection,Integer,CollectionMapper> implements CollectionService{

    @Autowired
    private GameService gameService;

    @Autowired
    private TopicService topicService;

    @Override
    public Collection newCollection(CollectionForm form) {
        ValidatorUtil.validate(form, CollectionForm.ADD);
        Integer sourceId = form.getSourceId();
        Byte type = form.getType();
        String mid = form.getMid();
        CollectionExample example = new CollectionExample();
        example.createCriteria()
                .andSourceIdEqualTo(sourceId)
                .andSourceTypeEqualTo(type)
                .andStatusEqualTo(StatusEnum.NORMAL.value())
                .andCreateIdEqualTo(mid);
        int count = mapper.countByExample(example);
        if (count > 0) {
            throw new ServiceException("已经收藏过啦");
        }

        Date now = new Date();
        Collection collection = new Collection();
        collection.setSourceId(sourceId);
        collection.setSourceType(type);
        collection.setCreateId(mid);
        collection.setModifyId(mid);
        collection.setGmtCreate(now);
        collection.setGmtModify(now);
        collection.setStatus(StatusEnum.NORMAL.value());
        mapper.insert(collection);
        return collection;
    }

    @Override
    public void removeCollection(CollectionForm form) {
        ValidatorUtil.validate(form, CollectionForm.REMOVE);
        Integer collectionId = form.getCollectId();
        Collection collection = mapper.selectByPrimaryKey(collectionId);
        String mid = form.getMid();
        if (collection == null || !collection.getCreateId().equals(mid)) {
            throw new ServiceException("此收藏不存在");
        }
        Collection update = new Collection();
        update.setCollectionId(collectionId);
        update.setStatus(StatusEnum.DELETED.value());
        update.setGmtModify(new Date());
        update.setModifyId(form.getMid());
        mapper.updateByPrimaryKeySelective(update);
    }

    @Override
    public PageInfo<CollectionVo> findCollections(CollectionQuery query) {
        Byte type = query.getType();
        CollectionSourceType sourceType = CollectionSourceType.valueOf(type);
        if (sourceType == null) {
            throw new ServiceException("类型参数不正确");
        }
        PageInfo<CollectionVo> collections = Pager.doPageInfo(query, () -> mapper.findCollection(query));
        if (type == CollectionSourceType.TOPIC.getValue()) {
            collections.getList().forEach(this::putTopicInfo);
        } else if (type == CollectionSourceType.WIKI.getValue()) {
            collections.getList().forEach(this::putWikiInfo);
        }
        return collections;
    }

    private void putWikiInfo(CollectionVo collection) {
        Integer gameId = collection.getSourceId();
        WikiVo wiki = gameService.findWikiById(gameId);
        collection.setData(wiki);
    }

    private void putTopicInfo(CollectionVo collection) {
        Integer topicId = collection.getSourceId();
        TopicVo topicVo = topicService.findTopicVoById(topicId);
        collection.setData(topicVo);
    }

}
