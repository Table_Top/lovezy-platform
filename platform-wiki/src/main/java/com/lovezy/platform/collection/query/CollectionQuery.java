package com.lovezy.platform.collection.query;


import com.lovezy.platform.core.base.query.BaseQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

/**
 * Created by jin on 2017/12/8.
 */
@Alias("collectionQuery")
@Getter
@Setter
@ToString
public class CollectionQuery extends BaseQuery{

    private String mid;

    private Byte type;
}
