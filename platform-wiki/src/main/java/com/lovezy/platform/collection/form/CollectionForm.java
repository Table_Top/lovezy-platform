package com.lovezy.platform.collection.form;

import com.lovezy.platform.core.valid.annotation.Alias;
import com.lovezy.platform.core.valid.annotation.Range;
import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/12/8.
 */
@Getter
@Setter
@ToString
public class CollectionForm {

    public static final String ADD = "ADD";

    public static final String REMOVE = "REMOVE";

    @Alias({REMOVE})
    @Required(errorMsg = "收藏ID不能为空")
    private Integer collectId;

    @Alias({ADD})
    @Required(errorMsg = "来源ID不能为空")
    private Integer sourceId;

    @Alias({ADD})
    @Required(errorMsg = "来源ID类型不能为空")
    @Range(errorMsg = "来源类型错误")
    private Byte type;

    @Alias({ADD,REMOVE})
    @Required(errorMsg = "用户ID不能为空")
    private String mid;

}
