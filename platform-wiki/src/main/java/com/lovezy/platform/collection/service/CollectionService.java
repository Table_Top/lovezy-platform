package com.lovezy.platform.collection.service;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.collection.form.CollectionForm;
import com.lovezy.platform.collection.model.Collection;
import com.lovezy.platform.collection.query.CollectionQuery;
import com.lovezy.platform.collection.vo.CollectionVo;

/**
 * Created by jin on 2017/12/8.
 */
public interface CollectionService {
    Collection newCollection(CollectionForm form);

    void removeCollection(CollectionForm form);

    PageInfo<CollectionVo> findCollections(CollectionQuery query);
}
