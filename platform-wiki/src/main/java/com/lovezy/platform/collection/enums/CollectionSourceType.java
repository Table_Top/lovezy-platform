package com.lovezy.platform.collection.enums;

/**
 * 1- 文章 2-桌游
 * Created by jin on 2017/12/8.
 */
public enum CollectionSourceType {
    TOPIC((byte)1),
    WIKI((byte)2)
    ;

    private byte value;

    CollectionSourceType(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }

    public static CollectionSourceType valueOf(byte value) {
        for (CollectionSourceType collectionSourceType : CollectionSourceType.values()) {
            if (collectionSourceType.value == value) {
                return collectionSourceType;
            }
        }
        return null;
    }
}
