package com.lovezy.platform.collection.mapper;

import com.lovezy.platform.collection.model.Collection;
import com.lovezy.platform.collection.model.CollectionExample;
import com.lovezy.platform.collection.query.CollectionQuery;
import com.lovezy.platform.collection.vo.CollectionVo;
import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CollectionMapper extends BaseExampleMapper<Collection, CollectionExample, Integer> {

    List<CollectionVo> findCollection(CollectionQuery query);
}