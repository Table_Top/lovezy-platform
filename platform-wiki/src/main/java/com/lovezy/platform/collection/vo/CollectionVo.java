package com.lovezy.platform.collection.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

/**
 * Created by jin on 2017/12/8.
 */
@Alias("collectionVo")
@Getter
@Setter
@ToString
public class CollectionVo {

    private Integer collectionId;

    private Integer sourceId;

    private Byte type;

    private Object data;
}
