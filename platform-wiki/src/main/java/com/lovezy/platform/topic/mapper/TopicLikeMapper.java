package com.lovezy.platform.topic.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.topic.model.TopicLike;
import com.lovezy.platform.topic.model.TopicLikeExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TopicLikeMapper extends BaseExampleMapper<TopicLike, TopicLikeExample, Integer> {

}