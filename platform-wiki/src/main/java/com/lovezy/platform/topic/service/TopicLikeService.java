package com.lovezy.platform.topic.service;

import com.lovezy.platform.topic.model.TopicLike;

/**
 * 文章话题点赞服务
 * Created by jin on 2017/12/3.
 */
public interface TopicLikeService {
    TopicLike likeTopic(Integer topicId, String mid);

    TopicLike unlikeTopic(Integer topicId, String mid);
}
