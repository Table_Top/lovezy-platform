package com.lovezy.platform.topic.service;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.topic.form.TopicForm;
import com.lovezy.platform.topic.model.Topic;
import com.lovezy.platform.topic.query.TopicQuery;
import com.lovezy.platform.topic.vo.TopicVo;
import org.jetbrains.annotations.NotNull;

/**
 * 文章话题服务
 * Created by jin on 2017/12/3.
 */
public interface TopicService {
    Topic publishTopic(TopicForm form);

    PageInfo<TopicVo> findTopicList(TopicQuery query);

    Topic findById(Integer topicId);

    TopicVo findTopicVoById(@NotNull Integer topicId);
}
