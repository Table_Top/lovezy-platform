package com.lovezy.platform.topic.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.topic.model.TopicReplyLike;
import com.lovezy.platform.topic.model.TopicReplyLikeExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TopicReplyLikeMapper extends BaseExampleMapper<TopicReplyLike, TopicReplyLikeExample, Integer> {

}