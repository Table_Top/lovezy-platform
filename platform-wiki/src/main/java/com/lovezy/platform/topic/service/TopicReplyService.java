package com.lovezy.platform.topic.service;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.topic.form.TopicReplyForm;
import com.lovezy.platform.topic.model.TopicReply;
import com.lovezy.platform.topic.query.TopicReplyQuery;
import com.lovezy.platform.topic.vo.TopicReplyVo;

/**
 * 文章话题评论服务
 * Created by jin on 2017/12/3.
 */
public interface TopicReplyService {
    TopicReply replyTopic(TopicReplyForm form);

    PageInfo<TopicReplyVo> replyList(TopicReplyQuery query);

    void deleteReply(TopicReplyQuery query);

    TopicReply findById(Integer replyId);
}
