package com.lovezy.platform.topic;

import com.lovezy.platform.topic.video.Bilibili;
import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jin on 2017/12/21.
 */
@Configuration
public class TopicConfiguration {


    @Bean
    public Bilibili bilibili() {
        Bilibili bilibili = Feign.builder()
                .options(new Request.Options(1000, 3500))
                .retryer(new Retryer.Default(5000, 5000, 3))
                .logger(new Logger.JavaLogger())
                .logLevel(Logger.Level.BASIC)
                .target(Bilibili.class, "http://9bl.bakayun.cn");
        return bilibili;
    }

}
