package com.lovezy.platform.topic.service;

import com.lovezy.platform.topic.model.TopicReplyLike;

/**
 * 文章话题评论点赞服务
 * Created by jin on 2017/12/3.
 */
public interface TopicReplyLikeService {
    TopicReplyLike likeTopicReply(Integer replyId, String mid);

    TopicReplyLike unlikeTopic(Integer replyId, String mid);
}
