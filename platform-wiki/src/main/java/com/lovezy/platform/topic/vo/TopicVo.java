package com.lovezy.platform.topic.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lovezy.platform.core.spring.converter.EnumToStringJsonSerializer;
import com.lovezy.platform.topic.enums.TopicTypeEnum;
import com.lovezy.platform.wiki.vo.WikiVo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * Created by jin on 2017/12/6.
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Alias("topicVo")
public class TopicVo {

    private WikiVo wiki;

    private Integer topicId;

    private String title;

    @JsonSerialize(using = EnumToStringJsonSerializer.class)
    private TopicTypeEnum topicType;

    private String body;

    private String video;

    private String cover;

    private String viewUrl;

    private String createId;

    private Date gmtCreate;

    private Integer replyCnt;

    private Integer likeCnt;

    private Byte liked;

}
