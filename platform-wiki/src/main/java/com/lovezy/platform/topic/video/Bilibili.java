package com.lovezy.platform.topic.video;

import feign.Param;
import feign.RequestLine;

/**
 * 哔哩哔哩接口
 * Created by jin on 2017/12/21.
 */
public interface Bilibili {

    @RequestLine("GET /API/GetVideoInfo.php?aid={aid}&p=1&type=json ")
    String videoCover(@Param("aid") String aid);

}
