package com.lovezy.platform.topic.enums;

import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.spring.SpringContext;
import com.lovezy.platform.topic.video.TopicSourceHandler;
import com.lovezy.platform.topic.video.BiliTopicHandler;
import com.lovezy.platform.topic.video.SelfTopicHandler;
import com.lovezy.platform.topic.video.YoukuTopicHandler;

/**
 * 话题视频来源
 * Created by jin on 2017/12/21.
 */
public enum TopicVideoSourceType {

    BILIBILI((byte)1, BiliTopicHandler.class),
    YOUKU((byte)2, YoukuTopicHandler.class),
    SELF((byte)3, SelfTopicHandler.class),
    ;

    private byte type;

    private Class<? extends TopicSourceHandler> handler;

    TopicVideoSourceType(byte type, Class<? extends TopicSourceHandler> handler) {
        this.type = type;
        this.handler = handler;
    }

    public byte getType() {
        return type;
    }

    public Class<? extends TopicSourceHandler> getHandler() {
        return handler;
    }

    public TopicSourceHandler getHandlerInstance() {
        return SpringContext.getContext().getBean(handler);
    }

    public static TopicVideoSourceType valueOf(byte type) {
        for (TopicVideoSourceType topicVideoSourceType : TopicVideoSourceType.values()) {
            if (topicVideoSourceType.type == type) {
                return topicVideoSourceType;
            }
        }
        throw new ServiceException("话题类别错误");
    }
}
