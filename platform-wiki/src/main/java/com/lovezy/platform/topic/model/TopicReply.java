package com.lovezy.platform.topic.model;

import java.util.Date;

public class TopicReply {
    private Integer replyId;

    private Integer parentId;

    private Integer topicId;

    private String text;

    private Integer upReplyId;

    private String upReplyer;

    private Byte status;

    private Date gmtCreate;

    private Date gmtModify;

    private String createId;

    private String modifyId;

    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId == null ? null : createId.trim();
    }

    public String getModifyId() {
        return modifyId;
    }

    public void setModifyId(String modifyId) {
        this.modifyId = modifyId == null ? null : modifyId.trim();
    }

    public Integer getUpReplyId() {
        return upReplyId;
    }

    public void setUpReplyId(Integer upReplyId) {
        this.upReplyId = upReplyId;
    }

    public String getUpReplyer() {
        return upReplyer;
    }

    public void setUpReplyer(String upReplyer) {
        this.upReplyer = upReplyer;
    }

}