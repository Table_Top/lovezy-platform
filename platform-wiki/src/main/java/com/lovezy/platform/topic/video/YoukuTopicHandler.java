package com.lovezy.platform.topic.video;

import com.lovezy.platform.topic.video.BaseTopicSourceHandler;
import org.springframework.stereotype.Component;

/**
 * Youku 处理类
 * Created by jin on 2017/12/21.
 */
@Component("youkuTopicHandler")
public class YoukuTopicHandler extends BaseTopicSourceHandler{
}
