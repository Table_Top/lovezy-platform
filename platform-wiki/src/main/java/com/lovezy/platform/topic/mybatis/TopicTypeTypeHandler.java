package com.lovezy.platform.topic.mybatis;

import com.lovezy.platform.topic.enums.TopicTypeEnum;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by jin on 2017/12/6.
 */

@MappedJdbcTypes({JdbcType.TINYINT})
@MappedTypes({TopicTypeEnum.class})
public class TopicTypeTypeHandler extends BaseTypeHandler<TopicTypeEnum>{
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, TopicTypeEnum parameter, JdbcType jdbcType) throws SQLException {
        ps.setByte(i,parameter.value());
    }

    @Override
    public TopicTypeEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return TopicTypeEnum.valueOf(rs.getByte(columnName));
    }

    @Override
    public TopicTypeEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return TopicTypeEnum.valueOf(rs.getByte(columnIndex));

    }

    @Override
    public TopicTypeEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return TopicTypeEnum.valueOf(cs.getByte(columnIndex));

    }
}
