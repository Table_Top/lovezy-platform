package com.lovezy.platform.topic.exception;

/**
 * Created by jin on 2017/12/6.
 */
public enum TopicExceptionEnum {
    TOPIC_GAME_NOT_FOUND ("000001","桌游不存在"),
    TOPIC_HAS_ALREADY_LIKED ("000002","已经点赞过啦"),
    TOPIC_NOT_LIKED ("000003","未点赞过"),
    REPLY_NOT_FOUND ("000004","评论不存在"),
    TOPIC_NOT_FOUND ("000005","话题不存在"),
    CNT_DELETE_REPLY ("000006","无法删除此回复"),

    ;
    String errorCode;
    String errorMsg;
    TopicExceptionEnum(String errorCode, String errorMsg) {
        this.errorCode = "TOPIC-" + errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
