package com.lovezy.platform.topic.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

import java.util.Date;
import java.util.List;

/**
 * Created by jin on 2017/12/6.
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Alias("topicReplyVo")
public class TopicReplyVo {

    private Integer replyId;

    private String memberId;

    private String avatar;

    private String nickname;

    private Integer upReplyId;

    private String upReplyer;

    private String upReplyerName;

    private Integer likeCnt;

    private String text;

    private Integer replyCnt;

    private Integer topicId;

    private Date gmtCreate;

    private Byte liked;

    private List<TopicReplyVo> childReplys;

}
