package com.lovezy.platform.topic.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.topic.model.TopicReply;
import com.lovezy.platform.topic.model.TopicReplyExample;
import com.lovezy.platform.topic.query.TopicReplyQuery;
import com.lovezy.platform.topic.vo.TopicReplyVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TopicReplyMapper extends BaseExampleMapper<TopicReply, TopicReplyExample, Integer> {

    List<TopicReplyVo> findReplyList(TopicReplyQuery query);

    List<TopicReplyVo> findSubReply(TopicReplyQuery query);

    String findReplyContextById(Integer replyId);

}