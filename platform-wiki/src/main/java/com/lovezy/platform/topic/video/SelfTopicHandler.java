package com.lovezy.platform.topic.video;

import com.lovezy.platform.topic.video.BaseTopicSourceHandler;
import org.springframework.stereotype.Component;

/**
 * 自身视频话题 处理类
 * Created by jin on 2017/12/21.
 */
@Component("selfTopicHandler")
public class SelfTopicHandler extends BaseTopicSourceHandler{
}
