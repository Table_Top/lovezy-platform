package com.lovezy.platform.topic.controller;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.topic.form.TopicReplyForm;
import com.lovezy.platform.topic.query.TopicReplyQuery;
import com.lovezy.platform.topic.service.TopicReplyLikeService;
import com.lovezy.platform.topic.service.TopicReplyService;
import com.lovezy.platform.topic.vo.TopicReplyVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2017/12/7.
 */
@RestController
@RequestMapping("/api/topic/reply")
public class TopicReplyController {

    @Autowired
    private TopicReplyService topicReplyService;

    @Autowired
    private TopicReplyLikeService topicReplyLikeService;

    @PostMapping({"","/"})
    public ResponseEntity<Response> replyTopic(@RequestBody TopicReplyForm form) {
        form.setMid(SiteContext.getCurrentMid());
        topicReplyService.replyTopic(form);
        return ResponseEntity.ok(ok());
    }

    @DeleteMapping({"","/"})
    public ResponseEntity<Response> deleteReply(Integer replyId) {
        TopicReplyQuery query = new TopicReplyQuery();
        query.setReplyId(replyId);
        query.setMid(SiteContext.getCurrentMid());
        topicReplyService.deleteReply(query);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/list")
    public ResponseEntity<Response> findReplys(TopicReplyQuery query) {
        query.setMid(SiteContext.getCurrentMid());
        PageInfo<TopicReplyVo> replys = topicReplyService.replyList(query);
        return ResponseEntity.ok(ok(replys));
    }

    @PostMapping("/like")
    public ResponseEntity<Response> likeReply(@RequestBody Map<String,Integer> param) {
        topicReplyLikeService.likeTopicReply(param.get("replyId"), SiteContext.getCurrentMid());
        return ResponseEntity.ok(ok());
    }

    @DeleteMapping("/like")
    public ResponseEntity<Response> unlikeReply(@RequestBody Map<String,Integer> param) {
        topicReplyLikeService.unlikeTopic(param.get("replyId"), SiteContext.getCurrentMid());
        return ResponseEntity.ok(ok());
    }

}
