package com.lovezy.platform.topic.query;

import com.lovezy.platform.core.base.query.BaseQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

/**
 * Created by jin on 2017/12/6.
 */
@Getter
@Setter
@ToString
@Alias("topicQuery")
public class TopicQuery extends BaseQuery{

    private Integer topicId;

    private Byte topicType;

    private Integer gameId;

    /**
     * 1-最新 2-关注
     */
    private Byte type;

    private String mid;

}
