package com.lovezy.platform.topic.model;

import java.util.Date;

public class TopicReplyLike {
    private Integer replyLikeId;

    private Integer topicId;

    private Integer replyId;

    private Byte status;

    private String createId;

    private Date gmtCreate;

    public Integer getReplyLikeId() {
        return replyLikeId;
    }

    public void setReplyLikeId(Integer replyLikeId) {
        this.replyLikeId = replyLikeId;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId == null ? null : createId.trim();
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }
}