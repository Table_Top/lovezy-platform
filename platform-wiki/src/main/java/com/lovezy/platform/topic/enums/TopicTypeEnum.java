package com.lovezy.platform.topic.enums;

/**
 * Created by jin on 2017/12/6.
 */
public enum TopicTypeEnum {
    GAME_ANALYSE ((byte)1,"游戏复盘"),
    GAME_STRATEGY ((byte)2,"游戏攻略")
    ;

    private byte value;

    private String name;

    TopicTypeEnum(byte v,String name) {
        this.value = v;
        this.name = name;
    }

    public byte value() {
        return value;
    }

    public String getName() {
        return name;
    }

    public static TopicTypeEnum valueOf(byte value) {
        for (TopicTypeEnum topicTypeEnum : TopicTypeEnum.values()) {
            if (topicTypeEnum.value == value) {
                return topicTypeEnum;
            }
        }
        return null;
    }


    @Override
    public String toString() {
        return this.name;
    }
}
