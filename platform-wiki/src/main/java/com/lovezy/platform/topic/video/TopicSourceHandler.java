package com.lovezy.platform.topic.video;

import org.jetbrains.annotations.Nullable;

/**
 * 话题来源处理类
 * Created by jin on 2017/12/21.
 */
public interface TopicSourceHandler {

    @Nullable
    String getCover(String formCover);

    default String videoUrl(String formVideoUrl) {
        if (formVideoUrl == null) {
            return null;
        }
        return formVideoUrl;
    }

}
