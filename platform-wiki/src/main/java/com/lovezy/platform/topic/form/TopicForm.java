package com.lovezy.platform.topic.form;

import com.lovezy.platform.core.valid.annotation.Alias;
import com.lovezy.platform.core.valid.annotation.Length;
import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/12/6.
 */
@Getter
@Setter
@ToString
public class TopicForm {

    public final static String ADD = "add";

    public final static String EDIT = "edit";


    @Alias({EDIT})
    @Required(errorMsg = "id不能为空")
    private Integer topicId;

    private String title;

    @Alias({EDIT,ADD})
    @Required(errorMsg = "类别不能空")
    private Byte topicType;

    @Alias({EDIT,ADD})
    @Length(maxLength = 10240,errorMsg = "内容格式错误")
    private String body;

    @Alias({EDIT,ADD})
    @Length(maxLength = 1024,errorMsg = "视频连接格式错误")
    private String video;

//    @Alias({ADD})
//    @Required(errorMsg = "关联桌游不能为空")
    private Integer gameId;

    @Alias({EDIT,ADD})
    @Required(errorMsg = "用户不能为空")
    private String mid;

    @Alias({EDIT,ADD})
    @Length(maxLength = 1024,errorMsg = "视频封面长度过长")
    private String cover;

    @Alias({EDIT,ADD})
//    @Required(errorMsg = "视频来源为空")
    private Byte sourceType;
}
