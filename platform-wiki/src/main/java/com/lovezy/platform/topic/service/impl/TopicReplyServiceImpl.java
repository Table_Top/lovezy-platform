package com.lovezy.platform.topic.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.utils.ArrayUtil;
import com.lovezy.platform.core.valid.ValidatorUtil;
import com.lovezy.platform.filestore.service.FileHelper;
import com.lovezy.platform.notice.enums.NoticeSourceTypeEnum;
import com.lovezy.platform.notice.form.NoticeForm;
import com.lovezy.platform.notice.service.NoticeService;
import com.lovezy.platform.topic.form.TopicReplyForm;
import com.lovezy.platform.topic.mapper.TopicReplyMapper;
import com.lovezy.platform.topic.model.TopicReply;
import com.lovezy.platform.topic.query.TopicReplyQuery;
import com.lovezy.platform.topic.service.TopicReplyService;
import com.lovezy.platform.topic.service.TopicService;
import com.lovezy.platform.topic.vo.TopicReplyVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static com.lovezy.platform.topic.exception.TopicExceptionEnum.CNT_DELETE_REPLY;
import static com.lovezy.platform.topic.exception.TopicExceptionEnum.REPLY_NOT_FOUND;

/**
 * 文章话题评论服务实现
 * Created by jin on 2017/12/3.
 */
@Service
public class TopicReplyServiceImpl extends MapperService<TopicReply, Integer, TopicReplyMapper> implements TopicReplyService {

    @Autowired
    private TopicService topicService;

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private FileHelper fileHelper;

    @Override
    @Transactional
    public TopicReply replyTopic(TopicReplyForm form) {
        ValidatorUtil.validate(form);
        Integer parentId = form.getParentId();
        Integer topicId = form.getTopicId();
        topicService.findById(topicId);
        TopicReply parent = null;
        if (parentId != null) {
            parent = findById(parentId);
        }

        Date now = new Date();
        TopicReply reply = new TopicReply();
        reply.setTopicId(topicId);
        reply.setText(form.getText());
        reply.setParentId(parentId);
        reply.setCreateId(form.getMid());
        reply.setModifyId(form.getMid());
        reply.setGmtCreate(now);
        reply.setGmtModify(now);
        reply.setStatus(StatusEnum.NORMAL.value());
        mapper.insert(reply);

        if (parent != null) {
            noticeService.newNotice(
                    new NoticeForm(reply.getReplyId(), NoticeSourceTypeEnum.TOPIC_REPLY, parent.getCreateId(), reply.getCreateId())
            );
        }
        return reply;
    }

    @Override
    public PageInfo<TopicReplyVo> replyList(TopicReplyQuery query) {
        PageInfo<TopicReplyVo> replys = Pager.doPageInfo(query, () -> mapper.findReplyList(query));
        changeTopicReplyVoFileDownloadUrl(replys.getList());
        replys.getList()
                .forEach(this::findSubReply);
        return replys;
    }

    private void findSubReply(TopicReplyVo reply) {
        TopicReplyQuery query = new TopicReplyQuery();
        query.setParentId(reply.getReplyId());
        query.setPageSize(2);
        query.setPage(1);
        Page<TopicReplyVo> subReplys = Pager.doPage(query, () -> mapper.findSubReply(query));
        changeTopicReplyVoFileDownloadUrl(subReplys);
        reply.setChildReplys(subReplys.getResult());
    }

    private void changeTopicReplyVoFileDownloadUrl(List<TopicReplyVo> replys) {
        if (ArrayUtil.isEmpty(replys)) {
            return;
        }
        replys.stream()
                .filter(r -> r.getAvatar() != null)
                .forEach(r-> {
                    String downloadUrl = fileHelper.toDownloadUrl(r.getAvatar());
                    r.setAvatar(downloadUrl);
                });
    }


    @Override
    public void deleteReply(TopicReplyQuery query) {
        Integer replyId = query.getReplyId();
        TopicReply reply = findById(replyId);
        if (!reply.getCreateId().equals(query.getMid())) {
            throw new ServiceException(CNT_DELETE_REPLY.getErrorCode(), CNT_DELETE_REPLY.getErrorMsg());
        }
        TopicReply update = new TopicReply();
        update.setReplyId(replyId);
        update.setStatus(StatusEnum.DELETED.value());
        update.setGmtModify(new Date());
        mapper.updateByPrimaryKeySelective(update);
    }

    @Override
    public TopicReply findById(Integer replyId) {
        TopicReply topicReply = mapper.selectByPrimaryKey(replyId);
        if (topicReply == null) {
            throw new ServiceException(REPLY_NOT_FOUND.getErrorCode(), REPLY_NOT_FOUND.getErrorCode());
        }
        return topicReply;
    }

}
