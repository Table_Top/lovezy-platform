package com.lovezy.platform.topic.query;

import com.lovezy.platform.core.base.query.BaseQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

/**
 * Created by jin on 2017/12/6.
 */
@Getter
@Setter
@ToString
@Alias("topicReplyQuery")
public class TopicReplyQuery extends BaseQuery{

    private Integer topicId;

    private String mid;

    private Integer parentId;

    private Byte isHot;

    private Integer replyId;

}
