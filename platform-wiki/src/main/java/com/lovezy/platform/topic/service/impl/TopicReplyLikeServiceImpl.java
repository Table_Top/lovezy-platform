package com.lovezy.platform.topic.service.impl;

import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.notice.enums.NoticeSourceTypeEnum;
import com.lovezy.platform.notice.form.NoticeForm;
import com.lovezy.platform.notice.service.NoticeService;
import com.lovezy.platform.topic.mapper.TopicReplyLikeMapper;
import com.lovezy.platform.topic.model.TopicReply;
import com.lovezy.platform.topic.model.TopicReplyLike;
import com.lovezy.platform.topic.model.TopicReplyLikeExample;
import com.lovezy.platform.topic.service.TopicReplyLikeService;
import com.lovezy.platform.topic.service.TopicReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

import static com.lovezy.platform.topic.exception.TopicExceptionEnum.TOPIC_HAS_ALREADY_LIKED;
import static com.lovezy.platform.topic.exception.TopicExceptionEnum.TOPIC_NOT_LIKED;

/**
 * 文章话题评论服务实现
 * Created by jin on 2017/12/3.
 */
@Service
public class TopicReplyLikeServiceImpl extends MapperService<TopicReplyLike, Integer, TopicReplyLikeMapper> implements TopicReplyLikeService {

    @Autowired
    private TopicReplyService topicReplyService;

    @Autowired
    private NoticeService noticeService;

    @Override
    @Transactional
    public TopicReplyLike likeTopicReply(Integer replyId, String mid) {
        TopicReply reply = topicReplyService.findById(replyId);
        Integer topicId = reply.getTopicId();

        Optional<TopicReplyLike> topicLike = hasLiked(replyId, mid);
        if (topicLike.isPresent()) {
            throw new ServiceException(TOPIC_HAS_ALREADY_LIKED.getErrorCode(), TOPIC_HAS_ALREADY_LIKED.getErrorMsg());
        }
        TopicReplyLike like = new TopicReplyLike();
        Date now = new Date();
        like.setReplyId(replyId);
        like.setCreateId(mid);
        like.setTopicId(topicId);
        like.setGmtCreate(now);
        like.setStatus(StatusEnum.NORMAL.value());
        mapper.insert(like);

        noticeService.newNotice(
                new NoticeForm(reply.getReplyId(), NoticeSourceTypeEnum.TOPIC_LIKE, reply.getCreateId(), mid)
        );
        return like;
    }

    @Override
    public TopicReplyLike unlikeTopic(Integer replyId, String mid) {
        TopicReplyLike topicReplyLike = hasLiked(replyId, mid).orElseThrow(() -> new ServiceException(TOPIC_NOT_LIKED.getErrorCode(), TOPIC_NOT_LIKED.getErrorMsg()));
        TopicReplyLike update = new TopicReplyLike();
        update.setReplyLikeId(topicReplyLike.getReplyLikeId());
        update.setStatus(StatusEnum.DELETED.value());
        mapper.updateByPrimaryKeySelective(update);
        return topicReplyLike;
    }

    private Optional<TopicReplyLike> hasLiked(Integer replyId, String mid) {
        TopicReplyLikeExample example = new TopicReplyLikeExample();
        example.createCriteria()
                .andReplyIdEqualTo(replyId)
                .andCreateIdEqualTo(mid)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        return Pager.doSingleton(() -> mapper.selectByExample(example));
    }

}
