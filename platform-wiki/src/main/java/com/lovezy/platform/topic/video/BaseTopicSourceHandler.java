package com.lovezy.platform.topic.video;

import com.lovezy.platform.filestore.service.FileService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by jin on 2017/12/21.
 */
public abstract class BaseTopicSourceHandler implements TopicSourceHandler {

    protected final Log log = LogFactory.getLog(this.getClass());

    @Autowired
    protected FileService fileService;

    @Override
    public String getCover(String formCover) {
        if (formCover == null) {
            return null;
        }
        fileService.findFileByUrl(formCover);
        return formCover;
    }
}
