package com.lovezy.platform.topic.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.topic.model.Topic;
import com.lovezy.platform.topic.model.TopicExample;
import com.lovezy.platform.topic.query.TopicQuery;
import com.lovezy.platform.topic.vo.TopicVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TopicMapper extends BaseExampleMapper<Topic, TopicExample, Integer> {

    List<TopicVo> findTopicList(TopicQuery query);

    TopicVo findTopicById(TopicQuery query);

}