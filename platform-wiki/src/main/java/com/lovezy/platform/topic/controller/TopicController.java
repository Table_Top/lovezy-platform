package com.lovezy.platform.topic.controller;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.topic.form.TopicForm;
import com.lovezy.platform.topic.query.TopicQuery;
import com.lovezy.platform.topic.service.TopicLikeService;
import com.lovezy.platform.topic.service.TopicService;
import com.lovezy.platform.topic.vo.TopicVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2017/12/7.
 */
@RestController
@RequestMapping("/api/topic")
public class TopicController {

    @Autowired
    private TopicService topicService;

    @Autowired
    private TopicLikeService topicLikeService;

    @GetMapping("/detail")
    public ResponseEntity<Response> topicDetail(Integer topicId) {
        TopicVo topicVo = topicService.findTopicVoById(topicId);
        return ResponseEntity.ok(ok(topicVo));
    }

    @PostMapping("/add")
    public ResponseEntity<Response> publishTopic(@RequestBody TopicForm form) {
        form.setMid(SiteContext.getCurrentMid());
        topicService.publishTopic(form);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/list")
    public ResponseEntity<Response> topicList(TopicQuery query) {
        query.setMid(SiteContext.getCurrentMidNullAble());
        PageInfo<TopicVo> topicList = topicService.findTopicList(query);
        return ResponseEntity.ok(ok(topicList));
    }

    @PostMapping("/like")
    public ResponseEntity<Response> likeTopic(@RequestBody Map<String,Integer> params) {
        topicLikeService.likeTopic(params.get("topicId"), SiteContext.getCurrentMid());
        return ResponseEntity.ok(ok());
    }

    @DeleteMapping("/like")
    public ResponseEntity<Response> unlikeTopic(@RequestBody Map<String,Integer> params) {
        topicLikeService.unlikeTopic(params.get("topicId"), SiteContext.getCurrentMid());
        return ResponseEntity.ok(ok());
    }


}
