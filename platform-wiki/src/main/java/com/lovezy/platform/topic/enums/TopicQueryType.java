package com.lovezy.platform.topic.enums;

/**
 * 1-最新 2-关注
 * Created by jin on 2017/12/6.
 */
public enum TopicQueryType {
    NEW ((byte)1),
    LIKE((byte)2)
    ;

    private byte value;


    TopicQueryType(byte v) {
        this.value = v;
    }

    public byte value() {
        return value;
    }
}
