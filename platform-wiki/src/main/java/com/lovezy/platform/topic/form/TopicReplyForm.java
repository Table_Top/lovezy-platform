package com.lovezy.platform.topic.form;

import com.lovezy.platform.core.valid.annotation.Length;
import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/12/6.
 */
@Getter
@Setter
@ToString
public class TopicReplyForm {

    @Required(errorMsg = "id不能为空")
    private Integer topicId;

    private Integer parentId;

    @Length(maxLength = 10240,errorMsg = "内容格式错误")
    private String text;

    @Required(errorMsg = "用户id不能为空")
    private String mid;
}
