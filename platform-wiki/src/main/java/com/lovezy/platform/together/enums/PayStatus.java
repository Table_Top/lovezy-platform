package com.lovezy.platform.together.enums;

/**
 * 付款状态 1-未付款 2-已付款
 * Created by jin on 2017/11/20.
 */
public enum PayStatus {
    NOT_PAID((byte)1),
    PAID((byte)2),
    ;
    byte value;

    PayStatus(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
