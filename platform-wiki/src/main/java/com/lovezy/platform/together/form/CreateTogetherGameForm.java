package com.lovezy.platform.together.form;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * Created by jin on 2017/11/19.
 */
@Getter
@Setter
@ToString
public class CreateTogetherGameForm {

    private String npc;

    private Integer shopId;

    private Integer gameId;

    private Integer total;

    private Date time;

    private String createId;

}
