package com.lovezy.platform.together.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.together.model.TogetherDetail;
import com.lovezy.platform.together.model.TogetherDetailExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TogetherDetailMapper extends BaseExampleMapper<TogetherDetail, TogetherDetailExample, Integer> {

}