package com.lovezy.platform.together.form;

import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/11/20.
 */
@Getter
@Setter
@ToString
public class DealApplyForm {

    @Required(errorMsg = "约局ID不能为空")
    private Integer togetherId;

    @Required(errorMsg = "用户ID不能为空")
    private String memberId;

    private Integer shopId;

    @Required(errorMsg = "操作状态不能为空")
    private Byte applyStatus;

}
