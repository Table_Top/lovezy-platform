package com.lovezy.platform.together.service;

import com.lovezy.platform.together.model.TogetherDetail;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * 约局人详情接口
 * Created by jin on 2017/11/20.
 */
public interface TogetherDetailService {
    TogetherDetail addOne(@NotNull Integer togetherId, @NotNull String mid);

    Optional<TogetherDetail> findById(Integer id);

    void removeOne(@NotNull Integer detailId, @NotNull String mid);

    boolean hasExistInTogether(String mid, Integer togetherId);
}
