package com.lovezy.platform.together.model;

import java.util.Date;

public class TogetherDetail {
    private Integer togetherDetailId;

    private Integer togetherId;

    private String memberId;

    private Byte payStatus;

    private Byte status;

    private Date gmtCreate;

    private Date gmtModify;

    private String createId;

    private String modifyId;

    public Integer getTogetherDetailId() {
        return togetherDetailId;
    }

    public void setTogetherDetailId(Integer togetherDetailId) {
        this.togetherDetailId = togetherDetailId;
    }

    public Integer getTogetherId() {
        return togetherId;
    }

    public void setTogetherId(Integer togetherId) {
        this.togetherId = togetherId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public Byte getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Byte payStatus) {
        this.payStatus = payStatus;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId == null ? null : createId.trim();
    }

    public String getModifyId() {
        return modifyId;
    }

    public void setModifyId(String modifyId) {
        this.modifyId = modifyId == null ? null : modifyId.trim();
    }
}