package com.lovezy.platform.together.exception;

/**
 * Created by jin on 2017/11/15.
 */
public enum TogetherExceptionEnum {
    NO_AUTH_TO_HANDLE_TOGETHER_APPLY ("000001","无权限处理此约局申请"),
    TOGETHER_NOT_FOUND ("000002","此约局不存在"),
    TOGETHER_IS_ALREADY_REVIEWED ("000003","此约局已被审核"),
    NO_AUTH_TO_HANDLE_TOGETHER ("000004","无权限处理此约局"),
    HAS_EXIST_IN_TOGETHER ("000005","已在此约局中"),
    CANT_JOIN_NOT_AGREE_TOGETHER ("000006","无法加入未通过审核的约局"),
    HAS_NOT_EXIST_IN_TOGETHER ("000007","您还未加入此约局"),

    ;
    private String errorCode;
    private String errorMsg;

    TogetherExceptionEnum(String errorCode, String errorMsg) {
        this.errorCode = "TOGETHER-" + errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
