package com.lovezy.platform.together.enums;

/**
 * 约局申请状态 申请状态 1-申请中 2-同意 3-拒绝 4-取消
 * Created by jin on 2017/11/20.
 */
public enum ApplyStatus {
    APPLYING((byte)1),
    AGREE((byte)2),
    REJECT((byte)3),
    CANCEL((byte)4),
    ;
    byte value;

    ApplyStatus(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
