package com.lovezy.platform.together.model;

import java.util.Date;

public class Together {
    private Integer togetherId;

    private String npc;

    private Integer shopId;

    private Integer gameId;

    private Integer total;

    private Integer confirmTotal;

    private Date time;

    private Byte status;

    private Byte applyStatus;

    private Date gmtCreate;

    private Date gmtModify;

    private String createId;

    private String modifyId;

    public Integer getTogetherId() {
        return togetherId;
    }

    public void setTogetherId(Integer togetherId) {
        this.togetherId = togetherId;
    }

    public String getNpc() {
        return npc;
    }

    public void setNpc(String npc) {
        this.npc = npc == null ? null : npc.trim();
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getConfirmTotal() {
        return confirmTotal;
    }

    public void setConfirmTotal(Integer confirmTotal) {
        this.confirmTotal = confirmTotal;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(Byte applyStatus) {
        this.applyStatus = applyStatus;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId == null ? null : createId.trim();
    }

    public String getModifyId() {
        return modifyId;
    }

    public void setModifyId(String modifyId) {
        this.modifyId = modifyId == null ? null : modifyId.trim();
    }
}