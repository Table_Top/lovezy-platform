package com.lovezy.platform.together.form;

import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 加入约局表单
 * Created by jin on 2017/11/20.
 */
@Getter
@Setter
@ToString
public class JoinTogetherForm {

    @Required(errorMsg = "用户id不能为空")
    private String memberId;

    @Required(errorMsg = "约局ID不能为空")
    private Integer togetherId;

}
