package com.lovezy.platform.together.exception;

import com.lovezy.platform.core.exception.BaseRuntimeException;

/**
 * 约局异常
 * Created by jin on 2017/11/20.
 */
public class TogetherException extends BaseRuntimeException{

    public TogetherException(TogetherExceptionEnum exception) {
        super(exception.getErrorCode(),exception.getErrorMsg());
    }

    public TogetherException(TogetherExceptionEnum exception,String errorMsg) {
        super(exception.getErrorCode(), errorMsg);
    }
}
