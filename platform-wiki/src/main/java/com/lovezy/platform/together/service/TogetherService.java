package com.lovezy.platform.together.service;

import com.lovezy.platform.together.form.CreateTogetherGameForm;
import com.lovezy.platform.together.form.DealApplyForm;
import com.lovezy.platform.together.form.JoinTogetherForm;
import com.lovezy.platform.together.form.QuitTogetherForm;
import com.lovezy.platform.together.model.Together;

import java.util.Optional;

/**
 * Created by jin on 2017/11/20.
 */
public interface TogetherService {

    Together addNewTogetherGame(CreateTogetherGameForm f);

    boolean isTogetherBelongToShop(Integer togetherId,Integer shopId);

    Optional<Together> findById(Integer togetherId);

    void agreeTogetherGameApply(DealApplyForm form);

    void rejectTogetherGameApply(DealApplyForm form);

    void cancelTogetherGameApply(DealApplyForm form);

    void joinTogetherGame(JoinTogetherForm form);

    void cancelJoinTogetherGame(QuitTogetherForm form);
}
