package com.lovezy.platform.together.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.together.model.Together;
import com.lovezy.platform.together.model.TogetherExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TogetherMapper extends BaseExampleMapper<Together, TogetherExample, Integer> {

}