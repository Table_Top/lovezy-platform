package com.lovezy.platform.wiki.service;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.wiki.form.GameInfoForm;
import com.lovezy.platform.wiki.model.GameTrait;
import com.lovezy.platform.wiki.query.TraitQuery;
import com.lovezy.platform.wiki.vo.WikiTraitVo;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * 游戏特质
 * Created by jin on 2017/11/7.
 */
public interface GameTraitService {
    List<GameTrait> saveGameTrait(GameInfoForm form);

    @Nullable
    GameTrait selectById(@NotNull Integer traitId);

    PageInfo<WikiTraitVo> traitList(TraitQuery query);
}
