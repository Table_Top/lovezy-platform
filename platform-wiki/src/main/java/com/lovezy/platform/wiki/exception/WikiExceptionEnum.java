package com.lovezy.platform.wiki.exception;

import com.lovezy.platform.core.base.Tuple;

/**
 * 百科异常枚举
 * Created by jin on 2017/11/7.
 */
public enum WikiExceptionEnum {
    WIKI_NOT_FOUND_EXCEPTION("000001","百科不存在"),
    GAME_OPT_NO_AUTH("000002","没有权限操作此桌游"),
    MYGAME_NOT_FOUND("000003","此桌游库存不存在"),
    BORROW_APPLY_NOT_FOUND("000004","此租借单不存在"),
    BORROW_APPLY_OPT_NO_AUTH("000005","无权限操作此租借单"),
    WIKI_HAS_FOLLOWED("000006","此桌游已关注"),
    WIKI_HAS_NOT_FOLLOWED("000007","未关注此关注"),
    ;
    String errorCode;
    String errorMsg;

    WikiExceptionEnum(String errorCode, String errorMsg) {
        this.errorCode = "WIKI-" + errorCode;
        this.errorMsg = errorMsg;
    }

    public String errorCode() {
        return errorCode;
    }

    public String errorMsg() {
        return errorMsg;
    }

    public static Tuple<String> errorSupplier(WikiExceptionEnum exceptionEnum) {
        Tuple<String> errorInfo = new Tuple<>();
        errorInfo.setFirst(exceptionEnum.errorCode);
        errorInfo.setSecond(exceptionEnum.errorMsg);
        return errorInfo;
    }
}
