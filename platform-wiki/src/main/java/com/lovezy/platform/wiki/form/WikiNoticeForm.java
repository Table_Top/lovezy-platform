package com.lovezy.platform.wiki.form;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 百科推送内容
 * Created by jin on 2018/1/23.
 */
@Getter
@Setter
@ToString
public class WikiNoticeForm {

    private Integer gameId;

    private String content;

}
