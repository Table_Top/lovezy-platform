package com.lovezy.platform.wiki.query;

import com.lovezy.platform.core.base.query.BaseQuery;
import com.lovezy.platform.wiki.enums.WikiSortTypeEnum;
import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * 百科搜索query
 * Created by jin on 2017/11/5.
 */
@Alias("wikiQuery")
public class WikiQuery extends BaseQuery{

    private Integer gameId;

    private Integer timeMax;

    private Integer timeMin;

    private Integer playerMax;

    private Integer playerMin;

    private List<Integer> categories;

    private List<Integer> mechanisms;

    private boolean isQueryHot = false;

    private String sortName;

    /**
     * 搜索名称 包含中文名和英文名
     */
    private String searchName;

    /**
     * 特质ID
     */
    private Integer traitId;

    private Byte sortType;

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getTimeMax() {
        return timeMax;
    }

    public void setTimeMax(Integer timeMax) {
        this.timeMax = timeMax;
    }

    public Integer getTimeMin() {
        return timeMin;
    }

    public void setTimeMin(Integer timeMin) {
        this.timeMin = timeMin;
    }

    public Integer getPlayerMax() {
        return playerMax;
    }

    public void setPlayerMax(Integer playerMax) {
        this.playerMax = playerMax;
    }

    public Integer getPlayerMin() {
        return playerMin;
    }

    public void setPlayerMin(Integer playerMin) {
        this.playerMin = playerMin;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public void setCategories(List<Integer> categories) {
        this.categories = categories;
    }

    public List<Integer> getMechanisms() {
        return mechanisms;
    }

    public void setMechanisms(List<Integer> mechanisms) {
        this.mechanisms = mechanisms;
    }

    public boolean isQueryHot() {
        return isQueryHot;
    }

    public void setQueryHot(boolean queryHot) {
        isQueryHot = queryHot;
    }

    public String getSortName() {
        return sortName;
    }

    public void setWikiSort(WikiSortTypeEnum wikiSort) {
        if (wikiSort != null) {
            this.sortName = wikiSort.getSortName();
        }
    }

    public Byte getSortType() {
        return sortType;
    }

    public void setSortType(Byte sortType) {
        this.sortType = sortType;
        if (sortType != null) {
            WikiSortTypeEnum wikiSortTypeEnum = WikiSortTypeEnum.valueOf(sortType);
            if (wikiSortTypeEnum != null) {
                this.sortName = wikiSortTypeEnum.getSortName();
            }
        }
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public Integer getTraitId() {
        return traitId;
    }

    public void setTraitId(Integer traitId) {
        this.traitId = traitId;
    }
}
