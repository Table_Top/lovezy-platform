package com.lovezy.platform.wiki.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.ibatis.type.Alias;

/**
 * 百科特质
 * Created by jin on 2017/11/5.
 */
@Alias("wikiTraitVo")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WikiTraitVo {

    private Integer id;

    private Byte type;

    private String name;

    private Integer cnt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }
}
