package com.lovezy.platform.wiki.service;

import com.lovezy.platform.wiki.model.GameExtend;
import com.lovezy.platform.wiki.model.GameTrait;

import java.util.List;

/**
 * 桌游附加信息
 * Created by jin on 2017/11/7.
 */
public interface GameExtendService {
    void saveGameExtend(Integer gameId, List<GameTrait> traits);

    List<GameExtend> findGameExtends(Integer gameId);
}
