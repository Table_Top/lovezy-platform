package com.lovezy.platform.wiki.controller;

import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.wiki.form.GameOperationForm;
import com.lovezy.platform.wiki.form.WikiNoticeForm;
import com.lovezy.platform.wiki.service.GameOperationService;
import com.lovezy.platform.wiki.vo.GameOperationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 百科运营控制器
 * Created by jin on 2018/1/4.
 */
@RestController
@RequestMapping("/api/back/operation")
public class BackGameOperationController {

    @Autowired
    GameOperationService gameOperationService;

    @PostMapping("/setting")
    public ResponseEntity<Response> settingOperationGames(@RequestBody GameOperationForm form) {
        gameOperationService.settingOperationGame(form);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/reset")
    public ResponseEntity<Response> resetOperation(Map<String,Byte> param) {
        gameOperationService.resetSetting(param.get("type"));
        return ResponseEntity.ok(ok());
    }


    @GetMapping("/detail")
    public ResponseEntity<Response> settingDetail(Integer gameId) {
        GameOperationVo gameOperationVo = gameOperationService.settingDetail(gameId);
        return ResponseEntity.ok(ok(gameOperationVo));
    }

    @PostMapping("/pushWiki")
    public ResponseEntity<Response> pushWiki(@RequestBody WikiNoticeForm form) {
        gameOperationService.pushWikiNotice(form);
        return ResponseEntity.ok(ok());
    }


}
