package com.lovezy.platform.wiki.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * 百科营运
 * Created by jin on 2018/1/4.
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GameOperationVo {
    private Integer gameId;

    private Integer newGame;

    private Integer hotGame;

    private Integer searchGame;
}
