package com.lovezy.platform.wiki.service;

import com.lovezy.platform.wiki.form.FollowForm;
import com.lovezy.platform.wiki.model.GameFollow;

/**
 * 百科关注服务
 * Created by jin on 2017/12/3.
 */
public interface GameFollowService {
    GameFollow follow(FollowForm form);

    void unFollow(FollowForm form);

    boolean isFollowed(FollowForm followForm);
}
