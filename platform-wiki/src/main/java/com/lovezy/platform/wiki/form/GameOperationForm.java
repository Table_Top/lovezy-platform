package com.lovezy.platform.wiki.form;

import lombok.Getter;
import lombok.Setter;

/**
 * 百科运营表单
 * Created by jin on 2018/1/4.
 */
@Getter
@Setter
public class GameOperationForm {

    private Integer gameId;

    private Integer newGame;

    private Integer hotGame;

    private Integer searchGame;
}
