package com.lovezy.platform.wiki.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.wiki.model.GameTrait;
import com.lovezy.platform.wiki.model.GameTraitExample;
import com.lovezy.platform.wiki.query.TraitQuery;
import com.lovezy.platform.wiki.vo.WikiTraitVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GameTraitMapper extends BaseExampleMapper<GameTrait, GameTraitExample, Integer> {

    void batchInsert(@Param("list") List<GameTrait> traits);

    List<WikiTraitVo> traitList(TraitQuery query);
}