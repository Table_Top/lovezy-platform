package com.lovezy.platform.wiki.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.wiki.model.GameFollow;
import com.lovezy.platform.wiki.model.GameFollowExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GameFollowMapper extends BaseExampleMapper<GameFollow, GameFollowExample, Integer> {

}