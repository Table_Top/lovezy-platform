package com.lovezy.platform.wiki.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 游戏规则Vo
 * Created by jin on 2018/1/13.
 */
@Getter
@Setter
@ToString
public class GameRuleVo {

    private Integer ruleId;

    private String pdfUrl;

    private String content;

    private Date gmtCreate;

    private Date gmtModify;

}
