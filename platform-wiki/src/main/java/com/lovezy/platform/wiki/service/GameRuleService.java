package com.lovezy.platform.wiki.service;

import com.lovezy.platform.wiki.form.GameRuleForm;
import com.lovezy.platform.wiki.model.GameRule;
import com.lovezy.platform.wiki.vo.GameRuleVo;

/**
 * 桌游规则
 * Created by jin on 2018/1/13.
 */
public interface GameRuleService {
    GameRule saveRule(GameRuleForm addForm);

    void removeRule(GameRule gameRule);

    void editRule(GameRuleForm ruleForm);

    GameRuleVo ruleDetail(Integer gameId);
}
