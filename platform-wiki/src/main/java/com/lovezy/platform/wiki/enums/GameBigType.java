package com.lovezy.platform.wiki.enums;

import com.lovezy.platform.core.exception.ServiceException;

/**
 * 1-策略
 * 2-角色扮演
 * 3-聚会
 * 4-战棋
 * 5-谋杀之谜
 * 6-跑团
 * Created by jin on 2018/1/21.
 */
public enum GameBigType {
    STRATEGY((byte)1,"策略"),
    RPG((byte)2,"角色扮演"),
    PARTY((byte)3,"聚会"),
    WAR_CHESS((byte)4,"战棋"),
    MURDER_MYSTERY((byte)5,"谋杀之谜"),
    TRPG((byte)6,"跑团"),
    ;

    byte value;
    private String description;

    GameBigType(byte value,String description) {
        this.value = value;
        this.description = description;
    }

    public byte getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public static GameBigType valueOf(byte value) {
        for (GameBigType gameBigType : GameBigType.values()) {
            if (value == gameBigType.value) {
                return gameBigType;
            }
        }
        throw new ServiceException("游戏大类出错");
    }
}
