package com.lovezy.platform.wiki.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.wiki.model.GameOperation;
import com.lovezy.platform.wiki.model.GameOperationExample;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GameOperationMapper extends BaseExampleMapper<GameOperation, GameOperationExample, Integer> {
    List<GameOperation> newOrHotList();
}