package com.lovezy.platform.wiki.enums;

import org.springframework.core.convert.converter.Converter;

/**
 * 百科排序枚举
 * Created by jin on 2017/11/5.
 */
public enum WikiSortTypeEnum {
    bestPlayerMax((byte)1,"bestPlayerMax","最佳游戏人数上限"),
    publishYear((byte)2,"publishYear","发布年份"),
    point((byte)3,"point","评分"),
    isChinese((byte)4,"isChinese","是否完全汉化"),
    timeMax((byte)5,"timeMax","最长时间"),
    gmtCreate((byte)-1,"gmtCreate","创建时间"),
    ;
    byte type;
    String sortName;
    String description;

    WikiSortTypeEnum(byte type, String sortName, String description) {
        this.type = type;
        this.sortName = sortName;
        this.description = description;
    }

    public byte getType() {
        return type;
    }

    public String getSortName() {
        return sortName;
    }

    public static WikiSortTypeEnum valueOf(Byte value) {
        for (WikiSortTypeEnum wikiSortTypeEnum : WikiSortTypeEnum.values()) {
            if (wikiSortTypeEnum.type == value) {
                return wikiSortTypeEnum;
            }
        }
        return null;
    }
}

class ByteToWikiSortTypeConverter implements Converter<Byte, WikiSortTypeEnum> {
    @Override
    public WikiSortTypeEnum convert(Byte aByte) {
        for (WikiSortTypeEnum wikiSortTypeEnum : WikiSortTypeEnum.values()) {
            if (aByte == wikiSortTypeEnum.type) {
                return wikiSortTypeEnum;
            }
        }
        return null;
    }
}
