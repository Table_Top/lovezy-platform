package com.lovezy.platform.wiki.form;

import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/12/3.
 */
@Getter
@Setter
@ToString
public class FollowForm {

    @Required(errorMsg = "用户ID不能为空")
    private String memberId;

    /**
     * 百科ID
     */
    @Required(errorMsg = "桌游ID不能为空")
    private Integer id;

}
