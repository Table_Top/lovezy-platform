package com.lovezy.platform.wiki.model;

import com.lovezy.platform.core.valid.annotation.Length;
import com.lovezy.platform.core.valid.annotation.Min;
import com.lovezy.platform.core.valid.annotation.Range;
import com.lovezy.platform.core.valid.annotation.Required;

import java.util.Date;

public class Game {
    private Integer id;

    @Required(errorMsg = "游戏名称不能为空")
    @Length(maxLength = 128,errorMsg = "名称过长")
    private String name;

    @Length(maxLength = 256,errorMsg = "中文名名称过长")
    private String chineseName;

    @Required(errorMsg = "来源ID不能为空")
    private String sourceId;

    private Byte bigType;

    private Integer parentId;

    @Min(min = 0,errorMsg = "最佳人数不能小于0")
    private Integer bestPlayerMax;

    @Min(min = 0,errorMsg = "最佳人数不能小于0")
    private Integer bestPlayerMin;

    @Min(min = 0,errorMsg = "人数不能小于0")
    private Integer playerMax;

    @Min(min = 0,errorMsg = "人数不能小于0")
    private Integer playerMin;

    @Min(min = 0,errorMsg = "时间不能小于0")
    private Integer timeMax;

    @Min(min = 0,errorMsg = "时间不能小于0")
    private Integer timeMin;

    @Min(min = 0,errorMsg = "年龄不能小于0")
    private Integer age;

    private Float weight;

    @Length(maxLength = 1024,errorMsg = "设计师名称过长")
    private String designer;

    @Length(maxLength = 1024,errorMsg = "艺术家名称过长")
    private String artist;

    private Float point;

    private Integer publishYear;

    private String description;

    private String logo;

    private Date gmtCreate;

    private Date gmtModify;

    @Range(errorMsg = "状态格式错误")
    private Byte status;

    private Integer hit;

    @Range(errorMsg = "是否完全汉化格式错误")
    private Byte isChinese;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getChineseName() {
        return chineseName;
    }

    public void setChineseName(String chineseName) {
        this.chineseName = chineseName == null ? null : chineseName.trim();
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId == null ? null : sourceId.trim();
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getBestPlayerMax() {
        return bestPlayerMax;
    }

    public void setBestPlayerMax(Integer bestPlayerMax) {
        this.bestPlayerMax = bestPlayerMax;
    }

    public Integer getBestPlayerMin() {
        return bestPlayerMin;
    }

    public void setBestPlayerMin(Integer bestPlayerMin) {
        this.bestPlayerMin = bestPlayerMin;
    }

    public Integer getPlayerMax() {
        return playerMax;
    }

    public void setPlayerMax(Integer playerMax) {
        this.playerMax = playerMax;
    }

    public Integer getPlayerMin() {
        return playerMin;
    }

    public void setPlayerMin(Integer playerMin) {
        this.playerMin = playerMin;
    }

    public Integer getTimeMax() {
        return timeMax;
    }

    public void setTimeMax(Integer timeMax) {
        this.timeMax = timeMax;
    }

    public Integer getTimeMin() {
        return timeMin;
    }

    public void setTimeMin(Integer timeMin) {
        this.timeMin = timeMin;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getDesigner() {
        return designer;
    }

    public void setDesigner(String designer) {
        this.designer = designer == null ? null : designer.trim();
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist == null ? null : artist.trim();
    }

    public Float getPoint() {
        return point;
    }

    public void setPoint(Float point) {
        this.point = point;
    }

    public Integer getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Integer publishYear) {
        this.publishYear = publishYear;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo == null ? null : logo.trim();
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }

    public Byte getIsChinese() {
        return isChinese;
    }

    public void setIsChinese(Byte isChinese) {
        this.isChinese = isChinese;
    }

    public Byte getBigType() {
        return bigType;
    }

    public void setBigType(Byte bigType) {
        this.bigType = bigType;
    }
}