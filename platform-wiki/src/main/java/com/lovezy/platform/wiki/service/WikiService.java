package com.lovezy.platform.wiki.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lovezy.platform.wiki.form.GameInfoForm;
import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.query.WikiQuery;
import com.lovezy.platform.wiki.vo.WikiVo;

/**
 * 百科服务
 * Created by jin on 2017/11/7.
 */
public interface WikiService {
    Game saveWiki(GameInfoForm form);

    WikiVo findWikiById(Integer gameId);

    WikiVo findWikiById(Integer gameId, String mid);

    void updateWiki(GameInfoForm form);

    PageInfo<WikiVo> findWikiList(WikiQuery query);

    PageInfo<WikiVo> findNewList(WikiQuery query);

    void deleteWiki(WikiQuery query);
}
