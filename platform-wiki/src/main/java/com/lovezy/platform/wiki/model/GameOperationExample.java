package com.lovezy.platform.wiki.model;

import java.util.ArrayList;
import java.util.List;

public class GameOperationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GameOperationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGameIdIsNull() {
            addCriterion("gameId is null");
            return (Criteria) this;
        }

        public Criteria andGameIdIsNotNull() {
            addCriterion("gameId is not null");
            return (Criteria) this;
        }

        public Criteria andGameIdEqualTo(Integer value) {
            addCriterion("gameId =", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdNotEqualTo(Integer value) {
            addCriterion("gameId <>", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdGreaterThan(Integer value) {
            addCriterion("gameId >", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("gameId >=", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdLessThan(Integer value) {
            addCriterion("gameId <", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdLessThanOrEqualTo(Integer value) {
            addCriterion("gameId <=", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdIn(List<Integer> values) {
            addCriterion("gameId in", values, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdNotIn(List<Integer> values) {
            addCriterion("gameId not in", values, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdBetween(Integer value1, Integer value2) {
            addCriterion("gameId between", value1, value2, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdNotBetween(Integer value1, Integer value2) {
            addCriterion("gameId not between", value1, value2, "gameId");
            return (Criteria) this;
        }

        public Criteria andNewGameIsNull() {
            addCriterion("newGame is null");
            return (Criteria) this;
        }

        public Criteria andNewGameIsNotNull() {
            addCriterion("newGame is not null");
            return (Criteria) this;
        }

        public Criteria andNewGameEqualTo(Integer value) {
            addCriterion("newGame =", value, "newGame");
            return (Criteria) this;
        }

        public Criteria andNewGameNotEqualTo(Integer value) {
            addCriterion("newGame <>", value, "newGame");
            return (Criteria) this;
        }

        public Criteria andNewGameGreaterThan(Integer value) {
            addCriterion("newGame >", value, "newGame");
            return (Criteria) this;
        }

        public Criteria andNewGameGreaterThanOrEqualTo(Integer value) {
            addCriterion("newGame >=", value, "newGame");
            return (Criteria) this;
        }

        public Criteria andNewGameLessThan(Integer value) {
            addCriterion("newGame <", value, "newGame");
            return (Criteria) this;
        }

        public Criteria andNewGameLessThanOrEqualTo(Integer value) {
            addCriterion("newGame <=", value, "newGame");
            return (Criteria) this;
        }

        public Criteria andNewGameIn(List<Integer> values) {
            addCriterion("newGame in", values, "newGame");
            return (Criteria) this;
        }

        public Criteria andNewGameNotIn(List<Integer> values) {
            addCriterion("newGame not in", values, "newGame");
            return (Criteria) this;
        }

        public Criteria andNewGameBetween(Integer value1, Integer value2) {
            addCriterion("newGame between", value1, value2, "newGame");
            return (Criteria) this;
        }

        public Criteria andNewGameNotBetween(Integer value1, Integer value2) {
            addCriterion("newGame not between", value1, value2, "newGame");
            return (Criteria) this;
        }

        public Criteria andHotGameIsNull() {
            addCriterion("hotGame is null");
            return (Criteria) this;
        }

        public Criteria andHotGameIsNotNull() {
            addCriterion("hotGame is not null");
            return (Criteria) this;
        }

        public Criteria andHotGameEqualTo(Integer value) {
            addCriterion("hotGame =", value, "hotGame");
            return (Criteria) this;
        }

        public Criteria andHotGameNotEqualTo(Integer value) {
            addCriterion("hotGame <>", value, "hotGame");
            return (Criteria) this;
        }

        public Criteria andHotGameGreaterThan(Integer value) {
            addCriterion("hotGame >", value, "hotGame");
            return (Criteria) this;
        }

        public Criteria andHotGameGreaterThanOrEqualTo(Integer value) {
            addCriterion("hotGame >=", value, "hotGame");
            return (Criteria) this;
        }

        public Criteria andHotGameLessThan(Integer value) {
            addCriterion("hotGame <", value, "hotGame");
            return (Criteria) this;
        }

        public Criteria andHotGameLessThanOrEqualTo(Integer value) {
            addCriterion("hotGame <=", value, "hotGame");
            return (Criteria) this;
        }

        public Criteria andHotGameIn(List<Integer> values) {
            addCriterion("hotGame in", values, "hotGame");
            return (Criteria) this;
        }

        public Criteria andHotGameNotIn(List<Integer> values) {
            addCriterion("hotGame not in", values, "hotGame");
            return (Criteria) this;
        }

        public Criteria andHotGameBetween(Integer value1, Integer value2) {
            addCriterion("hotGame between", value1, value2, "hotGame");
            return (Criteria) this;
        }

        public Criteria andHotGameNotBetween(Integer value1, Integer value2) {
            addCriterion("hotGame not between", value1, value2, "hotGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameIsNull() {
            addCriterion("searchGame is null");
            return (Criteria) this;
        }

        public Criteria andSearchGameIsNotNull() {
            addCriterion("searchGame is not null");
            return (Criteria) this;
        }

        public Criteria andSearchGameEqualTo(Integer value) {
            addCriterion("searchGame =", value, "searchGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameNotEqualTo(Integer value) {
            addCriterion("searchGame <>", value, "searchGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameGreaterThan(Integer value) {
            addCriterion("searchGame >", value, "searchGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameGreaterThanOrEqualTo(Integer value) {
            addCriterion("searchGame >=", value, "searchGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameLessThan(Integer value) {
            addCriterion("searchGame <", value, "searchGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameLessThanOrEqualTo(Integer value) {
            addCriterion("searchGame <=", value, "searchGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameIn(List<Integer> values) {
            addCriterion("searchGame in", values, "searchGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameNotIn(List<Integer> values) {
            addCriterion("searchGame not in", values, "searchGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameBetween(Integer value1, Integer value2) {
            addCriterion("searchGame between", value1, value2, "searchGame");
            return (Criteria) this;
        }

        public Criteria andSearchGameNotBetween(Integer value1, Integer value2) {
            addCriterion("searchGame not between", value1, value2, "searchGame");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}