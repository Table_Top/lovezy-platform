package com.lovezy.platform.wiki.service;

import com.lovezy.platform.wiki.form.GameOperationForm;
import com.lovezy.platform.wiki.form.WikiNoticeForm;
import com.lovezy.platform.wiki.vo.GameOperationVo;
import com.lovezy.platform.wiki.vo.WikiVo;

import java.util.List;

/**
 * 百科运营接口
 * Created by jin on 2018/1/4.
 */
public interface GameOperationService {
    void settingOperationGame(GameOperationForm form);

    GameOperationVo settingDetail(Integer gameId);

    List<String> findSearchName();

    void resetSetting(byte type);

    List<WikiVo> newOrHotList();

    void pushWikiNotice(WikiNoticeForm form);
}
