package com.lovezy.platform.wiki.controller;

import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.wiki.service.GameOperationService;
import com.lovezy.platform.wiki.vo.WikiVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2018/1/4.
 */
@RestController
@RequestMapping("/api/operation")
public class GameOperationController {

    @Autowired
    GameOperationService gameOperationService;

    @GetMapping("/suggestSearch")
    public ResponseEntity<Response> suggestSearch() {
        List<String> searchName = gameOperationService.findSearchName();
        return ResponseEntity.ok(ok(ImmutableMap.of("names", searchName)));
    }

    @GetMapping("/list")
    public ResponseEntity<Response> list() {
        List<WikiVo> wikiVos = gameOperationService.newOrHotList();
        return ResponseEntity.ok(ok(wikiVos));
    }

}
