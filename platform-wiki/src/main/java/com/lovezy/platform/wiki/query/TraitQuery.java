package com.lovezy.platform.wiki.query;

import com.lovezy.platform.core.base.query.BaseQuery;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

/**
 * Created by jin on 2017/12/3.
 */
@Alias("traitQuery")
@Getter
@Setter
public class TraitQuery extends BaseQuery{
    private Byte type;
}
