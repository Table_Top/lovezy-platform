package com.lovezy.platform.wiki.service.impl;

import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.enums.TrueOrFalseEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.wiki.mapper.GameNameMapper;
import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.model.GameName;
import com.lovezy.platform.wiki.model.GameNameExample;
import com.lovezy.platform.wiki.service.GameNameService;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 游戏服务实现
 * Created by jin on 2017/11/7.
 */
@Service
public class GameNameServiceImpl extends MapperService<GameName,Integer,GameNameMapper> implements GameNameService{

    @Override
    public void saveGameName(Game game, List<String> names) {
        Integer gameId = game.getId();
        Date now = new Date();

        GameName oldName = new GameName();
        oldName.setGameId(gameId);
        oldName.setName(game.getName());
        oldName.setIsAlias(TrueOrFalseEnum.FALSE.value());
        oldName.setStatus(StatusEnum.NORMAL.value());
        oldName.setGmtCreate(now);
        oldName.setGmtModify(now);
        mapper.insertSelective(oldName);

        names.stream().map(name -> {
            GameName gameName = new GameName();
            gameName.setGameId(gameId);
            gameName.setName(name);
            gameName.setIsAlias(TrueOrFalseEnum.TRUE.value());
            gameName.setStatus(StatusEnum.NORMAL.value());
            gameName.setGmtCreate(now);
            gameName.setGmtModify(now);
            return gameName;
        }).forEach(mapper::insertSelective);
    }

    @Override
    public List<GameName> findGameNames(Integer gameId, @Nullable Byte isAlias) {
        GameNameExample example = new GameNameExample();
        GameNameExample.Criteria criteria = example.createCriteria()
                .andGameIdEqualTo(gameId)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        if (isAlias != null) {
            criteria.andIsAliasEqualTo(isAlias);
        }
        return Pager.doPageAll(() -> mapper.selectByExample(example));
    }

}
