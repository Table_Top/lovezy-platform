package com.lovezy.platform.wiki.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.model.GameExample;
import com.lovezy.platform.wiki.query.WikiQuery;
import com.lovezy.platform.wiki.vo.WikiVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GameMapper extends BaseExampleMapper<Game, GameExample, Integer> {

    List<WikiVo> selectList(WikiQuery query);

    void incrementHit(Game game);

    WikiVo findWikiById(WikiQuery query);

}