package com.lovezy.platform.wiki.form;

import com.lovezy.platform.core.base.Tuple;
import com.lovezy.platform.wiki.model.Game;

import static com.lovezy.platform.core.utils.ObjectUtil.copy;

/**
 * 百科表单管理类
 * Created by jin on 2017/11/7.
 */
public class GameInfoFormManager {

    public static Game toGame(GameInfoForm form) {
        Game game = new Game();
        copy(form, game);

        Tuple<Integer> time = form.getTime();
        Tuple<Integer> bestPlayers = form.getBestPlayers();
        Tuple<Integer> players = form.getPlayers();
        if (time != null) {
            game.setTimeMax(time.getFirst());
            game.setTimeMin(time.getSecond());
        }

        if (bestPlayers != null) {
            game.setBestPlayerMax(bestPlayers.getFirst());
            game.setBestPlayerMin(bestPlayers.getSecond());
        }

        if (players != null) {
            game.setPlayerMax(players.getFirst());
            game.setPlayerMin(players.getSecond());
        }


        return game;
    }
}
