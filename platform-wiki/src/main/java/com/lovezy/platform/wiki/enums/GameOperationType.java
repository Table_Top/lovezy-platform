package com.lovezy.platform.wiki.enums;

/**
 * 百科运营类别
 * Created by jin on 2018/1/4.
 */
public enum GameOperationType {
    HOT((byte)1),
    NEW((byte)2),
    SEARCH((byte)3);

    byte type;

    GameOperationType(byte type) {
        this.type = type;
    }

    public byte getType() {
        return type;
    }

    public static GameOperationType valueOf(byte type) {
        for (GameOperationType gameOperationType : GameOperationType.values()) {
            if (gameOperationType.type == type) {
                return gameOperationType;
            }
        }
        return null;
    }
}
