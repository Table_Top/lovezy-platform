package com.lovezy.platform.wiki.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.wiki.model.GameExtend;
import com.lovezy.platform.wiki.model.GameExtendExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GameExtendMapper extends BaseExampleMapper<GameExtend, GameExtendExample, Integer> {

}