package com.lovezy.platform.wiki.model;

public class GameOperation {
    private Integer gameId;

    private Integer newGame;

    private Integer hotGame;

    private Integer searchGame;

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getNewGame() {
        return newGame;
    }

    public void setNewGame(Integer newGame) {
        this.newGame = newGame;
    }

    public Integer getHotGame() {
        return hotGame;
    }

    public void setHotGame(Integer hotGame) {
        this.hotGame = hotGame;
    }

    public Integer getSearchGame() {
        return searchGame;
    }

    public void setSearchGame(Integer searchGame) {
        this.searchGame = searchGame;
    }
}