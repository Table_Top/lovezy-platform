package com.lovezy.platform.wiki.service;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.wiki.form.GameInfoForm;
import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.query.WikiQuery;
import com.lovezy.platform.wiki.vo.WikiVo;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 游戏服务
 * Created by jin on 2017/11/4.
 */
public interface GameService {
    PageInfo<WikiVo> selectList(WikiQuery query);

    Game insertGame(GameInfoForm form);

    void incrementHit(Integer gameId, Integer hitNum);

    @Nullable
    Game selectById(@NotNull("桌游ID为空") Integer gameId);

    void updateGame(@NotNull("桌游ID为空") Game game);

    WikiVo findWikiById(@NotNull("桌游ID为空") Integer gameId);
}
