package com.lovezy.platform.wiki.enums;

/**
 * 特质枚举 1-类别 2-机制
 * Created by jin on 2017/11/7.
 */
public enum TraitTypeEnum {
    CATEGORIE((byte)1),
    MECHANISM((byte)2)
    ;

    TraitTypeEnum(byte value) {
        this.value = value;
    }

    byte value;

    public byte value() {
        return value;
    }
}
