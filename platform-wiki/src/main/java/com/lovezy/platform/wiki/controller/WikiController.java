package com.lovezy.platform.wiki.controller;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.wiki.enums.GameBigType;
import com.lovezy.platform.wiki.form.FollowForm;
import com.lovezy.platform.wiki.form.GameInfoForm;
import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.query.WikiQuery;
import com.lovezy.platform.wiki.service.GameFollowService;
import com.lovezy.platform.wiki.service.WikiService;
import com.lovezy.platform.wiki.vo.WikiVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 百科控制器
 * Created by jin on 2017/11/7.
 */
@RestController
@RequestMapping("/api/wiki")
public class WikiController {

    @Autowired
    private WikiService wikiService;

    @Autowired
    private GameFollowService gameFollowService;

    @GetMapping("/{id}")
    public ResponseEntity<Response> findWiki(@PathVariable Integer id) {
        String mid = SiteContext.getCurrentMidNullAble();
        WikiVo wikiVo = wikiService.findWikiById(id,mid);
        return ResponseEntity.ok(ok(wikiVo));
    }

    @GetMapping("/detail")
    public ResponseEntity<Response> findWikiDetail(Integer id) {
        String mid = SiteContext.getCurrentMidNullAble();
        WikiVo wikiVo = wikiService.findWikiById(id, mid);
        return ResponseEntity.ok(ok(wikiVo));
    }

    @PostMapping("/post")
    public ResponseEntity<Response> saveWiki(GameInfoForm form) {
        Game game = wikiService.saveWiki(form);
        return new ResponseEntity<>(ok(game.getId()), HttpStatus.CREATED);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Response> updateInfo(@PathVariable("id") Integer id, @RequestBody GameInfoForm form) {
        form.setId(id);
        wikiService.updateWiki(form);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/list")
    public ResponseEntity<Response> findList(WikiQuery query) {
        if (query == null) {
            query = new WikiQuery();
        }
        PageInfo<WikiVo> wikiPage = wikiService.findWikiList(query);
        return ResponseEntity.ok(ok(wikiPage));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteWiki(@PathVariable("id") Integer id) {
        WikiQuery query = new WikiQuery();
        query.setGameId(id);
        wikiService.deleteWiki(query);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/hotList")
    public ResponseEntity<Response> findhotGames(WikiQuery query) {
        query.setQueryHot(true);
        PageInfo<WikiVo> wikiList = wikiService.findWikiList(query);
        return ResponseEntity.ok(ok(wikiList));
    }

    @GetMapping("/newList")
    public ResponseEntity<Response> newList(WikiQuery query) {
        PageInfo<WikiVo> wikiList = wikiService.findNewList(query);
        return ResponseEntity.ok(ok(wikiList));
    }

    @PostMapping("/follow")
    public ResponseEntity<Response> follow(@RequestBody FollowForm form) {
        if (form == null) {
            form = new FollowForm();
        }
        form.setMemberId(SiteContext.getCurrentMid());
        gameFollowService.follow(form);
        return ResponseEntity.ok(ok());
    }

    @DeleteMapping("/follow")
    public ResponseEntity<Response> unFollow(@RequestBody FollowForm form) {
        if (form == null) {
            form = new FollowForm();
        }
        form.setMemberId(SiteContext.getCurrentMid());
        gameFollowService.unFollow(form);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/types")
    public ResponseEntity<Response> bigTypes() {
        Map<Byte, String> types = new HashMap<>();
        for (GameBigType gameBigType : GameBigType.values()) {
            types.put(gameBigType.getValue(), gameBigType.getDescription());
        }
        return ResponseEntity.ok((ok(types)));
    }
}
