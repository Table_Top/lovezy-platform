package com.lovezy.platform.wiki.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GameExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GameExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andChineseNameIsNull() {
            addCriterion("chineseName is null");
            return (Criteria) this;
        }

        public Criteria andChineseNameIsNotNull() {
            addCriterion("chineseName is not null");
            return (Criteria) this;
        }

        public Criteria andChineseNameEqualTo(String value) {
            addCriterion("chineseName =", value, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameNotEqualTo(String value) {
            addCriterion("chineseName <>", value, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameGreaterThan(String value) {
            addCriterion("chineseName >", value, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameGreaterThanOrEqualTo(String value) {
            addCriterion("chineseName >=", value, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameLessThan(String value) {
            addCriterion("chineseName <", value, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameLessThanOrEqualTo(String value) {
            addCriterion("chineseName <=", value, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameLike(String value) {
            addCriterion("chineseName like", value, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameNotLike(String value) {
            addCriterion("chineseName not like", value, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameIn(List<String> values) {
            addCriterion("chineseName in", values, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameNotIn(List<String> values) {
            addCriterion("chineseName not in", values, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameBetween(String value1, String value2) {
            addCriterion("chineseName between", value1, value2, "chineseName");
            return (Criteria) this;
        }

        public Criteria andChineseNameNotBetween(String value1, String value2) {
            addCriterion("chineseName not between", value1, value2, "chineseName");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNull() {
            addCriterion("sourceId is null");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNotNull() {
            addCriterion("sourceId is not null");
            return (Criteria) this;
        }

        public Criteria andSourceIdEqualTo(String value) {
            addCriterion("sourceId =", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotEqualTo(String value) {
            addCriterion("sourceId <>", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThan(String value) {
            addCriterion("sourceId >", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThanOrEqualTo(String value) {
            addCriterion("sourceId >=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThan(String value) {
            addCriterion("sourceId <", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThanOrEqualTo(String value) {
            addCriterion("sourceId <=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLike(String value) {
            addCriterion("sourceId like", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotLike(String value) {
            addCriterion("sourceId not like", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdIn(List<String> values) {
            addCriterion("sourceId in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotIn(List<String> values) {
            addCriterion("sourceId not in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdBetween(String value1, String value2) {
            addCriterion("sourceId between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotBetween(String value1, String value2) {
            addCriterion("sourceId not between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("parentId is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("parentId is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(Integer value) {
            addCriterion("parentId =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(Integer value) {
            addCriterion("parentId <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(Integer value) {
            addCriterion("parentId >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("parentId >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(Integer value) {
            addCriterion("parentId <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("parentId <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<Integer> values) {
            addCriterion("parentId in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<Integer> values) {
            addCriterion("parentId not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(Integer value1, Integer value2) {
            addCriterion("parentId between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("parentId not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxIsNull() {
            addCriterion("bestPlayerMax is null");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxIsNotNull() {
            addCriterion("bestPlayerMax is not null");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxEqualTo(Integer value) {
            addCriterion("bestPlayerMax =", value, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxNotEqualTo(Integer value) {
            addCriterion("bestPlayerMax <>", value, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxGreaterThan(Integer value) {
            addCriterion("bestPlayerMax >", value, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxGreaterThanOrEqualTo(Integer value) {
            addCriterion("bestPlayerMax >=", value, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxLessThan(Integer value) {
            addCriterion("bestPlayerMax <", value, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxLessThanOrEqualTo(Integer value) {
            addCriterion("bestPlayerMax <=", value, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxIn(List<Integer> values) {
            addCriterion("bestPlayerMax in", values, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxNotIn(List<Integer> values) {
            addCriterion("bestPlayerMax not in", values, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxBetween(Integer value1, Integer value2) {
            addCriterion("bestPlayerMax between", value1, value2, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMaxNotBetween(Integer value1, Integer value2) {
            addCriterion("bestPlayerMax not between", value1, value2, "bestPlayerMax");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinIsNull() {
            addCriterion("bestPlayerMin is null");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinIsNotNull() {
            addCriterion("bestPlayerMin is not null");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinEqualTo(Integer value) {
            addCriterion("bestPlayerMin =", value, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinNotEqualTo(Integer value) {
            addCriterion("bestPlayerMin <>", value, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinGreaterThan(Integer value) {
            addCriterion("bestPlayerMin >", value, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinGreaterThanOrEqualTo(Integer value) {
            addCriterion("bestPlayerMin >=", value, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinLessThan(Integer value) {
            addCriterion("bestPlayerMin <", value, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinLessThanOrEqualTo(Integer value) {
            addCriterion("bestPlayerMin <=", value, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinIn(List<Integer> values) {
            addCriterion("bestPlayerMin in", values, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinNotIn(List<Integer> values) {
            addCriterion("bestPlayerMin not in", values, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinBetween(Integer value1, Integer value2) {
            addCriterion("bestPlayerMin between", value1, value2, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andBestPlayerMinNotBetween(Integer value1, Integer value2) {
            addCriterion("bestPlayerMin not between", value1, value2, "bestPlayerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxIsNull() {
            addCriterion("playerMax is null");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxIsNotNull() {
            addCriterion("playerMax is not null");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxEqualTo(Integer value) {
            addCriterion("playerMax =", value, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxNotEqualTo(Integer value) {
            addCriterion("playerMax <>", value, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxGreaterThan(Integer value) {
            addCriterion("playerMax >", value, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxGreaterThanOrEqualTo(Integer value) {
            addCriterion("playerMax >=", value, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxLessThan(Integer value) {
            addCriterion("playerMax <", value, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxLessThanOrEqualTo(Integer value) {
            addCriterion("playerMax <=", value, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxIn(List<Integer> values) {
            addCriterion("playerMax in", values, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxNotIn(List<Integer> values) {
            addCriterion("playerMax not in", values, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxBetween(Integer value1, Integer value2) {
            addCriterion("playerMax between", value1, value2, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMaxNotBetween(Integer value1, Integer value2) {
            addCriterion("playerMax not between", value1, value2, "playerMax");
            return (Criteria) this;
        }

        public Criteria andPlayerMinIsNull() {
            addCriterion("playerMin is null");
            return (Criteria) this;
        }

        public Criteria andPlayerMinIsNotNull() {
            addCriterion("playerMin is not null");
            return (Criteria) this;
        }

        public Criteria andPlayerMinEqualTo(Integer value) {
            addCriterion("playerMin =", value, "playerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMinNotEqualTo(Integer value) {
            addCriterion("playerMin <>", value, "playerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMinGreaterThan(Integer value) {
            addCriterion("playerMin >", value, "playerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMinGreaterThanOrEqualTo(Integer value) {
            addCriterion("playerMin >=", value, "playerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMinLessThan(Integer value) {
            addCriterion("playerMin <", value, "playerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMinLessThanOrEqualTo(Integer value) {
            addCriterion("playerMin <=", value, "playerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMinIn(List<Integer> values) {
            addCriterion("playerMin in", values, "playerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMinNotIn(List<Integer> values) {
            addCriterion("playerMin not in", values, "playerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMinBetween(Integer value1, Integer value2) {
            addCriterion("playerMin between", value1, value2, "playerMin");
            return (Criteria) this;
        }

        public Criteria andPlayerMinNotBetween(Integer value1, Integer value2) {
            addCriterion("playerMin not between", value1, value2, "playerMin");
            return (Criteria) this;
        }

        public Criteria andTimeMaxIsNull() {
            addCriterion("timeMax is null");
            return (Criteria) this;
        }

        public Criteria andTimeMaxIsNotNull() {
            addCriterion("timeMax is not null");
            return (Criteria) this;
        }

        public Criteria andTimeMaxEqualTo(Integer value) {
            addCriterion("timeMax =", value, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMaxNotEqualTo(Integer value) {
            addCriterion("timeMax <>", value, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMaxGreaterThan(Integer value) {
            addCriterion("timeMax >", value, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMaxGreaterThanOrEqualTo(Integer value) {
            addCriterion("timeMax >=", value, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMaxLessThan(Integer value) {
            addCriterion("timeMax <", value, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMaxLessThanOrEqualTo(Integer value) {
            addCriterion("timeMax <=", value, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMaxIn(List<Integer> values) {
            addCriterion("timeMax in", values, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMaxNotIn(List<Integer> values) {
            addCriterion("timeMax not in", values, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMaxBetween(Integer value1, Integer value2) {
            addCriterion("timeMax between", value1, value2, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMaxNotBetween(Integer value1, Integer value2) {
            addCriterion("timeMax not between", value1, value2, "timeMax");
            return (Criteria) this;
        }

        public Criteria andTimeMinIsNull() {
            addCriterion("timeMin is null");
            return (Criteria) this;
        }

        public Criteria andTimeMinIsNotNull() {
            addCriterion("timeMin is not null");
            return (Criteria) this;
        }

        public Criteria andTimeMinEqualTo(Integer value) {
            addCriterion("timeMin =", value, "timeMin");
            return (Criteria) this;
        }

        public Criteria andTimeMinNotEqualTo(Integer value) {
            addCriterion("timeMin <>", value, "timeMin");
            return (Criteria) this;
        }

        public Criteria andTimeMinGreaterThan(Integer value) {
            addCriterion("timeMin >", value, "timeMin");
            return (Criteria) this;
        }

        public Criteria andTimeMinGreaterThanOrEqualTo(Integer value) {
            addCriterion("timeMin >=", value, "timeMin");
            return (Criteria) this;
        }

        public Criteria andTimeMinLessThan(Integer value) {
            addCriterion("timeMin <", value, "timeMin");
            return (Criteria) this;
        }

        public Criteria andTimeMinLessThanOrEqualTo(Integer value) {
            addCriterion("timeMin <=", value, "timeMin");
            return (Criteria) this;
        }

        public Criteria andTimeMinIn(List<Integer> values) {
            addCriterion("timeMin in", values, "timeMin");
            return (Criteria) this;
        }

        public Criteria andTimeMinNotIn(List<Integer> values) {
            addCriterion("timeMin not in", values, "timeMin");
            return (Criteria) this;
        }

        public Criteria andTimeMinBetween(Integer value1, Integer value2) {
            addCriterion("timeMin between", value1, value2, "timeMin");
            return (Criteria) this;
        }

        public Criteria andTimeMinNotBetween(Integer value1, Integer value2) {
            addCriterion("timeMin not between", value1, value2, "timeMin");
            return (Criteria) this;
        }

        public Criteria andAgeIsNull() {
            addCriterion("age is null");
            return (Criteria) this;
        }

        public Criteria andAgeIsNotNull() {
            addCriterion("age is not null");
            return (Criteria) this;
        }

        public Criteria andAgeEqualTo(Integer value) {
            addCriterion("age =", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotEqualTo(Integer value) {
            addCriterion("age <>", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThan(Integer value) {
            addCriterion("age >", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThanOrEqualTo(Integer value) {
            addCriterion("age >=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThan(Integer value) {
            addCriterion("age <", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThanOrEqualTo(Integer value) {
            addCriterion("age <=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeIn(List<Integer> values) {
            addCriterion("age in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotIn(List<Integer> values) {
            addCriterion("age not in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeBetween(Integer value1, Integer value2) {
            addCriterion("age between", value1, value2, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotBetween(Integer value1, Integer value2) {
            addCriterion("age not between", value1, value2, "age");
            return (Criteria) this;
        }

        public Criteria andWeightIsNull() {
            addCriterion("weight is null");
            return (Criteria) this;
        }

        public Criteria andWeightIsNotNull() {
            addCriterion("weight is not null");
            return (Criteria) this;
        }

        public Criteria andWeightEqualTo(Float value) {
            addCriterion("weight =", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotEqualTo(Float value) {
            addCriterion("weight <>", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThan(Float value) {
            addCriterion("weight >", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThanOrEqualTo(Float value) {
            addCriterion("weight >=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThan(Float value) {
            addCriterion("weight <", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThanOrEqualTo(Float value) {
            addCriterion("weight <=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightIn(List<Float> values) {
            addCriterion("weight in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotIn(List<Float> values) {
            addCriterion("weight not in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightBetween(Float value1, Float value2) {
            addCriterion("weight between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotBetween(Float value1, Float value2) {
            addCriterion("weight not between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andDesignerIsNull() {
            addCriterion("designer is null");
            return (Criteria) this;
        }

        public Criteria andDesignerIsNotNull() {
            addCriterion("designer is not null");
            return (Criteria) this;
        }

        public Criteria andDesignerEqualTo(String value) {
            addCriterion("designer =", value, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerNotEqualTo(String value) {
            addCriterion("designer <>", value, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerGreaterThan(String value) {
            addCriterion("designer >", value, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerGreaterThanOrEqualTo(String value) {
            addCriterion("designer >=", value, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerLessThan(String value) {
            addCriterion("designer <", value, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerLessThanOrEqualTo(String value) {
            addCriterion("designer <=", value, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerLike(String value) {
            addCriterion("designer like", value, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerNotLike(String value) {
            addCriterion("designer not like", value, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerIn(List<String> values) {
            addCriterion("designer in", values, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerNotIn(List<String> values) {
            addCriterion("designer not in", values, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerBetween(String value1, String value2) {
            addCriterion("designer between", value1, value2, "designer");
            return (Criteria) this;
        }

        public Criteria andDesignerNotBetween(String value1, String value2) {
            addCriterion("designer not between", value1, value2, "designer");
            return (Criteria) this;
        }

        public Criteria andArtistIsNull() {
            addCriterion("artist is null");
            return (Criteria) this;
        }

        public Criteria andArtistIsNotNull() {
            addCriterion("artist is not null");
            return (Criteria) this;
        }

        public Criteria andArtistEqualTo(String value) {
            addCriterion("artist =", value, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistNotEqualTo(String value) {
            addCriterion("artist <>", value, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistGreaterThan(String value) {
            addCriterion("artist >", value, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistGreaterThanOrEqualTo(String value) {
            addCriterion("artist >=", value, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistLessThan(String value) {
            addCriterion("artist <", value, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistLessThanOrEqualTo(String value) {
            addCriterion("artist <=", value, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistLike(String value) {
            addCriterion("artist like", value, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistNotLike(String value) {
            addCriterion("artist not like", value, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistIn(List<String> values) {
            addCriterion("artist in", values, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistNotIn(List<String> values) {
            addCriterion("artist not in", values, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistBetween(String value1, String value2) {
            addCriterion("artist between", value1, value2, "artist");
            return (Criteria) this;
        }

        public Criteria andArtistNotBetween(String value1, String value2) {
            addCriterion("artist not between", value1, value2, "artist");
            return (Criteria) this;
        }

        public Criteria andPointIsNull() {
            addCriterion("point is null");
            return (Criteria) this;
        }

        public Criteria andPointIsNotNull() {
            addCriterion("point is not null");
            return (Criteria) this;
        }

        public Criteria andPointEqualTo(Float value) {
            addCriterion("point =", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotEqualTo(Float value) {
            addCriterion("point <>", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointGreaterThan(Float value) {
            addCriterion("point >", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointGreaterThanOrEqualTo(Float value) {
            addCriterion("point >=", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointLessThan(Float value) {
            addCriterion("point <", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointLessThanOrEqualTo(Float value) {
            addCriterion("point <=", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointIn(List<Float> values) {
            addCriterion("point in", values, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotIn(List<Float> values) {
            addCriterion("point not in", values, "point");
            return (Criteria) this;
        }

        public Criteria andPointBetween(Float value1, Float value2) {
            addCriterion("point between", value1, value2, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotBetween(Float value1, Float value2) {
            addCriterion("point not between", value1, value2, "point");
            return (Criteria) this;
        }

        public Criteria andPublishYearIsNull() {
            addCriterion("publishYear is null");
            return (Criteria) this;
        }

        public Criteria andPublishYearIsNotNull() {
            addCriterion("publishYear is not null");
            return (Criteria) this;
        }

        public Criteria andPublishYearEqualTo(Integer value) {
            addCriterion("publishYear =", value, "publishYear");
            return (Criteria) this;
        }

        public Criteria andPublishYearNotEqualTo(Integer value) {
            addCriterion("publishYear <>", value, "publishYear");
            return (Criteria) this;
        }

        public Criteria andPublishYearGreaterThan(Integer value) {
            addCriterion("publishYear >", value, "publishYear");
            return (Criteria) this;
        }

        public Criteria andPublishYearGreaterThanOrEqualTo(Integer value) {
            addCriterion("publishYear >=", value, "publishYear");
            return (Criteria) this;
        }

        public Criteria andPublishYearLessThan(Integer value) {
            addCriterion("publishYear <", value, "publishYear");
            return (Criteria) this;
        }

        public Criteria andPublishYearLessThanOrEqualTo(Integer value) {
            addCriterion("publishYear <=", value, "publishYear");
            return (Criteria) this;
        }

        public Criteria andPublishYearIn(List<Integer> values) {
            addCriterion("publishYear in", values, "publishYear");
            return (Criteria) this;
        }

        public Criteria andPublishYearNotIn(List<Integer> values) {
            addCriterion("publishYear not in", values, "publishYear");
            return (Criteria) this;
        }

        public Criteria andPublishYearBetween(Integer value1, Integer value2) {
            addCriterion("publishYear between", value1, value2, "publishYear");
            return (Criteria) this;
        }

        public Criteria andPublishYearNotBetween(Integer value1, Integer value2) {
            addCriterion("publishYear not between", value1, value2, "publishYear");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andLogoIsNull() {
            addCriterion("logo is null");
            return (Criteria) this;
        }

        public Criteria andLogoIsNotNull() {
            addCriterion("logo is not null");
            return (Criteria) this;
        }

        public Criteria andLogoEqualTo(String value) {
            addCriterion("logo =", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotEqualTo(String value) {
            addCriterion("logo <>", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoGreaterThan(String value) {
            addCriterion("logo >", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoGreaterThanOrEqualTo(String value) {
            addCriterion("logo >=", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoLessThan(String value) {
            addCriterion("logo <", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoLessThanOrEqualTo(String value) {
            addCriterion("logo <=", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoLike(String value) {
            addCriterion("logo like", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotLike(String value) {
            addCriterion("logo not like", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoIn(List<String> values) {
            addCriterion("logo in", values, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotIn(List<String> values) {
            addCriterion("logo not in", values, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoBetween(String value1, String value2) {
            addCriterion("logo between", value1, value2, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotBetween(String value1, String value2) {
            addCriterion("logo not between", value1, value2, "logo");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmtCreate is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmtCreate is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmtCreate =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmtCreate <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmtCreate >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmtCreate >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmtCreate <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmtCreate <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmtCreate in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmtCreate not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmtCreate between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmtCreate not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifyIsNull() {
            addCriterion("gmtModify is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifyIsNotNull() {
            addCriterion("gmtModify is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifyEqualTo(Date value) {
            addCriterion("gmtModify =", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyNotEqualTo(Date value) {
            addCriterion("gmtModify <>", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyGreaterThan(Date value) {
            addCriterion("gmtModify >", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyGreaterThanOrEqualTo(Date value) {
            addCriterion("gmtModify >=", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyLessThan(Date value) {
            addCriterion("gmtModify <", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyLessThanOrEqualTo(Date value) {
            addCriterion("gmtModify <=", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyIn(List<Date> values) {
            addCriterion("gmtModify in", values, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyNotIn(List<Date> values) {
            addCriterion("gmtModify not in", values, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyBetween(Date value1, Date value2) {
            addCriterion("gmtModify between", value1, value2, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyNotBetween(Date value1, Date value2) {
            addCriterion("gmtModify not between", value1, value2, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andHitIsNull() {
            addCriterion("hit is null");
            return (Criteria) this;
        }

        public Criteria andHitIsNotNull() {
            addCriterion("hit is not null");
            return (Criteria) this;
        }

        public Criteria andHitEqualTo(Integer value) {
            addCriterion("hit =", value, "hit");
            return (Criteria) this;
        }

        public Criteria andHitNotEqualTo(Integer value) {
            addCriterion("hit <>", value, "hit");
            return (Criteria) this;
        }

        public Criteria andHitGreaterThan(Integer value) {
            addCriterion("hit >", value, "hit");
            return (Criteria) this;
        }

        public Criteria andHitGreaterThanOrEqualTo(Integer value) {
            addCriterion("hit >=", value, "hit");
            return (Criteria) this;
        }

        public Criteria andHitLessThan(Integer value) {
            addCriterion("hit <", value, "hit");
            return (Criteria) this;
        }

        public Criteria andHitLessThanOrEqualTo(Integer value) {
            addCriterion("hit <=", value, "hit");
            return (Criteria) this;
        }

        public Criteria andHitIn(List<Integer> values) {
            addCriterion("hit in", values, "hit");
            return (Criteria) this;
        }

        public Criteria andHitNotIn(List<Integer> values) {
            addCriterion("hit not in", values, "hit");
            return (Criteria) this;
        }

        public Criteria andHitBetween(Integer value1, Integer value2) {
            addCriterion("hit between", value1, value2, "hit");
            return (Criteria) this;
        }

        public Criteria andHitNotBetween(Integer value1, Integer value2) {
            addCriterion("hit not between", value1, value2, "hit");
            return (Criteria) this;
        }

        public Criteria andIsChineseIsNull() {
            addCriterion("isChinese is null");
            return (Criteria) this;
        }

        public Criteria andIsChineseIsNotNull() {
            addCriterion("isChinese is not null");
            return (Criteria) this;
        }

        public Criteria andIsChineseEqualTo(Byte value) {
            addCriterion("isChinese =", value, "isChinese");
            return (Criteria) this;
        }

        public Criteria andIsChineseNotEqualTo(Byte value) {
            addCriterion("isChinese <>", value, "isChinese");
            return (Criteria) this;
        }

        public Criteria andIsChineseGreaterThan(Byte value) {
            addCriterion("isChinese >", value, "isChinese");
            return (Criteria) this;
        }

        public Criteria andIsChineseGreaterThanOrEqualTo(Byte value) {
            addCriterion("isChinese >=", value, "isChinese");
            return (Criteria) this;
        }

        public Criteria andIsChineseLessThan(Byte value) {
            addCriterion("isChinese <", value, "isChinese");
            return (Criteria) this;
        }

        public Criteria andIsChineseLessThanOrEqualTo(Byte value) {
            addCriterion("isChinese <=", value, "isChinese");
            return (Criteria) this;
        }

        public Criteria andIsChineseIn(List<Byte> values) {
            addCriterion("isChinese in", values, "isChinese");
            return (Criteria) this;
        }

        public Criteria andIsChineseNotIn(List<Byte> values) {
            addCriterion("isChinese not in", values, "isChinese");
            return (Criteria) this;
        }

        public Criteria andIsChineseBetween(Byte value1, Byte value2) {
            addCriterion("isChinese between", value1, value2, "isChinese");
            return (Criteria) this;
        }

        public Criteria andIsChineseNotBetween(Byte value1, Byte value2) {
            addCriterion("isChinese not between", value1, value2, "isChinese");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}