package com.lovezy.platform.wiki.service.impl;

import com.lovezy.platform.core.base.AppServer;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.exception.ServiceException;
import com.lovezy.platform.core.spring.SpringContext;
import com.lovezy.platform.core.utils.ObjectUtil;
import com.lovezy.platform.core.valid.ValidatorUtil;
import com.lovezy.platform.filestore.model.File;
import com.lovezy.platform.filestore.service.FileHelper;
import com.lovezy.platform.filestore.service.FileService;
import com.lovezy.platform.wiki.form.GameRuleForm;
import com.lovezy.platform.wiki.mapper.GameRuleMapper;
import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.model.GameRule;
import com.lovezy.platform.wiki.model.GameRuleExample;
import com.lovezy.platform.wiki.service.GameRuleService;
import com.lovezy.platform.wiki.service.GameService;
import com.lovezy.platform.wiki.vo.GameRuleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.lovezy.platform.core.utils.ObjectUtil.nonNull;

/**
 * 桌游规则
 * Created by jin on 2018/1/13.
 */
@Service
public class GameRuleServiceImpl extends MapperService<GameRule,Integer,GameRuleMapper> implements GameRuleService{

    @Autowired
    FileService fileService;

    @Autowired
    GameService gameService;

    @Autowired
    FileHelper fileHelper;

    @Override
    public GameRule saveRule(GameRuleForm addForm) {
        ValidatorUtil.validate(addForm);
        GameRule rule = ObjectUtil.copyTo(addForm, new GameRule());

        Integer pdfId = findPdfId(addForm.getPdfUrl());
        rule.setPdfId(pdfId);

        rule.setStatus(StatusEnum.NORMAL.value());
        Date now = new Date();
        rule.setGmtCreate(now);
        rule.setGmtModify(now);
        String mid = addForm.getMid();
        rule.setCreateId(mid);
        rule.setModifyId(mid);

        mapper.insert(rule);
        return rule;
    }

    @Override
    public void removeRule(GameRule gameRule) {
        Integer ruleId = gameRule.getRuleId();
        nonNull(ruleId);
        GameRule updateRule = new GameRule();
        updateRule.setRuleId(ruleId);
        updateRule.setGmtModify(new Date());
        updateRule.setModifyId(gameRule.getModifyId());
        updateRule.setStatus(StatusEnum.DELETED.value());
        mapper.updateByPrimaryKeySelective(updateRule);
    }

    @Override
    public void editRule(GameRuleForm ruleForm) {
        Integer ruleId = ruleForm.getRuleId();
        GameRule gameRule = mapper.selectByPrimaryKey(ruleId);
        if (gameRule == null) {
            throw new ServiceException("规则不存在");
        }
        Integer pdfId = findPdfId(ruleForm.getPdfUrl());
        gameRule.setPdfId(pdfId);
        ObjectUtil.copy(ruleForm, gameRule);
        gameRule.setModifyId(ruleForm.getMid());
        gameRule.setGmtModify(new Date());
        mapper.updateByPrimaryKeySelective(gameRule);
    }

    @Override
    public GameRuleVo ruleDetail(Integer gameId) {
        Game game = gameService.selectById(gameId);
        if (game == null) {
//            throw new ServiceException("百科不存在");
            return null;
        }
        GameRuleExample example = new GameRuleExample();
        example.setOrderByClause("gmtModify desc");
        example.createCriteria()
                .andGameIdEqualTo(gameId)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        Optional<GameRule> gameRuleOption = Pager.doSingleton(() -> mapper.selectByExample(example));
        if (!gameRuleOption.isPresent()) {
            return null;
        }
        GameRule gameRule = gameRuleOption.get();
        GameRuleVo gameRuleVo = ObjectUtil.copyTo(gameRule, new GameRuleVo());
        Integer pdfId = gameRule.getPdfId();
        if (pdfId != null) {
            File fileById = fileService.findFileById(pdfId);
            String pdfUrl = fileHelper.toDownloadUrl(fileById.getUrl());
            gameRuleVo.setPdfUrl(pdfUrl);
        }
        return gameRuleVo;
    }

    public static Map<String,String> ruleUrl(Integer gameId) {
        if (gameId == null) {
            return null;
        }
        Map<String, String> urlMap = new HashMap<>();
        GameRuleService gameRuleService = SpringContext.getContext().getBean(GameRuleService.class);
        GameRuleVo gameRuleVo = gameRuleService.ruleDetail(gameId);
        if (gameRuleVo == null) {
            return null;
        }else{
            String pdfUrl = gameRuleVo.getPdfUrl();
            if (StringUtils.hasText(pdfUrl)) {
                urlMap.put("pdf", pdfUrl);
            }
            if (StringUtils.hasText(gameRuleVo.getContent())) {
                AppServer appServer = SpringContext.getContext().getBean(AppServer.class);
                String viewUrl = appServer.getViewDomain() + "view/wiki?id=" + gameId;
                urlMap.put("viewUrl", viewUrl);
            }
            return urlMap;
        }
    }

    private Integer findPdfId(String pdfUrl) {
        if (pdfUrl == null) {
            return null;
        }
        File pdf = fileService.findFileByUrl(pdfUrl);
        return pdf.getFileId();
    }

}
