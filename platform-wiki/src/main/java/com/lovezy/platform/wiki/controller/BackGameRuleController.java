package com.lovezy.platform.wiki.controller;

import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.wiki.form.GameRuleForm;
import com.lovezy.platform.wiki.model.GameRule;
import com.lovezy.platform.wiki.service.GameRuleService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 后台桌游规则控制器
 * Created by jin on 2018/1/13.
 */
@RestController
@RequestMapping("/api/back/game")
public class BackGameRuleController {

    @Autowired
    GameRuleService gameRuleService;


    @PostMapping("/rule")
    public ResponseEntity<Response> saveRule(@RequestBody GameRuleForm form) {
        String mid = SiteContext.getCurrentMid();
        form.setMid(mid);
        gameRuleService.saveRule(form);
        return ResponseEntity.ok(ok());
    }

    @PutMapping("/rule")
    public ResponseEntity<Response> editRule(@RequestBody GameRuleForm form) {
        String mid = SiteContext.getCurrentMid();
        form.setMid(mid);
        gameRuleService.editRule(form);
        return ResponseEntity.ok(ok());
    }


    @Delete("/rule")
    public ResponseEntity<Response> deleteRule(@RequestBody GameRule rule) {
        String mid = SiteContext.getCurrentMid();
        rule.setModifyId(mid);
        gameRuleService.removeRule(rule);
        return ResponseEntity.ok(ok());
    }


}
