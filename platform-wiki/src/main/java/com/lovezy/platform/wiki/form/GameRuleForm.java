package com.lovezy.platform.wiki.form;

import com.lovezy.platform.core.valid.annotation.Length;
import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2018/1/13.
 */
@Getter
@Setter
@ToString
public class GameRuleForm {

    private Integer ruleId;

    @Required(errorMsg = "游戏ID不存在")
    private Integer gameId;

    @Length(maxLength = 10240,errorMsg = "简介过长")
    private String content;

    private String pdfUrl;

    @Required(errorMsg = "用户ID不能为空")
    private String mid;

}
