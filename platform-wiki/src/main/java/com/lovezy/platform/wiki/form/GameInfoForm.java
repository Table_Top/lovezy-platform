package com.lovezy.platform.wiki.form;

import com.lovezy.platform.core.base.Tuple;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 游戏百科表单
 * Created by jin on 2017/11/7.
 */
@Getter
@Setter
public class GameInfoForm {

    private Integer id;

    private Integer sourceId;

    private Tuple<Integer> bestPlayers;

    private Tuple<Integer> players;

    private Tuple<Integer> time;

    private Integer age;

    private Float weight;

    private String designer;

    private String artist;

    private Float point;

    private String name;

    private Byte bigType;

    private String chineseName;

    private Integer publishYear;

    private String description;

    private Byte isChinese;

    private List<String> categories;

    private List<String> mechanisms;

    private List<String> alternateNames;

    private String logo;

    private List<String> dlcs;

}
