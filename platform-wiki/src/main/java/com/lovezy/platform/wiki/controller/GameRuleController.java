package com.lovezy.platform.wiki.controller;

import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.wiki.service.GameRuleService;
import com.lovezy.platform.wiki.vo.GameRuleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 桌游规则控制器
 * Created by jin on 2018/1/13.
 */
@RestController
@RequestMapping("/api/game")
public class GameRuleController {

    @Autowired
    GameRuleService gameRuleService;

    @GetMapping("/rule")
    public ResponseEntity<Response> ruleDetail(@RequestParam Integer gameId) {
        GameRuleVo gameRuleVo = gameRuleService.ruleDetail(gameId);
        return ResponseEntity.ok(ok(gameRuleVo));
    }
}
