package com.lovezy.platform.wiki.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.wiki.model.GameName;
import com.lovezy.platform.wiki.model.GameNameExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GameNameMapper extends BaseExampleMapper<GameName, GameNameExample, Integer> {

}