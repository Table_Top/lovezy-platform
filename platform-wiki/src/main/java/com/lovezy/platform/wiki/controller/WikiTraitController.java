package com.lovezy.platform.wiki.controller;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.wiki.enums.GameBigType;
import com.lovezy.platform.wiki.query.TraitQuery;
import com.lovezy.platform.wiki.service.GameTraitService;
import com.lovezy.platform.wiki.vo.WikiTraitVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2017/12/3.
 */
@RestController
@RequestMapping("/api/wiki/trait")
public class WikiTraitController {

    @Autowired
    private GameTraitService gameTraitService;

    @GetMapping("/list")
    public ResponseEntity<Response> list(TraitQuery query) {
        if (query == null) {
            query = new TraitQuery();
        }
        PageInfo<WikiTraitVo> traitList = gameTraitService.traitList(query);
        return ResponseEntity.ok(ok(traitList));
    }

    @GetMapping("/bigTypes")
    public ResponseEntity<Response> bigTypes() {
        Map<String, Object> bigTypes = new LinkedHashMap<>(16);
        for (GameBigType gameBigType : GameBigType.values()) {
            bigTypes.put("type", gameBigType.getDescription());
            bigTypes.put("name", gameBigType.getValue());
        }
        return ResponseEntity.ok(ok(bigTypes));
    }


}
