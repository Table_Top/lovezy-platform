package com.lovezy.platform.wiki.service.impl;

import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.query.Pager;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.wiki.mapper.GameExtendMapper;
import com.lovezy.platform.wiki.model.GameExtend;
import com.lovezy.platform.wiki.model.GameExtendExample;
import com.lovezy.platform.wiki.model.GameTrait;
import com.lovezy.platform.wiki.service.GameExtendService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 桌游附加信息实现
 * Created by jin on 2017/11/7.
 */
@Service
public class GameExtendServiceImpl extends MapperService<GameExtend,Integer,GameExtendMapper> implements GameExtendService{

    @Override
    public void saveGameExtend(Integer gameId, List<GameTrait> traits) {
        Date now = new Date();
        traits.stream().map(trait -> {
            GameExtend extend = new GameExtend();
            extend.setGameId(gameId);
            extend.setType(trait.getType());
            extend.setStatus(StatusEnum.NORMAL.value());
            extend.setTraitId(trait.getId());
            extend.setGmtCreate(now);
            extend.setGmtModify(now);
            return extend;
        }).forEach(mapper::insert);
    }

    @Override
    public List<GameExtend> findGameExtends(Integer gameId) {
        GameExtendExample example = new GameExtendExample();
        example.createCriteria()
                .andGameIdEqualTo(gameId)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        return Pager.doPageAll(() -> mapper.selectByExample(example));
    }

}
