package com.lovezy.platform.wiki.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * 桌游百科VO
 *
 bestPlayers: [5,6]
 players: [3,6]
 time: 30
 age: 6
 weight: 1.271712(Float)Number策略度
 designer: String
 artist: String
 point: Number
 name: String
 publishYear: String
 description: Text
 categories: [](类别)
 mechanisms: [](机制)
 alternateNames: [](别名)
 logo: file(存地址还是存文件流你来定) 图片
 发行年份: string
 dlcs (扩展包这个你来定)
 * Created by jin on 2017/11/5.
 */
@Alias("wikiVo")
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WikiVo {

    private Integer id;

    private String name;

    private Byte bigType;

    private String chineseName;

    private Integer sourceId;

    private Integer bestPlayerMax;

    private Integer bestPlayerMin;

    private Integer playerMax;

    private Integer playerMin;

    private Integer timeMin;

    private Integer timeMax;

    private Integer age;

    private Float weight;

    private String designer;

    private List<WikiTraitVo> categories;

    private List<WikiTraitVo> mechanisms;

    private List<String> alternateNames;

    private String logo;

    private List<WikiTraitVo> dlcs;

    private Byte hasFollow;

    private Byte operationType;

    private String viewUrl;

    private String pdf;

    private Float point;

    private String description;

    private Byte isChinese;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChineseName() {
        return chineseName;
    }

    public void setChineseName(String chineseName) {
        this.chineseName = chineseName;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getBestPlayerMax() {
        return bestPlayerMax;
    }

    public void setBestPlayerMax(Integer bestPlayerMax) {
        this.bestPlayerMax = bestPlayerMax;
    }

    public Integer getBestPlayerMin() {
        return bestPlayerMin;
    }

    public void setBestPlayerMin(Integer bestPlayerMin) {
        this.bestPlayerMin = bestPlayerMin;
    }

    public Integer getPlayerMax() {
        return playerMax;
    }

    public void setPlayerMax(Integer playerMax) {
        this.playerMax = playerMax;
    }

    public Integer getPlayerMin() {
        return playerMin;
    }

    public void setPlayerMin(Integer playerMin) {
        this.playerMin = playerMin;
    }

    public Integer getTimeMin() {
        return timeMin;
    }

    public void setTimeMin(Integer timeMin) {
        this.timeMin = timeMin;
    }

    public Integer getTimeMax() {
        return timeMax;
    }

    public void setTimeMax(Integer timeMax) {
        this.timeMax = timeMax;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getDesigner() {
        return designer;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public List<WikiTraitVo> getCategories() {
        return categories;
    }

    public void setCategories(List<WikiTraitVo> categories) {
        this.categories = categories;
    }

    public List<WikiTraitVo> getMechanisms() {
        return mechanisms;
    }

    public void setMechanisms(List<WikiTraitVo> mechanisms) {
        this.mechanisms = mechanisms;
    }

    public List<String> getAlternateNames() {
        return alternateNames;
    }

    public void setAlternateNames(List<String> alternateNames) {
        this.alternateNames = alternateNames;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<WikiTraitVo> getDlcs() {
        return dlcs;
    }

    public void setDlcs(List<WikiTraitVo> dlcs) {
        this.dlcs = dlcs;
    }

    public Byte getHasFollow() {
        return hasFollow;
    }

    public void setHasFollow(Byte hasFollow) {
        this.hasFollow = hasFollow;
    }

    public Byte getOperationType() {
        return operationType;
    }

    public void setOperationType(Byte operationType) {
        this.operationType = operationType;
    }

    public Float getPoint() {
        return point;
    }

    public void setPoint(Float point) {
        this.point = point;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Byte getIsChinese() {
        return isChinese;
    }

    public void setIsChinese(Byte isChinese) {
        this.isChinese = isChinese;
    }

    public void setBigType(Byte bigType) {
        this.bigType = bigType;
    }

    public Byte getBigType() {
        return bigType;
    }

    public String getViewUrl() {
        return viewUrl;
    }

    public void setViewUrl(String viewUrl) {
        this.viewUrl = viewUrl;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }
}
