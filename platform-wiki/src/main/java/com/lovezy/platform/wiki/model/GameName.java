package com.lovezy.platform.wiki.model;

import com.lovezy.platform.core.valid.annotation.Length;
import com.lovezy.platform.core.valid.annotation.Range;
import com.lovezy.platform.core.valid.annotation.Required;

import java.util.Date;

public class GameName {
    private Integer nameId;

    @Required(errorMsg = "别名不能为空")
    @Length(maxLength = 256,errorMsg = "别名格式错误")
    private String name;

    @Required(errorMsg = "游戏ID不能为空")
    private Integer gameId;

    @Required(errorMsg = "别名标识不能为空")
    @Range(errorMsg = "别名标识格式错误")
    private Byte isAlias;

    private Date gmtCreate;

    private Date gmtModify;

    @Range(errorMsg = "状态格式错误")
    private Byte status;

    public Integer getNameId() {
        return nameId;
    }

    public void setNameId(Integer nameId) {
        this.nameId = nameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Byte getIsAlias() {
        return isAlias;
    }

    public void setIsAlias(Byte isAlias) {
        this.isAlias = isAlias;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}