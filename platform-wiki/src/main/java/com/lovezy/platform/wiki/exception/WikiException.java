package com.lovezy.platform.wiki.exception;

import com.lovezy.platform.core.exception.ServiceException;

/**
 * 百科异常
 * Created by jin on 2017/11/7.
 */
public class WikiException extends ServiceException{

    public WikiException(String errorCode, String message) {
        super(errorCode, message);
    }

    public WikiException(WikiExceptionEnum exceptionEnum) {
        super(exceptionEnum.errorCode, exceptionEnum.errorMsg);
    }
}
