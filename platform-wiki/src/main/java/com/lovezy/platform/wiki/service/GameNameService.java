package com.lovezy.platform.wiki.service;

import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.model.GameName;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * 游戏名称服务
 * Created by jin on 2017/11/7.
 */
public interface GameNameService {
    void saveGameName(Game game, List<String> names);

    List<GameName> findGameNames(Integer gameId, @Nullable Byte isAlias);
}
