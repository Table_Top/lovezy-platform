package com.lovezy.platform.wiki.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.wiki.model.GameRule;
import com.lovezy.platform.wiki.model.GameRuleExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GameRuleMapper extends BaseExampleMapper<GameRule, GameRuleExample, Integer> {

}