package com.lovezy.platform.borrow.form;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 借还表单
 * Created by jin on 2017/11/15.
 */
@Getter
@Setter
@ToString
public class BorrowForm {

    private Integer borrowId;

    private Integer myGameId;

    private String memberId;

    private Integer shopId;

}
