package com.lovezy.platform.borrow.form;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 我的桌游表单
 * Created by jin on 2017/11/14.
 */
@Getter
@Setter
@ToString
public class MyGameForm {

    private Integer myGameId;

    /**
     * 桌游ID
     */
    private Integer gameId;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 库存
     */
    private Integer inventory;

    /**
     * 热门
     */
    private Byte hot;
}
