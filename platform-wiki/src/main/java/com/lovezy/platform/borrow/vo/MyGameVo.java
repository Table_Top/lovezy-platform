package com.lovezy.platform.borrow.vo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lovezy.platform.core.base.enums.TrueOrFalseEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * 我的游戏
 * Created by jin on 2017/11/14.
 */
@Alias("myGameVo")
@Getter
@Setter
@ToString
public class MyGameVo {

    private String myGameId;

    private String gameId;

    private String memberId;

    private String name;

    private String logo;

    private Double point;

    private BigDecimal price;

    private Integer inventory;

    private String chineseName;

    @JsonIgnore
    private Byte hot;

    @JsonGetter
    public boolean hot() {
        return Objects.equals(hot, TrueOrFalseEnum.TRUE.value());
    }

}
