package com.lovezy.platform.borrow.service;

import com.lovezy.platform.borrow.form.BorrowForm;
import org.springframework.transaction.annotation.Transactional;

/**
 * 租借服务
 * Created by jin on 2017/11/14.
 */
public interface BorrowService {
    void borrowGame(BorrowForm form);

    void cancleBorrow(BorrowForm form);

    @Transactional
    void agreeApply(BorrowForm form);

    void rejectApply(BorrowForm form);

    void returnItem(BorrowForm form);
}
