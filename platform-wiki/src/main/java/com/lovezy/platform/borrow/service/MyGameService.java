package com.lovezy.platform.borrow.service;

import com.github.pagehelper.Page;
import com.lovezy.platform.borrow.enums.MyGameTypeEnum;
import com.lovezy.platform.borrow.form.HotEditForm;
import com.lovezy.platform.borrow.form.SetGameForm;
import com.lovezy.platform.borrow.model.MyGame;
import com.lovezy.platform.borrow.query.SearchMyGameQuery;
import com.lovezy.platform.borrow.vo.MyGameVo;
import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * 我的游戏服务
 * Created by jin on 2017/11/14.
 */
public interface MyGameService {
    @Transactional
    void addMyGame(SetGameForm form);

    @Transactional
    void delMyGame(SetGameForm form);

    Page<MyGameVo> findMyGame(SearchMyGameQuery query);

    void editHotGames(HotEditForm form);

    MyGame selectById(@NotNull Integer id);

    Optional<MyGame> findMyGameWithRole(@NotNull Integer gameId, MyGameTypeEnum myGameType);

    MyGame shopFindIfHasNotGameThenCreate(Integer shopId, Integer gameId, String optId);

    Optional<MyGame> findShopGame(Integer shopId, Integer gameId);

    void modifyInventory(@NotNull Integer id, @NotNull Integer num);
}
