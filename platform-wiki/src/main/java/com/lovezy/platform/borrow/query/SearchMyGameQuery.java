package com.lovezy.platform.borrow.query;

import com.lovezy.platform.wiki.query.WikiQuery;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

/**
 * 搜索我的游戏
 * Created by jin on 2017/11/14.
 */
@Getter
@Setter
@Alias("searchMyGameQuery")
public class SearchMyGameQuery extends WikiQuery{

    private String optId;

    /**
     * 我的ID
     */
    private String memberId;

    private Integer shopId;

    private String shopKey;

    private Byte hot;

}
