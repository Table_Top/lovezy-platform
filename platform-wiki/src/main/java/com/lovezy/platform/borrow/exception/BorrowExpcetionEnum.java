package com.lovezy.platform.borrow.exception;

/**
 * Created by jin on 2017/11/15.
 */
public enum BorrowExpcetionEnum {
    BORROW_MYGAME_NOT_FOUND ("000001","此租借游戏不存在"),
    BORROW_APPLY_NOT_FOUND ("000002","此租借申请不存在"),
    BORROW_APPLY_NO_AUTH ("000003","无权限操作此申请"),
    ;
    private String errorCode;
    private String errorMsg;

    BorrowExpcetionEnum(String errorCode, String errorMsg) {
        this.errorCode = "BORROW-" + errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
