package com.lovezy.platform.borrow.service;

import com.lovezy.platform.borrow.form.MyGameForm;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 服务端的 库存游戏包装类
 * Created by jin on 2017/11/19.
 */
@Getter
@Setter
@ToString
public class MyGameServiceForm {

    private MyGameForm myGameForm;

    private Integer shopId;

    private Byte type;
}
