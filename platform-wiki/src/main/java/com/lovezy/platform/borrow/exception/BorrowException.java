package com.lovezy.platform.borrow.exception;

import com.lovezy.platform.core.exception.BaseRuntimeException;

/**
 * Created by jin on 2017/11/15.
 */
public class BorrowException extends BaseRuntimeException{

    public BorrowException(String errorCode, String message) {
        super(errorCode, message);
    }

    public BorrowException(BorrowExpcetionEnum expcetionEnum) {
        super(expcetionEnum.getErrorCode(), expcetionEnum.getErrorMsg());
    }
}
