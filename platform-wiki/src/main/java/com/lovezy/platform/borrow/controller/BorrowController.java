package com.lovezy.platform.borrow.controller;

import com.lovezy.platform.borrow.form.BorrowForm;
import com.lovezy.platform.borrow.service.BorrowService;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 借还控制器
 * Created by jin on 2017/11/15.
 */
@RestController
@RequestMapping("/api/borrow")
public class BorrowController {

    @Autowired
    private BorrowService borrowService;

    @PostMapping("/apply")
    public ResponseEntity<Response> apply(@RequestBody BorrowForm form) {
        String mid = SiteContext.getCurrentMid();
        form.setMemberId(mid);
        borrowService.borrowGame(form);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/cancel")
    public ResponseEntity<Response> cancelApply(@RequestBody BorrowForm form) {
        String mid = SiteContext.getCurrentMid();
        form.setMemberId(mid);
        borrowService.cancleBorrow(form);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/agree")
    public ResponseEntity<Response> agreeApply(@RequestBody BorrowForm form) {
        String mid = SiteContext.getCurrentMid();
        form.setMemberId(mid);
        borrowService.agreeApply(form);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/reject")
    public ResponseEntity<Response> rejectApply(@RequestBody BorrowForm form) {
        String mid = SiteContext.getCurrentMid();
        form.setMemberId(mid);
        borrowService.rejectApply(form);
        return ResponseEntity.ok(ok());
    }

    @PostMapping("/return")
    public ResponseEntity<Response> returnItem(@RequestBody BorrowForm form) {
        String mid = SiteContext.getCurrentMid();
        form.setMemberId(mid);
        borrowService.returnItem(form);
        return ResponseEntity.ok(ok());
    }


}
