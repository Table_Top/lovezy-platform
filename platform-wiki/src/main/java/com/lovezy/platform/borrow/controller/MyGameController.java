package com.lovezy.platform.borrow.controller;

import com.github.pagehelper.Page;
import com.lovezy.platform.borrow.form.HotEditForm;
import com.lovezy.platform.borrow.form.SetGameForm;
import com.lovezy.platform.borrow.query.SearchMyGameQuery;
import com.lovezy.platform.borrow.service.MyGameService;
import com.lovezy.platform.borrow.vo.MyGameVo;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * 我的桌游
 * Created by jin on 2017/11/14.
 */
@RestController
@RequestMapping("/api/store")
public class MyGameController {

    @Autowired
    private MyGameService myGameService;

    @PostMapping("/bulk")
    public ResponseEntity<Response> addMyGames(@RequestBody SetGameForm form) {
        form.setOptId(SiteContext.getCurrentMid());
        myGameService.addMyGame(form);
        return new ResponseEntity<>(ok(), HttpStatus.CREATED);
    }

    @PostMapping("/delete")
    public ResponseEntity<Response> delMyGames(@RequestBody SetGameForm form) {
        form.setOptId(SiteContext.getCurrentMid());
        myGameService.delMyGame(form);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/search")
    public ResponseEntity<Response> searchMyGames(SearchMyGameQuery query) {
        query.setOptId(SiteContext.getCurrentMidNullAble());
        Page<MyGameVo> myGames = myGameService.findMyGame(query);
        return new ResponseEntity<>(ok(myGames), HttpStatus.OK);
    }

    @PutMapping("/hot")
    public ResponseEntity<Response> hotGame(@RequestBody HotEditForm form) {
        form.setMid(SiteContext.getCurrentMid());
        myGameService.editHotGames(form);
        return ResponseEntity.ok(ok());
    }
}
