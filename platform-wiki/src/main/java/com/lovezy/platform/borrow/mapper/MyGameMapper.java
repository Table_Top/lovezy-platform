package com.lovezy.platform.borrow.mapper;

import com.lovezy.platform.borrow.model.MyGame;
import com.lovezy.platform.borrow.model.MyGameExample;
import com.lovezy.platform.borrow.query.SearchMyGameQuery;
import com.lovezy.platform.borrow.vo.MyGameVo;
import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MyGameMapper extends BaseExampleMapper<MyGame, MyGameExample, Integer> {

    List<MyGameVo> searchMyGame(SearchMyGameQuery query);

    void modifyInventory(MyGame myGame);
}