package com.lovezy.platform.borrow.form;

import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/12/8.
 */
@Getter
@Setter
@ToString
public class HotEditForm {

    private Integer[] myGameIds;

    @Required(errorMsg = "状态不能为空")
    private Boolean hot;

    @Required(errorMsg = "状态不能为空")
    private String mid;
}
