package com.lovezy.platform.borrow.service.impl;

import com.lovezy.platform.borrow.enums.ApplyStatusEnum;
import com.lovezy.platform.borrow.form.BorrowForm;
import com.lovezy.platform.borrow.model.BorrowApply;
import com.lovezy.platform.borrow.service.BorrowApplyService;
import com.lovezy.platform.borrow.service.BorrowService;
import com.lovezy.platform.borrow.service.MyGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.lovezy.platform.core.utils.ObjectUtil.nonNull;

/**
 * 租借服务实现类
 * Created by jin on 2017/11/14.
 */
@Service
public class BorrowServiceImpl implements BorrowService{

    @Autowired
    private BorrowApplyService borrowApplyService;

    @Autowired
    private MyGameService myGameService;

    @Override
    @Transactional
    public void borrowGame(BorrowForm form) {
        borrowApplyService.applyBorrow(form);
    }

    @Override
    public void cancleBorrow(BorrowForm form) {
        borrowApplyService.confirmApply(form, ApplyStatusEnum.CANCEL);
    }

    @Transactional
    @Override
    public void agreeApply(BorrowForm form) {
        borrowApplyService.confirmApply(form, ApplyStatusEnum.AGREE);
    }

    @Override
    public void rejectApply(BorrowForm form) {
        borrowApplyService.confirmApply(form, ApplyStatusEnum.REJECT);
    }

    @Override
    @Transactional
    public void returnItem(BorrowForm form) {
        Integer borrowId = form.getBorrowId();
        String optId = form.getMemberId();
        Integer shopId = form.getShopId();
        nonNull(borrowId, optId, shopId);

        BorrowApply apply = new BorrowApply();
        apply.setBorrowId(borrowId);
        apply.setShopId(shopId);
        apply.setMemberId(optId);
        borrowApplyService.returnBorrowItem(apply);

    }

}
