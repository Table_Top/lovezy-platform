package com.lovezy.platform.borrow.enums;

/**
 * 桌游租借类别
 * Created by jin on 2017/11/15.
 */
public enum BorrowApplyTypeEnum {
    USER((byte)1),
    SHOP((byte)2)
    ;

    byte value;

    BorrowApplyTypeEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
