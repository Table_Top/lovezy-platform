package com.lovezy.platform.borrow.service;

import com.lovezy.platform.borrow.enums.ApplyStatusEnum;
import com.lovezy.platform.borrow.form.BorrowForm;
import com.lovezy.platform.borrow.model.BorrowApply;

/**
 * 借还申请订单服务实现
 * Created by jin on 2017/11/15.
 */
public interface BorrowApplyService {
    BorrowApply applyBorrow(BorrowForm form);

    BorrowApply confirmApply(BorrowForm form, ApplyStatusEnum applyStatus);

    void returnBorrowItem(BorrowApply apply);
}
