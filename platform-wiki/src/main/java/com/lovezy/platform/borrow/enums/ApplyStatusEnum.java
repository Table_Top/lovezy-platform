package com.lovezy.platform.borrow.enums;

/**
 * 申请状态 1-申请中 2-待收货 3-拒绝 4-取消 5-已收货 6-已归还
 * Created by jin on 2017/11/15.
 */
public enum  ApplyStatusEnum {
    APPLY_ING ((byte)1),
    AGREE ((byte)2),
    REJECT ((byte)3),
    CANCEL ((byte)4),
    RECEIVED ((byte)5),
    RETURNED ((byte)6),
    ;

    byte value;

    ApplyStatusEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
