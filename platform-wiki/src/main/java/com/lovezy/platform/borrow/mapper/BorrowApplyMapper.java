package com.lovezy.platform.borrow.mapper;

import com.lovezy.platform.borrow.model.BorrowApply;
import com.lovezy.platform.borrow.model.BorrowApplyExample;
import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BorrowApplyMapper extends BaseExampleMapper<BorrowApply, BorrowApplyExample, Integer> {

}