package com.lovezy.platform.borrow.form;

import com.lovezy.platform.borrow.model.MyGame;
import com.lovezy.platform.borrow.service.MyGameServiceForm;
import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.enums.TrueOrFalseEnum;

import java.math.BigDecimal;
import java.util.Date;

import static com.lovezy.platform.core.utils.ObjectUtil.ifPresent;

/**
 * 设置我的游戏表单管理
 * Created by jin on 2017/11/14.
 */
public class SetGameFormManager {

    public static MyGame toAddMyGame(MyGameServiceForm serviceForm, String optId) {
        MyGameForm form = serviceForm.getMyGameForm();
        MyGame myGame = new MyGame();
        myGame.setMemberId(optId);
        myGame.setGameId(form.getGameId());
        myGame.setInventory(ifPresent(form.getInventory(),1));
        myGame.setPrice(ifPresent(form.getPrice(), BigDecimal.ZERO));
        myGame.setStatus(StatusEnum.NORMAL.value());
        myGame.setShopId(serviceForm.getShopId());
        myGame.setType(serviceForm.getType());
        myGame.setHot(form.getHot() == null ? TrueOrFalseEnum.FALSE.value() : TrueOrFalseEnum.TRUE.value());
        Date now = new Date();
        myGame.setGmtCreate(now);
        myGame.setGmtModify(now);
        return myGame;
    }

    public static MyGame toAddShopGame(Integer shopId, Integer gameId, String optId) {
        MyGame myGame = new MyGame();
        myGame.setMemberId(optId);
        myGame.setGameId(gameId);
        myGame.setShopId(shopId);
        myGame.setInventory(1);
        myGame.setPrice(BigDecimal.ZERO);
        myGame.setStatus(StatusEnum.NORMAL.value());
        Date now = new Date();
        myGame.setGmtCreate(now);
        myGame.setGmtModify(now);
        return myGame;
    }



}
