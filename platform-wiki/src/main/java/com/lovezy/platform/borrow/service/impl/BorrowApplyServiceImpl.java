package com.lovezy.platform.borrow.service.impl;

import com.lovezy.platform.borrow.enums.ApplyStatusEnum;
import com.lovezy.platform.borrow.enums.MyGameTypeEnum;
import com.lovezy.platform.borrow.exception.BorrowException;
import com.lovezy.platform.borrow.exception.BorrowExpcetionEnum;
import com.lovezy.platform.borrow.form.BorrowForm;
import com.lovezy.platform.borrow.form.BorrowFormManager;
import com.lovezy.platform.borrow.mapper.BorrowApplyMapper;
import com.lovezy.platform.borrow.model.BorrowApply;
import com.lovezy.platform.borrow.model.MyGame;
import com.lovezy.platform.borrow.service.BorrowApplyService;
import com.lovezy.platform.borrow.service.MyGameService;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.wiki.exception.WikiException;
import com.lovezy.platform.wiki.exception.WikiExceptionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

import static com.lovezy.platform.core.utils.ObjectUtil.nonNull;

/**
 * 借还申请订单服务实现
 * Created by jin on 2017/11/15.
 */
@Service
public class BorrowApplyServiceImpl extends MapperService<BorrowApply,Integer,BorrowApplyMapper> implements BorrowApplyService{

    @Autowired
    private BorrowFormManager borrowFormManager;

    @Autowired
    private MyGameService myGameService;

    @Override
    public BorrowApply applyBorrow(BorrowForm form) {
        BorrowApply apply = borrowFormManager.toBorrowApply(form);
        apply.setStatus(ApplyStatusEnum.APPLY_ING.value());
        mapper.insertSelective(apply);
        return apply;
    }

    @Override
    public BorrowApply confirmApply(BorrowForm form, ApplyStatusEnum applyStatus) {
        String optId = form.getMemberId();
        Integer borrowId = form.getBorrowId();
        Integer shopId = form.getShopId();
        nonNull(optId, borrowId, shopId);
        BorrowApply apply = mapper.selectByPrimaryKey(borrowId);
        if (apply == null) {
            throw new BorrowException(BorrowExpcetionEnum.BORROW_APPLY_NOT_FOUND);
        }

        if (applyStatus == ApplyStatusEnum.CANCEL) {
            if (!apply.getMemberId().equals(form.getMemberId())) {
                throw new BorrowException(BorrowExpcetionEnum.BORROW_APPLY_NO_AUTH);
            }
        } else if (applyStatus == ApplyStatusEnum.AGREE || applyStatus == ApplyStatusEnum.REJECT) {
            if (!apply.getOwnId().equals(form.getMemberId())) {
                throw new BorrowException(BorrowExpcetionEnum.BORROW_APPLY_NO_AUTH);
            }
        }

        BorrowApply updateApply = new BorrowApply();
        updateApply.setBorrowId(borrowId);
        updateApply.setApplyStatus(applyStatus.value());
        updateApply.setGmtModify(new Date());
        mapper.updateByPrimaryKeySelective(updateApply);
        borrowOptFinish(applyStatus, apply.getMyGameId(), shopId, optId);
        return apply;
    }

    @Override
    public void returnBorrowItem(BorrowApply apply) {
        BorrowApply dbApply = mapper.selectByPrimaryKey(apply.getBorrowId());
        if (dbApply == null) {
            throw new WikiException(WikiExceptionEnum.BORROW_APPLY_NOT_FOUND);
        }
        if (!dbApply.getMemberId().equals(apply.getMemberId())) {
            throw new WikiException(WikiExceptionEnum.BORROW_APPLY_OPT_NO_AUTH);
        }

        BorrowApply update = new BorrowApply();
        update.setBorrowId(dbApply.getBorrowId());
        update.setApplyStatus(ApplyStatusEnum.RETURNED.value());
        update.setGmtModify(new Date());
        mapper.updateByPrimaryKeySelective(update);
        myGameService.modifyInventory(dbApply.getMyGameId(), 1);

        MyGame myGame = myGameService.selectById(dbApply.getMyGameId());
        Optional<MyGame> shopGame = myGameService.findShopGame(dbApply.getShopId(), myGame.getGameId());
        shopGame.ifPresent(myGame1 -> myGameService.modifyInventory(myGame1.getGameId(), -1));

    }


    /**
     * 租借操作结束后，的其他操作
     * @param applyStatus 租借操作状态
     */
    private void borrowOptFinish(ApplyStatusEnum applyStatus,Integer myGameId,Integer shopId,String optId) {
        Optional<MyGame> myGameOpt = myGameService.findMyGameWithRole(myGameId, MyGameTypeEnum.USER);
        if (!myGameOpt.isPresent()) {
            throw new WikiException(WikiExceptionEnum.GAME_OPT_NO_AUTH);
        }
        MyGame myGame = myGameOpt.get();
        if (applyStatus == ApplyStatusEnum.AGREE) {
            MyGame shopGame = myGameService.shopFindIfHasNotGameThenCreate(shopId, myGameId, optId);
            myGameService.modifyInventory(shopGame.getGameId(), 1);
            myGameService.modifyInventory(myGame.getGameId(), -1);
        }

    }

}
