package com.lovezy.platform.borrow.enums;

/**
 * 我的桌游库存类型
 * Created by jin on 2017/11/15.
 */
public enum MyGameTypeEnum {
    USER((byte)1),
    SHOP((byte)2)
    ;

    byte value;

    MyGameTypeEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
