package com.lovezy.platform.borrow.form;

import com.lovezy.platform.core.valid.annotation.Range;
import com.lovezy.platform.core.valid.annotation.Required;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 设置我的游戏库存表单
 * Created by jin on 2017/11/14.
 */
@Getter
@Setter
@ToString
public class SetGameForm {

    private MyGameForm[] games;

    private String optId;

    /**
     * 1-用户 2-商户
     */
    @Required(errorMsg = "库存类型不能为空")
    @Range(errorMsg = "库存类型格式错误")
    private Byte type;

    private Integer shopId;

}
