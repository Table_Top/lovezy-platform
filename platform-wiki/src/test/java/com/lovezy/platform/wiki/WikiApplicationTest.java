package com.lovezy.platform.wiki;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by jin on 2017/11/3.
 */
@SpringBootApplication
public class WikiApplicationTest {
    public static void main(String[] args) {
        SpringApplication.run(WikiApplicationTest.class, args);
    }
}
