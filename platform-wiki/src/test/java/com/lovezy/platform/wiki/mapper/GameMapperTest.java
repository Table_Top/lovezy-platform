package com.lovezy.platform.wiki.mapper;

import com.lovezy.platform.wiki.model.Game;
import com.lovezy.platform.wiki.model.GameExtend;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by jin on 2017/11/3.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {com.lovezy.platform.wiki.WikiApplicationTest.class})
public class GameMapperTest {

    @Autowired
    private GameMapper gameMapper;
    @Autowired
    private GameExtendMapper gameExtendMapper;

    @Test
    public void testSelectById() {
        Game game = gameMapper.selectByPrimaryKey(1);
        Assert.assertNotNull(game);
    }

    @Test
    public void testGameExtend() {
        GameExtend gameExtend = gameExtendMapper.selectByPrimaryKey(1);
        Assert.assertNotNull(gameExtend);

    }
}