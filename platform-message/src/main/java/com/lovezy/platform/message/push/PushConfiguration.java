package com.lovezy.platform.message.push;

import com.lovezy.platform.core.mq.MessageProducer;
import com.lovezy.platform.message.push.instance.IgetuiPush;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.core.JmsMessagingTemplate;

import javax.jms.Queue;

/**
 * Created by jin on 2017/12/17.
 */
@Configuration
public class PushConfiguration {

    @Bean("igetuiPush")
    @Primary
    public Pusher igetuiPush() {
        return new IgetuiPush();
    }

    @Bean("pushMessageProducer")
    public MessageProducer pushMessageProducer(
            @Qualifier("pushQueue")Queue pushQueue,
            JmsMessagingTemplate template
    ) {
        return new MessageProducer(template, pushQueue);
    }

}
