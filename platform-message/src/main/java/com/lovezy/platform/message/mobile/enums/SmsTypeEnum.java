package com.lovezy.platform.message.mobile.enums;

import com.lovezy.platform.core.exception.ServiceException;

/**短信类型 1-注册
 * Created by jin on 2017/11/29.
 */
public enum SmsTypeEnum {
    MEMBER_REGISTER((byte)1), //用户注册
    MEMBER_LOGIN((byte)2), //用户登陆
    EDIT_PASSWORD((byte)3), //修改密码
    ;
    byte value;

    SmsTypeEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }

    public static SmsTypeEnum valueOf(byte value) {
        for (SmsTypeEnum s : SmsTypeEnum.values()) {
            if (s.value == value) {
                return s;
            }
        }
        throw new ServiceException("短信类别不存在");
    }
}
