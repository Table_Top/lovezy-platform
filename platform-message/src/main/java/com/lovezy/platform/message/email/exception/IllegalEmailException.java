package com.lovezy.platform.message.email.exception;

import com.lovezy.platform.core.exception.BaseRuntimeException;
import com.lovezy.platform.message.email.enums.EmailIllegalEnum;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 非法的邮箱异常
 * Created by jin on 2017/11/9.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IllegalEmailException extends BaseRuntimeException{

    public IllegalEmailException(String errorCode, String message) {
        super(errorCode, message);
    }

    public IllegalEmailException(EmailIllegalEnum illegalEnum) {
        super("MESSAGE_EMAIL_" + illegalEnum.name(), illegalEnum.errorMsg());
    }
}
