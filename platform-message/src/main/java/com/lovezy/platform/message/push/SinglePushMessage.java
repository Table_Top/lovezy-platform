package com.lovezy.platform.message.push;

import lombok.Getter;
import lombok.Setter;

/**
 * 单个推送消息
 * Created by jin on 2017/12/16.
 */
@Getter
@Setter
public class SinglePushMessage<T> extends PushMessage<T>{

    private String id;
}
