package com.lovezy.platform.message.email.service;

import com.lovezy.platform.message.email.enums.EmailTypeEnum;
import com.lovezy.platform.message.email.model.Email;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

/**
 * 邮箱服务
 * Created by jin on 2017/11/9.
 */
public interface EmailService {
    Email sendEmail(EmailServiceTarget target);


    void verifyEmail(@NotNull String authKey, EmailTypeEnum type, Function<VerifyEmailResult,Boolean> handler);
}
