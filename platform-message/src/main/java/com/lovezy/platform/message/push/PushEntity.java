package com.lovezy.platform.message.push;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 推送透传消息
 * Created by jin on 2017/12/17.
 */

@AllArgsConstructor
@Getter
public class PushEntity<T> {

    private PushIType type;

    private T t;

}
