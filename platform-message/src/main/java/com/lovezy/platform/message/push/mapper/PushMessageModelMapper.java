package com.lovezy.platform.message.push.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.message.push.model.PushMessageModel;
import com.lovezy.platform.message.push.model.PushMessageModelExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PushMessageModelMapper extends BaseExampleMapper<PushMessageModel, PushMessageModelExample, Integer> {

}