package com.lovezy.platform.message.email.enums;

/**
 * 邮件不合法枚举
 * Created by jin on 2017/11/9.
 */
public enum  EmailIllegalEnum {
    NOT_FOUND("邮件不存在"),
    ALREADY_USED("邮件已被使用"),
    TYPE_ERROR ("邮件类别错误")
    ;

    EmailIllegalEnum(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    String errorMsg;

    public String errorMsg() {
        return errorMsg;
    }
}
