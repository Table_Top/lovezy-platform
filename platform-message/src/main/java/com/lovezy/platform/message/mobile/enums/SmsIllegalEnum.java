package com.lovezy.platform.message.mobile.enums;

/**
 * 短信不合法枚举
 * Created by jin on 2017/11/9.
 */
public enum SmsIllegalEnum {
    NOT_FOUND("短信验证码不存在"),
    ALREADY_USED("手机号已被使用"),
    TYPE_ERROR ("短信类别错误"),
    EXPIRE ("短信已过期")
    ;

    SmsIllegalEnum(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    String errorMsg;

    public String errorMsg() {
        return errorMsg;
    }
}
