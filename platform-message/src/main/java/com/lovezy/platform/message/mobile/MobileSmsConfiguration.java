package com.lovezy.platform.message.mobile;

import com.lovezy.platform.core.mq.MessageProducer;
import com.lovezy.platform.core.sms.SmsSender;
import com.lovezy.platform.message.mobile.service.SmsSenderService;
import com.lovezy.platform.message.mobile.service.impl.SmsSenderServiceImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.core.JmsMessagingTemplate;

import javax.jms.Queue;

/**
 * 手机短信服务配置
 * Created by jin on 2017/11/10.
 */
@Configuration
public class MobileSmsConfiguration {

    @Bean("alidayuSmsService")
    @Primary
    public SmsSenderService alidayuSmsService(
            @Qualifier("alidayuSmsSender") SmsSender smsSender){
        return new SmsSenderServiceImpl(smsSender);
    }

    @Bean("smsMessageProducer")
    public MessageProducer smsMessageProducer(@Qualifier("smsQueue")Queue queue, JmsMessagingTemplate template) {
        return new MessageProducer(template, queue);
    }

}
