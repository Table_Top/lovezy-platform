package com.lovezy.platform.message.push;

/**
 * 推送器
 * Created by jin on 2017/12/7.
 */
@FunctionalInterface
public interface Pusher {

    PushResponse push(PushMessage message);

}
