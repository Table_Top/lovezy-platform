package com.lovezy.platform.message.email.model;

import java.util.Date;

public class Email {
    /**
     * 主键
     */
    private Integer emailNo;

    /**
     * 用户ID
     */
    private String memberId;

    /**
     * 类别 1-注册用户
     */
    private Byte type;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 是否被使用1-是2-否
     */
    private Byte used;

    /**
     * 验证key
     */
    private String authKey;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 编辑时间
     */
    private Date gmtModify;

    public Integer getEmailNo() {
        return emailNo;
    }

    public void setEmailNo(Integer emailNo) {
        this.emailNo = emailNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Byte getUsed() {
        return used;
    }

    public void setUsed(Byte used) {
        this.used = used;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey == null ? null : authKey.trim();
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }
}