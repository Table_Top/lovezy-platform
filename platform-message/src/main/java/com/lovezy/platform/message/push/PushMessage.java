package com.lovezy.platform.message.push;

import lombok.Getter;
import lombok.Setter;

/**
 * 推送消息实体
 * Created by jin on 2017/12/7.
 */
@Getter
@Setter
public abstract class PushMessage<T> {


    private String title = "爱桌游";

    private String content;

    private PushEntity<T> entity;

    private String receiverMemberId;
}
