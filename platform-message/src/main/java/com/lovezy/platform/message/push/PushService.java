package com.lovezy.platform.message.push;

import com.lovezy.platform.common.extend.ExtendService;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.core.utils.JsonUtil;
import com.lovezy.platform.message.push.mapper.PushMessageModelMapper;
import com.lovezy.platform.message.push.model.PushMessageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 推送服务
 * Created by jin on 2017/12/17.
 */
@Service
public class PushService extends MapperService<PushMessageModel,Integer,PushMessageModelMapper>{

    @Autowired
    @Qualifier("igetuiPush")
    private Pusher igetuiPush;

    @Autowired
    private ExtendService extendService;

    public void pushMessage(PushMessage message) {
        if (message instanceof SinglePushMessage) {
            String deviceId = extendService.getDeviceId(message.getReceiverMemberId());
            ((SinglePushMessage) message).setId(deviceId);
        }
        PushResponse response = igetuiPush.push(message);
        if (response.getStatus()) {
            PushMessageModel messageModel = new PushMessageModel();
            messageModel.setEntity(JsonUtil.toJson(message.getEntity()));
            messageModel.setGmt_create(new Date());
            messageModel.setMemberId(message.getReceiverMemberId());
            mapper.insert(messageModel);
        }
    }


}
