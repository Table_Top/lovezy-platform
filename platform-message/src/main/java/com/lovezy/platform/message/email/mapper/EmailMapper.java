package com.lovezy.platform.message.email.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.message.email.model.Email;
import com.lovezy.platform.message.email.model.EmailExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmailMapper extends BaseExampleMapper<Email, EmailExample, Integer> {

}