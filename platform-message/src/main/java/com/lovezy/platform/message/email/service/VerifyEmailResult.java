package com.lovezy.platform.message.email.service;

import com.lovezy.platform.message.email.enums.EmailIllegalEnum;
import com.lovezy.platform.message.email.model.Email;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 检验邮箱是否合法结果
 * Created by jin on 2017/11/9.
 */
@Getter
@Setter
@ToString
public class VerifyEmailResult {

    private boolean success = false;

    private EmailIllegalEnum illegal;

    private Email email;
}
