package com.lovezy.platform.message.push;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 推送结果
 * Created by jin on 2017/12/7.
 */
@Getter
@AllArgsConstructor
public class PushResponse {

    private Boolean status;

    private String result;
}
