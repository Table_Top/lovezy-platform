package com.lovezy.platform.message.email.enums;

/**
 * 邮件业务类别枚举
 * Created by jin on 2017/11/9.
 */
public enum EmailTypeEnum {
    MEMBER_REGISTER((byte)1) //用户注册
    ;
    byte value;

    EmailTypeEnum(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }
}
