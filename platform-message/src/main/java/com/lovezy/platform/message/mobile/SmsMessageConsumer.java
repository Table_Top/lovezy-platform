package com.lovezy.platform.message.mobile;

import com.lovezy.platform.common.prop.MessageQueueName;
import com.lovezy.platform.core.mq.MessageConsumer;
import com.lovezy.platform.message.mobile.service.SmsSenderService;
import com.lovezy.platform.message.mobile.service.SmsServiceTarget;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 短信队列消费者
 * Created by jin on 2017/12/17.
 */
@Component("smsMessageConsumer")
public class SmsMessageConsumer implements MessageConsumer<SmsServiceTarget>{

    private final Log log = LogFactory.getLog(SmsMessageConsumer.class);

    @Autowired
    private SmsSenderService smsSenderService;

    @JmsListener(destination= MessageQueueName.smsQueue)
    @Override
    public void accept(SmsServiceTarget target) {
        if (log.isDebugEnabled()) {
            log.debug("接受到消息(" + MessageQueueName.smsQueue + ") -> " + target);
        }
        smsSenderService.sendSms(target);
    }

}
