package com.lovezy.platform.message.mobile.exception;

import com.lovezy.platform.core.exception.BaseRuntimeException;
import com.lovezy.platform.message.mobile.enums.SmsIllegalEnum;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 非法的邮箱异常
 * Created by jin on 2017/11/9.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IllegalMobileException extends BaseRuntimeException{

    public IllegalMobileException(String errorCode, String message) {
        super(errorCode, message);
    }

    public IllegalMobileException(SmsIllegalEnum illegalEnum) {
        super("MESSAGE_MOBILE_" + illegalEnum.name(), illegalEnum.errorMsg());
    }
}
