package com.lovezy.platform.message.mobile.service;

import com.lovezy.platform.message.mobile.enums.SmsTypeEnum;
import com.lovezy.platform.message.mobile.model.Mobile;
import org.jetbrains.annotations.NotNull;
import org.springframework.scheduling.annotation.Async;

import java.util.function.Function;

/**
 * 短信服务
 * Created by jin on 2017/11/10.
 */
public interface SmsSenderService {
    @Async
    Mobile sendSms(SmsServiceTarget target);

    void verifyMobile(@NotNull String authKey, @NotNull SmsTypeEnum type, Function<VerifyMobileResult, Boolean> handler);
}
