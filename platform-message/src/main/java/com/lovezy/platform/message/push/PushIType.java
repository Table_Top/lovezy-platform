package com.lovezy.platform.message.push;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 推送接口ID
 * Created by jin on 2017/12/17.
 */
public enum PushIType {

    NOTICE((byte)1), //通知
    GAME((byte)2), //桌游
    ;

    private byte value;

    PushIType(byte value) {
        this.value = value;
    }

    public byte value() {
        return value;
    }


    @JsonValue
    public String jsonValue() {
        return String.valueOf(value);
    }
}
