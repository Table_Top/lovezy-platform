package com.lovezy.platform.message.push;

import com.lovezy.platform.common.prop.MessageQueueName;
import com.lovezy.platform.core.mq.MessageConsumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 推送消息消费者
 * Created by jin on 2017/12/17.
 */
@Component("pushMessageConsumer")
public class PushMessageConsumer implements MessageConsumer<PushMessage>{

    private final Log log = LogFactory.getLog(PushMessageConsumer.class);

    @Autowired
    private PushService pushService;

    @JmsListener(destination = MessageQueueName.pushQueue)
    @Override
    public void accept(PushMessage message) {
        if (log.isDebugEnabled()) {
            log.debug("接受到消息(" + MessageQueueName.smsQueue + ") -> " + message);
        }
        pushService.pushMessage(message);
    }
}
