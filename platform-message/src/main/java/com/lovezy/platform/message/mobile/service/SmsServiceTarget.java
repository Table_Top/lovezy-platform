package com.lovezy.platform.message.mobile.service;

import com.lovezy.platform.core.sms.SmsTarget;
import com.lovezy.platform.message.mobile.enums.SmsTypeEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2017/11/29.
 */
@Getter
@Setter
@ToString
public class SmsServiceTarget extends SmsTarget{

    private String authKey;

    private SmsTypeEnum type;

    private String memberId;
}
