package com.lovezy.platform.message.mobile.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.message.mobile.model.Mobile;
import com.lovezy.platform.message.mobile.model.MobileExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MobileMapper extends BaseExampleMapper<Mobile, MobileExample, Integer> {

}