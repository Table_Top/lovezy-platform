package com.lovezy.platform.message.email.service;

import com.lovezy.platform.core.email.EmailTarget;
import com.lovezy.platform.message.email.enums.EmailTypeEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import static com.lovezy.platform.core.codec.CodecUtil.uuid;

/**
 * 邮箱服务信息
 * Created by jin on 2017/11/9.
 */
@Getter
@Setter
@ToString
public class EmailServiceTarget extends EmailTarget{

    private EmailTypeEnum emailType;

    private String memberId;

    private String authKey;

    public String createAuthKey() {
        this.authKey = uuid();
        return this.authKey;
    }

    public String getAuthKey() {
        if (authKey == null) {
            authKey = uuid();
        }
        return authKey;
    }
}
