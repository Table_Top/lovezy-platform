package com.lovezy.platform.message.mobile.service;

import com.lovezy.platform.message.mobile.enums.SmsIllegalEnum;
import com.lovezy.platform.message.mobile.model.Mobile;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 检验短信是否合法结果
 * Created by jin on 2017/11/9.
 */
@Getter
@Setter
@ToString
public class VerifyMobileResult {

    private boolean success = false;

    private SmsIllegalEnum illegal;

    private Mobile mobile;
}
