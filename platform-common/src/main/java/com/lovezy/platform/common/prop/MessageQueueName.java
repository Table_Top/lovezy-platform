package com.lovezy.platform.common.prop;

/**
 * Created by jin on 2017/12/17.
 */
public class MessageQueueName {

    public static final String smsQueue = "smsQueue";

    public static final String pushQueue = "pushQueue";
}
