package com.lovezy.platform.common.prop;

/**
 * 静态配置类
 * Created by jin on 2017/12/17.
 */
public class Constant {

    /**
     * TOKEN 有效期(天)）
     */
    public static Integer TOKEN_VALID_TIME = 7;

    /**
     * 短信验证码 有效期(分钟)）
     */
    public static Integer SMS_VALID_TIME = 30;

    /**
     * 短信验证码模板
      */
    public static String REGISTER_TEMPLATE_CODE = "SMS_117510558";

    public static Integer DOWNLOAD_URL_EXPIRE = 3600;

    public static Integer UPLOAD_TOKEN_EXPIRE_SECOND = 3600;

}
