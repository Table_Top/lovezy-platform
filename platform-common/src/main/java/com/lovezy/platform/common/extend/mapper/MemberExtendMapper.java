package com.lovezy.platform.common.extend.mapper;

import com.lovezy.platform.common.extend.model.MemberExtend;
import com.lovezy.platform.common.extend.model.MemberExtendExample;
import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MemberExtendMapper extends BaseExampleMapper<MemberExtend, MemberExtendExample, String> {

}