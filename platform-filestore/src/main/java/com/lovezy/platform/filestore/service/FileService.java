package com.lovezy.platform.filestore.service;

import com.lovezy.platform.filestore.model.File;
import com.lovezy.platform.filestore.oss.OssBucket;
import com.lovezy.platform.filestore.vo.FileVo;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * 文件服务
 * Created by jin on 2017/11/6.
 */
public interface FileService {
    @NotNull File findFileByUrl(@NotNull String url);

    File findFileById(@NotNull Integer id);

    ResponseEntity<byte[]> downloadFile(@NotNull String url, String downloadName);

    String downloadUrl(@NotNull String fileUrl);

    String downloadId(@NotNull Integer fileId);

    FileVo uploadFile(MultipartFile file, String bucket, String mid);

    FileVo uploadFile(java.io.File file, String mid);

    Map<String, String> createUploadToken(OssBucket bucket, String fileName);

    File saveFile(File file);
}
