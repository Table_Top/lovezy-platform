package com.lovezy.platform.filestore.exception;

import com.lovezy.platform.core.exception.ServiceException;

/**
 * 文件存储异常
 * Created by jin on 2017/11/6.
 */
public class FileStoreException extends ServiceException{

    public FileStoreException(FileStoreExceptionEnum exceptionEnum) {
        super(exceptionEnum.errorCode(), exceptionEnum.errorMsg());
    }

    public FileStoreException(String errorCode, String message) {
        super(errorCode, message);
    }
}
