package com.lovezy.platform.filestore.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 文件的 VO
 * Created by jin on 2017/11/6.
 */
@Getter
@Setter
@ToString
public class FileVo {
    /**
     * 文件名称
     */
    private String name;

    /**
     * 位置
     */
    private String url;

    private String contentType;

    /**
     * 大小 B
     */
    private Long size;

    private Date gmtCreate;
}
