package com.lovezy.platform.filestore.service;

import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.filestore.exception.FileStoreException;
import com.lovezy.platform.filestore.exception.FileStoreExceptionEnum;
import com.lovezy.platform.filestore.model.File;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.util.StringUtils;

import java.io.InputStream;
import java.util.Map;

/**
 * Created by jin on 2017/12/18.
 */
public interface FileHelper {

    String toDownloadUrl(String url);

    @Nullable String toDownloadUrl(File file);

    @NotNull byte[] getFileByteArray(File dbFile);

    @NotNull
    default String getFileExtension(@NotNull String fileName) {
        if (!StringUtils.hasText(fileName)) {
            throw new FileStoreException(FileStoreExceptionEnum.FILE_NAME_IS_BLANK);
        }
        String extension = "";
        Integer index = fileName.lastIndexOf('.');
        if(index != -1){
            extension = fileName.substring(index, fileName.length());
        }
        return extension;
    }

    void storeFile(InputStream inputStream, File fileData);

    default Map<String,String> createToken(String bucket, String fileName){
        return ImmutableMap.of();
    };
}
