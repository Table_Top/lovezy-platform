package com.lovezy.platform.filestore;

/**
 * 文件仓库缓存枚举
 * Created by jin on 2017/11/6.
 */
public enum  FileStoreCacheEnum {
    FILE_URL ("FILE_URL_"),
    FILE_ID ("FILE_ID_")
    ;
    private String prefix;

    FileStoreCacheEnum(String prefix) {
        this.prefix = prefix;
    }

    public String prefix() {
        return "FILESTORE-" + prefix;
    }
}
