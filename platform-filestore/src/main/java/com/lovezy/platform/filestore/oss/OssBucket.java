package com.lovezy.platform.filestore.oss;

/**
 * OSS Bucket 枚举
 * Created by jin on 2018/1/16.
 */
public enum OssBucket {
    oss("lovezy-oss",false),
    open("lovezy-open",true),
    dev_oss("lovezy-dev",false),
    dev_open("lovezy-dev-open",true),
    ;

    private String bucket;

    private boolean isOpen;

    OssBucket(String bucket, boolean isOpen) {
        this.bucket = bucket;
        this.isOpen = isOpen;
    }

    public String getBucket() {
        return bucket;
    }

    public boolean isOpen() {
        return isOpen;
    }

}
