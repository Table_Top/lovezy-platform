package com.lovezy.platform.filestore.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.filestore.model.File;
import com.lovezy.platform.filestore.model.FileExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FileMapper extends BaseExampleMapper<File, FileExample, Integer> {

}