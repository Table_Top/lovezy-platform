package com.lovezy.platform.filestore.service;

import com.lovezy.platform.filestore.model.File;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;

/**
 * Created by jin on 2017/12/18.
 */
@Slf4j
public class OpenOssFileHelper extends OssFileHelper{

    @Override
    public @Nullable String toDownloadUrl(String url) {
        return properties.getOpenUrl() + "/" + url;
    }

    @Override
    public @NotNull byte[] getFileByteArray(File dbFile) {
        return doGetObjectInFile(dbFile, properties.getOpenBucket());
    }

    @Override
    public void storeFile(InputStream inputStream, File fileData) {
        doSaveObject(inputStream, fileData, properties.getOpenBucket());
    }
}
