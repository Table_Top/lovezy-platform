package com.lovezy.platform.filestore;

import com.lovezy.platform.filestore.oss.OssBucket;
import com.lovezy.platform.filestore.oss.OssBucketChooser;
import com.lovezy.platform.filestore.service.FileHelper;
import com.lovezy.platform.filestore.service.LocalFileHelper;
import com.lovezy.platform.filestore.service.OssFileHelper;
import com.lovezy.platform.filestore.service.QiniuFileHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * 文件服务配置
 * Created by jin on 2017/11/6.
 */
@Configuration
public class FileStoreConfiguration {

    @Bean
    @Primary
    public FileHelper ossFileHelper(@Value("${app.filestore.oss}") String oss) {
        if ("local".equals(oss)) {
            return new LocalFileHelper();
        } else if("oss".equals(oss)){
            return new OssFileHelper();
        } else {
            return new QiniuFileHelper();
        }
    }

    @Bean
    public OssBucketChooser ossBucketChooser(@Value("${spring.profiles.active}") String active) {
        if ("dev".equals(active)) {
            return new OssBucketChooser(OssBucket.dev_oss, OssBucket.dev_open);
        }else{
            return new OssBucketChooser(OssBucket.oss, OssBucket.open);
        }
    }

}

