package com.lovezy.platform.filestore.oss;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by jin on 2017/12/18.
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "app.filestore.aliOss")
@Component
public class AliOssProperties {

    private String accessId;

    private String secretKey;

    private String bucket;

    private String openBucket;

    private String openUrl;

    private String endPoint;

    private String externalEndPoint;

}
