package com.lovezy.platform.filestore.oss;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by jin on 2018/1/21.
 */
@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "app.filestore.qiniu")
public class QiniuOssProperties {

    private String accessId;

    private String secretKey;

    private String bucket;

    private String domain;

    private String openDomain;

}
