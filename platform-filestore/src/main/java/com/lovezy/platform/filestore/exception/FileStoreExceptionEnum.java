package com.lovezy.platform.filestore.exception;

/**
 * 文件仓库异常枚举
 * Created by jin on 2017/11/6.
 */
public enum FileStoreExceptionEnum {
    FILE_NOT_FOUND  ("000001","无此文件"),
    FILE_MISSING  ("000002","文件不存在"),
    READ_FILE_ERROR  ("000003","读取文件错误"),
    FILE_NAME_IS_BLANK  ("000004","文件名为空"),
    FILE_STORE_ERROR  ("000005","文件存储失败")
    ;
    private String errorCode;
    private String errorMsg;

    FileStoreExceptionEnum(String errorCode, String errorMsg) {
        this.errorCode = "FILESTORE-" + errorCode;
        this.errorMsg = errorMsg;
    }

    public String errorCode() {
        return errorCode;
    }

    public String errorMsg() {
        return errorMsg;
    }
}
