package com.lovezy.platform.filestore.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by jin on 2018/1/21.
 */
@Getter
@Setter
@ToString
public class QiniuCallBackBody {

    private String fileKey;

    private String hash;

    private String dir;

    private Long size;

    private String name;

    private String mimeType;

    private String userToken;
}
