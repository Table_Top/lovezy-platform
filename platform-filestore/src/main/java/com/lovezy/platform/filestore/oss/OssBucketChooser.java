package com.lovezy.platform.filestore.oss;

import com.lovezy.platform.core.exception.ServiceException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jin on 2018/1/21.
 */
public class OssBucketChooser {

    final private Map<String, OssBucket> buckets = new HashMap<>(2);

    public OssBucketChooser(OssBucket ossBucket, OssBucket openBucket) {
        if (ossBucket == null || openBucket == null) {
            throw new RuntimeException("初始化OSS BUCKET Chooser失败");
        }
        buckets.put("oss", ossBucket);
        buckets.put("open", openBucket);
    }

    public OssBucket bucketOf(String bucket) {
        OssBucket ossBucket = buckets.get(bucket);
        if (ossBucket == null) {
            throw new ServiceException("Bucket类型错误");
        }
        return ossBucket;
    }

    public OssBucket bucketNameOf(String bucketName) {
        for (OssBucket ossBucket : OssBucket.values()) {
            if (ossBucket.getBucket().equals(bucketName)) {
                return ossBucket;
            }
        }
        throw new ServiceException("Bucket类型错误");
    }
}
