CREATE TABLE `d_shop` (
  `shopId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `memberId` char(32) NOT NULL COMMENT '用户ID',
  `name` varchar(256) DEFAULT NULL COMMENT '名字',
  `address` varchar(512) DEFAULT NULL COMMENT '地址',
  `tel` varchar(128) DEFAULT NULL COMMENT '联系方式',
  `status` tinyint(4) NOT NULL COMMENT '状态 1-正常 2-删除',
  `createId` char(32) NOT NULL COMMENT '创建人ID',
  `modifyId` char(32) NOT NULL COMMENT '修改人ID',
  `gmtCreate` datetime NOT NULL COMMENT '创建时间',
  `gmtModify` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`shopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商铺表';


CREATE TABLE `d_borrow_apply` (
  `borrowId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `myGameId` int(11) NOT NULL COMMENT '我的游戏ID',
  `memberId` char(32) NOT NULL COMMENT '申请人ID',
  `ownId` char(32) NOT NULL COMMENT '持有人ID',
  `price` decimal(12,3) NOT NULL COMMENT '价格',
  `comment` varchar(1024) DEFAULT NULL COMMENT '备注',
  `applyComment` varchar(1024) DEFAULT NULL COMMENT '申请结果备注',
  `applyStatus` tinyint(4) NOT NULL COMMENT '申请状态 1-申请中 2-同意 3-拒绝 4-取消',
  `status` tinyint(4) NOT NULL COMMENT '状态 1-正常 2-删除',
  `gmtCreate` datetime NOT NULL COMMENT '创建时间',
  `gmtModify` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`borrowId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='借用申请表';

CREATE TABLE `d_borrow_mygame` (
  `myGameId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `memberId` char(32) NOT NULL COMMENT '用户ID',
  `gameId` int(11) NOT NULL COMMENT '游戏ID',
  `Inventory` int(11) NOT NULL COMMENT '库存',
  `price` decimal(12,3) NOT NULL COMMENT '价格',
  `status` tinyint(4) NOT NULL COMMENT '状态 1-正常 2-删除',
  `gmtCreate` datetime NOT NULL COMMENT '创建时间',
  `gmtModify` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`myGameId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='我的游戏表';


CREATE TABLE `n_borrow_log` (
  `logId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `applyId` int(11) NOT NULL COMMENT '申请ID',
  `myGameId` int(11) NOT NULL COMMENT '我的桌游ID',
  `optId` char(32) NOT NULL COMMENT '操作人ID',
  `message` varchar(1024) NOT NULL COMMENT '操作内容',
  `gmtCreate` datetime NOT NULL COMMENT '生成时间',
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='借用日志';

ALTER TABLE `boardgame`.`d_borrow_mygame`
  ADD COLUMN `shopId` INT(11) NULL COMMENT '商铺ID' AFTER `memberId`,
  ADD COLUMN `type` TINYINT(4) NOT NULL COMMENT '类型 1-用户 2-商铺' AFTER `shopId`;

ALTER TABLE `boardgame`.`d_borrow_apply`
  CHANGE COLUMN `applyStatus` `applyStatus` TINYINT(4) NOT NULL COMMENT '申请状态 1-申请中 2-待收货 3-拒绝 4-取消 5-已收货 6-已归还' ;

CREATE TABLE `boardgame`.`d_together` (
  `togetherId` INT NOT NULL AUTO_INCREMENT COMMENT '主键',
  `npc` CHAR(32) NOT NULL COMMENT 'npcID',
  `shopId` INT(11) NOT NULL COMMENT '商铺ID',
  `gameId` INT(11) NOT NULL COMMENT '约局游戏ID',
  `total` INT(11) NOT NULL COMMENT '申请人数',
  `time` DATETIME NOT NULL COMMENT '约局时间',
  `status` TINYINT(4) NOT NULL COMMENT '状态 1-正常 2-删除',
  `applyStatus` TINYINT(4) NOT NULL COMMENT '申请状态 1-申请中 2-同意 3-拒绝 4-取消',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  `createId` CHAR(32) NOT NULL COMMENT '创建人',
  `modifyId` CHAR(32) NOT NULL COMMENT '修改人',
  PRIMARY KEY (`togetherId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COMMENT = '约局表';
CREATE TABLE `d_together_detail` (
  `togetherDetailId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `togetherId` int(11) NOT NULL COMMENT '约局ID',
  `memberId` char(32) NOT NULL COMMENT '参与人ID',
  `payStatus` tinyint(4) NOT NULL COMMENT '付款状态 1-未付款 2-已付款',
  `status` tinyint(4) NOT NULL COMMENT '状态 1-正常 2-删除',
  `gmtCreate` datetime NOT NULL COMMENT '创建时间',
  `gmtModify` datetime NOT NULL COMMENT '修改时间',
  `createId` char(32) NOT NULL COMMENT '创建人ID',
  `modifyId` char(32) NOT NULL COMMENT '修改人ID',
  PRIMARY KEY (`togetherDetailId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='约局详情表';

ALTER TABLE `boardgame`.`d_together`
  ADD COLUMN `confirmTotal` INT(11) NOT NULL COMMENT '成功人数' AFTER `total`;

ALTER TABLE `boardgame`.`d_borrow_apply`
  CHANGE COLUMN `ownId` `ownId` CHAR(32) NULL COMMENT '持有人ID' ,
  ADD COLUMN `shopId` INT(11) NULL COMMENT '商铺ID' AFTER `ownId`,
  ADD COLUMN `type` TINYINT(4) NOT NULL COMMENT '1-用户 2-商铺' AFTER `shopId`;
ALTER TABLE `boardgame`.`d_borrow_apply`
  CHANGE COLUMN `shopId` `shopId` INT(11) NULL DEFAULT NULL COMMENT '商铺ID' AFTER `memberId`,
  CHANGE COLUMN `memberId` `memberId` CHAR(32) NULL COMMENT '申请人ID' ,
  CHANGE COLUMN `ownId` `ownId` CHAR(32) NOT NULL COMMENT '持有人ID' ;
ALTER TABLE `boardgame`.`c_member`
  CHANGE COLUMN `email` `email` VARCHAR(128) NULL COMMENT '邮箱' ;
