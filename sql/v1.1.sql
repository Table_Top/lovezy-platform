ALTER TABLE d_game ADD column chineseName varchar(256) comment '中文名' after name;
ALTER TABLE d_game ADD column hit int(11) not null DEFAULT 0 comment '点击量';
ALTER TABLE d_game ADD column isChinese tinyint(4) not null DEFAULT 2 comment '是否完全汉化 1-是 2-否';

ALTER TABLE d_game_extend  MODIFY COLUMN gameId int(11) NOT NULL COMMENT '游戏ID';
ALTER TABLE c_trait  MODIFY COLUMN status tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态1-正常2-删除';


CREATE TABLE `n_email` (
  `emailNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `memberId` char(32) DEFAULT '' COMMENT '用户ID',
  `type` tinyint(4) NOT NULL COMMENT '类别 1-注册用户',
  `email` varchar(128) NOT NULL COMMENT '邮箱地址',
  `used` tinyint(4) NOT NULL DEFAULT '2' COMMENT '是否被使用1-是2-否',
  `authKey` char(32) NOT NULL COMMENT '验证key',
  `gmtCreate` datetime NOT NULL COMMENT '创建时间',
  `gmtModify` datetime NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`emailNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮箱表';

CREATE TABLE `n_mobile` (
  `mobileId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `memberId` char(32) DEFAULT NULL COMMENT '用户ID',
  `mobileNumber` char(11) NOT NULL COMMENT '手机号',
  `type` tinyint(4) NOT NULL COMMENT '业务类别',
  `used` tinyint(4) NOT NULL COMMENT '是否使用',
  `authKey` char(6) NOT NULL COMMENT '验证码',
  `status` varchar(45) NOT NULL COMMENT '1-正常 2-失败',
  `failReason` varchar(1024) DEFAULT NULL COMMENT '失败原因',
  `gmtCreate` datetime NOT NULL COMMENT '创建时间',
  `gmtModify` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`mobileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信流水表';

CREATE TABLE `c_member` (
  `memberId` char(32) NOT NULL,
  `avatar` int(11) DEFAULT NULL COMMENT '头像',
  `memberNo` int(11) DEFAULT NULL COMMENT '用户号',
  `username` varchar(32) NOT NULL COMMENT '登陆名称',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `nickname` varchar(32) NOT NULL COMMENT '用户名称',
  `email` varchar(128) NOT NULL COMMENT '邮箱',
  `mobile` varchar(13) DEFAULT NULL COMMENT '手机号',
  `emailStatus` tinyint(4) NOT NULL DEFAULT '1' COMMENT '邮箱验证状态 1-未认证 2-已认证',
  `salt` char(6) NOT NULL COMMENT '盐',
  `sex` tinyint(4) DEFAULT NULL COMMENT '性别 1-男 2-女',
  `regDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  `lastLoginDate` timestamp NULL DEFAULT NULL COMMENT '最近登陆时间',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态 1-正常 2-删除',
  `gmtModify` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `dzId` int(11) DEFAULT NULL COMMENT 'dz用户ID',
  `qqOpenId` varchar(64) DEFAULT NULL COMMENT 'QQID',
  `token` varchar(128) DEFAULT NULL COMMENT '网页token',
  `role` tinyint(4) NOT NULL DEFAULT '1' COMMENT '角色 1-普通 2-管理员',
  `mobileToken` varchar(128) DEFAULT NULL COMMENT '客户端token',
  PRIMARY KEY (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';


