ALTER TABLE `boardgame`.`c_member`
  CHANGE COLUMN `username` `username` VARCHAR(32) NULL COMMENT '登陆名称' ;
ALTER TABLE `boardgame`.`c_member`
  CHANGE COLUMN `password` `password` VARCHAR(32) NULL COMMENT '密码' ,
  CHANGE COLUMN `salt` `salt` CHAR(6) NULL COMMENT '盐' ;

CREATE TABLE `boardgame`.`d_game_follow` (
  `followId` INT NOT NULL AUTO_INCREMENT COMMENT '主键',
  `memberId` CHAR(32) NOT NULL COMMENT '用户ID',
  `gameId` INT(11) NOT NULL COMMENT '桌游ID',
  `status` TINYINT(4) NOT NULL COMMENT '状态 1-正常 2-删除',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`followId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '桌游关注表';

ALTER TABLE `boardgame`.`d_game_follow`
  ADD INDEX `member_id` (`memberId` ASC),
  ADD INDEX `game_id` (`gameId` ASC);


CREATE TABLE `boardgame`.`d_topic` (
  `topicId` INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `topicType` TINYINT(4) NOT NULL COMMENT '文章类型',
  `body` VARCHAR(10240) NOT NULL COMMENT '文章内容',
  `vedio` VARCHAR(1024) NULL COMMENT '视频地址',
  `gameId` INT(11) NULL COMMENT '游戏ID',
  `createId` CHAR(32) NOT NULL COMMENT '创建人ID',
  `modifyId` CHAR(32) NOT NULL COMMENT '修改人ID',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  `status` TINYINT NOT NULL COMMENT '状态 ',
  PRIMARY KEY (`topicId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '文章话题表';

CREATE TABLE `boardgame`.`d_topic_reply` (
  `replyId` INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parentId` INT(11) NULL COMMENT '父回复ID',
  `topicId` INT(11) NOT NULL COMMENT '话题ID',
  `text` VARCHAR(10240) NOT NULL COMMENT '内容',
  `status` TINYINT(4) NOT NULL COMMENT '状态',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  `createId` CHAR(32) NOT NULL COMMENT '创建人',
  `modifyId` CHAR(32) NOT NULL COMMENT '修改人',
  PRIMARY KEY (`replyId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '文章评论表';

CREATE TABLE `boardgame`.`d_topic_like` (
  `likeId` INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `topicId` INT(11) NOT NULL COMMENT '文章ID',
  `status` TINYINT(4) NOT NULL COMMENT '状态',
  `createId` CHAR(32) NOT NULL COMMENT '创建人',
  `gmtCreate` CHAR(32) NOT NULL,
  PRIMARY KEY (`likeId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '文章点赞表';

CREATE TABLE `boardgame`.`d_topic_reply_like` (
  `replyLikeId` INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `topicId` INT(11) NOT NULL COMMENT '文章ID',
  `replyId` INT(11) NOT NULL COMMENT '评论ID',
  `status` TINYINT(4) NOT NULL COMMENT '状态',
  `createId` CHAR(32) NOT NULL COMMENT '创建人',
  `gmtCreate` CHAR(32) NOT NULL,
  PRIMARY KEY (`replyLikeId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '文章评论点赞表';

CREATE TABLE `boardgame`.`d_activity` (
  `activityId` INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shopId` INT(11) NOT NULL,
  `title` VARCHAR(1024) NOT NULL,
  `content` VARCHAR(10240) NULL,
  `gmtCreate` DATETIME NOT NULL,
  `gmtModify` DATETIME NOT NULL,
  `createId` CHAR(32) NOT NULL,
  `modifyId` CHAR(32) NOT NULL,
  PRIMARY KEY (`activityId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '活动表';
CREATE TABLE `boardgame`.`d_activity_join` (
  `joinId` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(16) NULL COMMENT '姓名',
  `reason` VARCHAR(2048) NULL COMMENT '申请理由',
  `time` VARCHAR(2048) NULL COMMENT '参与时间',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`joinId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '活动申请表';

ALTER TABLE `boardgame`.`d_activity_join`
  ADD COLUMN `status` TINYINT NOT NULL COMMENT '状态' AFTER `gmtModify`;
ALTER TABLE `boardgame`.`d_activity`
  ADD COLUMN `status` TINYINT NOT NULL COMMENT '状态' AFTER `modifyId`;
ALTER TABLE `boardgame`.`d_activity_join`
  ADD COLUMN `activityId` INT(11) NOT NULL AFTER `joinId`;


ALTER TABLE `boardgame`.`d_shop`
  ADD COLUMN `shopKey` VARCHAR(45) NOT NULL COMMENT '商户KEY' AFTER `memberId`;
ALTER TABLE `boardgame`.`d_borrow_mygame`
  ADD COLUMN `hot` TINYINT(4) NOT NULL DEFAULT 2 COMMENT '热门' AFTER `price`;

ALTER TABLE `boardgame`.`d_topic_like`
  CHANGE COLUMN `gmtCreate` `gmtCreate` DATETIME NOT NULL COMMENT '日期' ;

ALTER TABLE `boardgame`.`d_topic_reply_like`
  CHANGE COLUMN `gmtCreate` `gmtCreate` DATETIME NOT NULL COMMENT '日期' ;
ALTER TABLE `boardgame`.`d_shop`
  CHANGE COLUMN `shopKey` `shopKey` VARCHAR(45) NULL COMMENT '商户KEY' ;
ALTER TABLE `boardgame`.`d_activity_join`
  ADD COLUMN `num` INT NULL COMMENT '人数' AFTER `time`;

CREATE TABLE `boardgame`.`d_collection` (
  `collectionId` INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sourceId` INT NOT NULL COMMENT '来源ID',
  `sourceType` TINYINT NOT NULL COMMENT '类型',
  `createId` CHAR(32) NOT NULL,
  `modifyId` CHAR(32) NOT NULL COMMENT '修改人',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  `status` TINYINT NOT NULL COMMENT '状态',
  PRIMARY KEY (`collectionId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '收藏表';

ALTER TABLE `boardgame`.`d_topic_reply`
  ADD COLUMN `upReplyId` INT NULL COMMENT '上级回复ID' AFTER `text`,
  ADD COLUMN `upReplyer` CHAR(32) NULL COMMENT '上级回复人' AFTER `upReplyId`;

CREATE TABLE `boardgame`.`d_notice` (
  `noticeId` INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sourceId` INT(11) NOT NULL COMMENT '来源ID',
  `sourceType` VARCHAR(45) NOT NULL COMMENT '来源类型',
  `memberId` CHAR(32) NOT NULL,
  `operator` CHAR(32) NOT NULL,
  `read` TINYINT NOT NULL COMMENT '已读未读状态',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL,
  PRIMARY KEY (`noticeId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '通知表';
CREATE TABLE `boardgame`.`d_notice_member` (
  `noticeMemberId` INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `noticeId` INT NOT NULL,
  `memberId` CHAR(32) NOT NULL,
  `status` TINYINT NOT NULL COMMENT '状态',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`noticeMemberId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '通知关于人表';
ALTER TABLE `boardgame`.`d_notice`
  ADD COLUMN `status` TINYINT NOT NULL COMMENT '状态' AFTER `gmtModify`;
ALTER TABLE `boardgame`.`d_notice`
  CHANGE COLUMN `sourceType` `sourceType` TINYINT NOT NULL COMMENT '来源类型' ;
-- 2017 12 10
ALTER TABLE `boardgame`.`n_mobile`
  CHANGE COLUMN `status` `status` TINYINT NOT NULL COMMENT '1-正常 2-失败' ;

-- 2017 12 12
ALTER TABLE `boardgame`.`c_member`
  CHARACTER SET = utf8mb4 ;
ALTER TABLE `boardgame`.`c_member`
  CHANGE COLUMN `username` `username` VARCHAR(32) CHARACTER SET 'utf8mb4' NULL DEFAULT NULL COMMENT '登陆名称' ;
ALTER TABLE `boardgame`.`c_member`
  CHANGE COLUMN `nickname` `nickname` VARCHAR(32) CHARACTER SET 'utf8mb4' NOT NULL COMMENT '用户名称' ;
ALTER SCHEMA `boardgame`  DEFAULT CHARACTER SET utf8mb4 ;


-- 20171216
CREATE TABLE `boardgame`.`n_push_message` (
  `pushId` INT NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` CHAR(32) NOT NULL COMMENT '用户ID',
  `entity` VARCHAR(10240) NULL COMMENT '消息实体',
  `gmt_create` DATETIME NOT NULL,
  PRIMARY KEY (`pushId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '推送消息';

CREATE TABLE `boardgame`.`c_member_extend` (
  `memberId` CHAR(32) NOT NULL COMMENT '用户ID',
  `token` CHAR(32) NULL,
  `clientId` CHAR(32) NULL,
  PRIMARY KEY (`memberId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '用户扩展属性表';

-- 20171218
ALTER TABLE `boardgame`.`c_file`
  ADD COLUMN `createId` CHAR(32) NOT NULL COMMENT '创建人ID' AFTER `gmtCreate`;

-- 20171220
ALTER TABLE `boardgame`.`d_topic`
  ADD COLUMN `sourceType` TINYINT(4) NULL COMMENT '视频来源类别' AFTER `vedio`,
  ADD COLUMN `conver` VARCHAR(1024) NULL COMMENT '视频封面' AFTER `sourceType`;
ALTER TABLE `boardgame`.`d_topic`
  CHANGE COLUMN `conver` `cover` VARCHAR(1024) NULL DEFAULT NULL COMMENT '视频封面' ;


ALTER TABLE `boardgame`.`d_topic`
  CHANGE COLUMN `vedio` `video` VARCHAR(1024) NULL DEFAULT NULL COMMENT '视频地址' ;


-- 20180104
CREATE TABLE `boardgame`.`d_game_operation` (
  `gameId` INT NOT NULL COMMENT '游戏ID',
  `newGame` INT(11) NULL COMMENT '最新游戏排序',
  `hotGame` INT(11) NULL COMMENT '最热游戏排序',
  `searchGame` INT(11) NULL COMMENT '搜索游戏排序',
  PRIMARY KEY (`gameId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '百科运维表';

-- 20180113
CREATE TABLE `boardgame`.`d_game_rule` (
  `ruleId` INT NOT NULL AUTO_INCREMENT COMMENT '规则ID',
  `gameId` INT NOT NULL COMMENT '游戏ID',
  `content` VARCHAR(10240) NULL COMMENT '规则内容',
  `pdfId` INT(11) NULL COMMENT 'pdfID',
  `status` TINYINT NOT NULL COMMENT '状态1正常2删除',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  `createId` CHAR(32) NOT NULL COMMENT '创建人ID',
  `modifyId` CHAR(32) NOT NULL COMMENT '修改人',
  PRIMARY KEY (`ruleId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '游戏规则';

-- 20180116
ALTER TABLE `boardgame`.`d_topic`
  ADD COLUMN `title` VARCHAR(32) NULL COMMENT '标题' AFTER `topicId`;

-- 20180121
ALTER TABLE `boardgame`.`d_game`
  ADD COLUMN `bigType` TINYINT(4) NULL COMMENT '大类' AFTER `name`;

-- 20180123
CREATE TABLE `boardgame`.`c_member_role` (
  `memberId` CHAR(32) NOT NULL COMMENT '用户ID',
  `superAdmin` TINYINT(4) NULL COMMENT '超级管理员',
  `shopManager` TINYINT(4) NULL COMMENT '商户管理员',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  `modifyId` CHAR(32) NULL COMMENT '修改人ID',
  PRIMARY KEY (`memberId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '用户角色表';

ALTER TABLE `boardgame`.`n_push_message`
  CHANGE COLUMN `memberId` `memberId` CHAR(32) NULL COMMENT '用户ID' ;

