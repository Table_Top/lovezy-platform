CREATE TABLE `d_play` (
  `playId` int(11) NOT NULL AUTO_INCREMENT COMMENT '剧本ID',
  `name` varchar(45) DEFAULT NULL COMMENT '名字',
  `logo` char(32) DEFAULT NULL COMMENT 'logo',
  `point` float DEFAULT NULL COMMENT '评分',
  `createId` char(32) NOT NULL COMMENT '创建人',
  `modifyId` char(32) NOT NULL COMMENT '修改人',
  `gmtCreate` datetime NOT NULL COMMENT '创建时间',
  `gmtModify` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`playId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='剧本表';
CREATE TABLE `boardgame`.`d_play_process` (
  `processId` INT NOT NULL AUTO_INCREMENT,
  `playId` INT(11) NOT NULL COMMENT '剧本ID',
  `content` VARCHAR(512) NULL COMMENT '主题内容',
  `title` VARCHAR(45) NULL COMMENT '标题',
  `orderNum` INT(11) NULL COMMENT '排序顺序',
  `createId` CHAR(32) NOT NULL COMMENT '创建人',
  `modifyId` CHAR(32) NOT NULL COMMENT '修改人',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`processId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '剧本流程表';

CREATE TABLE `boardgame`.`d_play_character` (
  `characterId` INT NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `avatar` CHAR(32) NULL COMMENT '角色头像',
  `name` VARCHAR(45) NULL COMMENT '角色名字',
  `description` VARCHAR(512) NULL COMMENT '角色描述',
  `createId` CHAR(32) NOT NULL COMMENT '创建人ID',
  `modifyId` CHAR(32) NOT NULL COMMENT '修改人ID',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`characterId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '剧本角色表';
CREATE TABLE `boardgame`.`d_play_character_skill` (
  `skillId` INT NOT NULL AUTO_INCREMENT COMMENT '技能iD',
  `name` VARCHAR(45) NULL COMMENT '名称',
  `descrption` VARCHAR(256) NULL COMMENT '描述',
  `createId` CHAR(32) NOT NULL COMMENT '创建人',
  `modifyId` CHAR(32) NOT NULL COMMENT '修改人',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`skillId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '角色技能表';
CREATE TABLE `boardgame`.`d_play_item` (
  `itemId` INT NOT NULL AUTO_INCREMENT,
  `playId` INT NOT NULL COMMENT '剧本ID',
  `name` VARCHAR(45) NULL COMMENT '名字',
  `description` VARCHAR(256) NULL COMMENT '描述',
  `createId` CHAR(32) NOT NULL COMMENT '创建人',
  `modifyId` CHAR(32) NOT NULL COMMENT '修改人',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NULL COMMENT '修改时间',
  PRIMARY KEY (`itemId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '桌游道具表';
ALTER TABLE `boardgame`.`d_play`
  ADD COLUMN `status` TINYINT NOT NULL COMMENT '状态1正常2删除' AFTER `gmtModify`;
ALTER TABLE `boardgame`.`d_play_process`
  ADD COLUMN `status` TINYINT NOT NULL COMMENT '状态1正常2删除' AFTER `gmtModify`;
ALTER TABLE `boardgame`.`d_play_character`
  ADD COLUMN `status` TINYINT NOT NULL COMMENT '状态1正常2删除' AFTER `gmtModify`;
ALTER TABLE `boardgame`.`d_play_character_skill`
  ADD COLUMN `status` TINYINT NOT NULL COMMENT '状态1正常2删除' AFTER `gmtModify`;
ALTER TABLE `boardgame`.`d_play_item`
  ADD COLUMN `status` TINYINT NOT NULL COMMENT '状态1正常2删除' AFTER `gmtModify`;

ALTER TABLE `boardgame`.`d_play_character`
  ADD COLUMN `playId` INT NOT NULL COMMENT '剧本ID' AFTER `characterId`;
ALTER TABLE `boardgame`.`d_play_character_skill`
  ADD COLUMN `characterId` INT NOT NULL COMMENT '角色ID' AFTER `skillId`;
ALTER TABLE `boardgame`.`d_play_item`
  ADD COLUMN `useable` TINYINT(4) NULL COMMENT '可用性 1-是 2-否' AFTER `description`;
ALTER TABLE `boardgame`.`d_play_character_skill`
  CHANGE COLUMN `descrption` `description` VARCHAR(256) NULL DEFAULT NULL COMMENT '描述' ;
ALTER TABLE `boardgame`.`d_play_item`
  ADD COLUMN `pic` INT NULL COMMENT '图片' AFTER `description`;
ALTER TABLE `boardgame`.`d_play_character_skill`
  ADD COLUMN `pic` INT NULL COMMENT '图片' AFTER `description`;
ALTER TABLE `boardgame`.`d_play_character`
  CHANGE COLUMN `avatar` `avatar` CHAR(64) NULL DEFAULT NULL COMMENT '角色头像' ;


CREATE TABLE `boardgame`.`d_play_scene` (
  `sceneId` INT NOT NULL AUTO_INCREMENT COMMENT '主键',
  `playId` INT NOT NULL COMMENT '剧本ID',
  `processId` INT NOT NULL COMMENT '流程ID',
  `createId` CHAR(32) NOT NULL COMMENT '创建人ID',
  `status` TINYINT NOT NULL COMMENT '状态 1-正常 2-删除(关闭)',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  `gmtModify` DATETIME NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`sceneId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '桌游剧本实例';
CREATE TABLE `boardgame`.`d_play_scene_character` (
  `sceneCharacterId` INT NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sceneId` INT NOT NULL COMMENT '实例ID',
  `characterId` INT NOT NULL COMMENT '角色ID',
  `memberId` CHAR(32) NULL COMMENT '所在用户ID',
  PRIMARY KEY (`sceneCharacterId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '剧本实例角色';
CREATE TABLE `boardgame`.`d_play_scene_character_skill` (
  `sceneSkillId` INT NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sceneCharacterId` INT NOT NULL COMMENT '实例角色ID',
  `sceneId` INT NOT NULL COMMENT '实例ID',
  `skillId` INT NOT NULL COMMENT '技能ID',
  `enbleStatus` TINYINT NOT NULL COMMENT '是否可用 1-是 2-否',
  `status` TINYINT NOT NULL COMMENT '1-正常 2-删除',
  PRIMARY KEY (`sceneSkillId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '剧本实例角色技能表';
CREATE TABLE `boardgame`.`d_play_scene_character_item` (
  `sceneItemId` INT NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sceneCharacterId` INT NOT NULL COMMENT '实例角色ID',
  `sceneId` INT NOT NULL COMMENT '实例ID',
  `itemId` INT NOT NULL COMMENT '道具ID',
  `enbleStatus` TINYINT NOT NULL COMMENT '是否可用 1-是 2-否',
  `status` TINYINT NOT NULL COMMENT '1-正常 2-删除',
  PRIMARY KEY (`sceneItemId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '剧本实例角色道具表';
CREATE TABLE `boardgame`.`d_play_scene_log` (
  `sceneLogId` INT NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sceneId` INT NOT NULL COMMENT '实例ID',
  `content` VARCHAR(10240) NOT NULL COMMENT '内容',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`sceneLogId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '剧本实例日志';

ALTER TABLE `boardgame`.`d_play_scene_character_item`
  CHANGE COLUMN `enbleStatus` `enableStatus` TINYINT(4) NOT NULL COMMENT '是否可用 1-是 2-否' ;
ALTER TABLE `boardgame`.`d_play_scene_character_skill`
  CHANGE COLUMN `enbleStatus` `enableStatus` TINYINT(4) NOT NULL COMMENT '是否可用 1-是 2-否' ;


CREATE TABLE `boardgame`.`d_play_attachment` (
  `attachmentId` INT NOT NULL AUTO_INCREMENT COMMENT '剧本附件',
  `fileKey` CHAR(32) NOT NULL COMMENT '文件KEY',
  `name` VARCHAR(128) NOT NULL COMMENT '文件名称',
  `status` TINYINT NOT NULL COMMENT '状态',
  `gmtCreate` DATETIME NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`attachmentId`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '剧本附件';
ALTER TABLE `boardgame`.`d_play_attachment`
  CHANGE COLUMN `fileKey` `fileKey` CHAR(64) NOT NULL COMMENT '文件KEY' ;

ALTER TABLE `boardgame`.`d_play`
  ADD COLUMN `description` VARCHAR(10240) NULL COMMENT '描述' AFTER `point`;
ALTER TABLE `boardgame`.`d_play_character`
  ADD COLUMN `introduce` VARCHAR(128) NULL COMMENT '介绍' AFTER `description`;
