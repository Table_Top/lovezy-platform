# lovezy-platform
桌游平台

## 基础模块
### platform-configuration 
平台的配置模块，主要负责控制依赖注入的bean，以及其他公共配置

### platform-core
平台核心模块，公用类以及公用工具

### platform-filestore
文件存储操作模块

### platform-message
平台外部消息模块(邮件，短信，推送等)

## 业务模块
### platform-member
平台用户模块

### platform-discuz
兼容论坛模块

### platform-wiki
游戏百科

### platform-notice
平台通知中心

### platform-web
平台web模块
