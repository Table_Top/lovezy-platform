package com.lovezy.platform.notice.service;

import com.github.pagehelper.PageInfo;
import com.lovezy.platform.notice.form.NoticeForm;
import com.lovezy.platform.notice.model.Notice;
import com.lovezy.platform.notice.query.NoticeQuery;
import com.lovezy.platform.notice.vo.NoticeVo;

/**
 * Created by jin on 2017/12/9.
 */
public interface NoticeService {
    Notice newNotice(NoticeForm form);

    void deleteNotice(Notice notice);

    PageInfo<NoticeVo> findNoticeList(NoticeQuery query);

    Integer notReadNotice(NoticeQuery query);

    void readAllNotice(NoticeQuery query);
}
