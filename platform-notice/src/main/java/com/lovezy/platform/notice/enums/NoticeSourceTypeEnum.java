package com.lovezy.platform.notice.enums;

import com.lovezy.platform.core.exception.ServiceException;

/**
 * Created by jin on 2017/12/9.
 */
public enum  NoticeSourceTypeEnum {
    TOPIC_REPLY((byte)1,false,"回复了你的评论："), // 话题回复
    TOPIC_LIKE((byte)2,true,"赞了你的评论：") //话题点赞
    ;
    private byte value;

    private boolean addMember;

    private String prefix;

    NoticeSourceTypeEnum(byte value,boolean addMember,String prefix) {
        this.value = value;
        this.addMember = addMember;
        this.prefix = prefix;
    }

    public byte value() {
        return value;
    }

    public boolean needAddMember() {
        return addMember;
    }

    public String prefix() {
        return prefix;
    }

    public static NoticeSourceTypeEnum valueOf(byte value) {
        for (NoticeSourceTypeEnum noticeSourceTypeEnum : NoticeSourceTypeEnum.values()) {
            if (noticeSourceTypeEnum.value == value) {
                return noticeSourceTypeEnum;
            }
        }
        throw new ServiceException("通知类型错误");
    }
}
