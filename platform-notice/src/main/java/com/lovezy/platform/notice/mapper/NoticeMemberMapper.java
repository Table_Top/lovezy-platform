package com.lovezy.platform.notice.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.notice.model.NoticeMember;
import com.lovezy.platform.notice.model.NoticeMemberExample;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NoticeMemberMapper extends BaseExampleMapper<NoticeMember, NoticeMemberExample, Integer> {

    List<String> findNoticeMemberIds(Integer noticeId);
}