package com.lovezy.platform.notice.model;

import java.util.Date;

public class NoticeMember {
    private Integer noticeMemberId;

    private Integer noticeId;

    private String memberId;

    private Byte status;

    private Date gmtCreate;

    private Date gmtModify;

    public Integer getNoticeMemberId() {
        return noticeMemberId;
    }

    public void setNoticeMemberId(Integer noticeMemberId) {
        this.noticeMemberId = noticeMemberId;
    }

    public Integer getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Integer noticeId) {
        this.noticeId = noticeId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }
}