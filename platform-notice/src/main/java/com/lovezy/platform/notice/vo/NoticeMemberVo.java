package com.lovezy.platform.notice.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

/**
 * Created by jin on 2017/12/9.
 */
@Getter
@Setter
@ToString
@Alias("noticeMemberVo")
public class NoticeMemberVo {

    private String names;

    private Integer num;

}
