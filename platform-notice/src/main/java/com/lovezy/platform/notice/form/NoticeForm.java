package com.lovezy.platform.notice.form;

import com.lovezy.platform.core.valid.annotation.Required;
import com.lovezy.platform.notice.enums.NoticeSourceTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * Created by jin on 2017/12/9.
 */
@ToString
@AllArgsConstructor
@Getter
public class NoticeForm {

    @Required(errorMsg = "来源ID不能为空")
    private Integer sourceId;

    @Required(errorMsg = "来源类型不能为空")
    private NoticeSourceTypeEnum sourceType;

    @Required(errorMsg = "用户ID不能为空")
    private String mid;

    @Required(errorMsg = "操作人不能为空")
    private String operator;
}
