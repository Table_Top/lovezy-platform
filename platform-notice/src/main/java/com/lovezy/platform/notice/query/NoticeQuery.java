package com.lovezy.platform.notice.query;

import com.lovezy.platform.core.base.query.BaseQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * Created by jin on 2017/12/9.
 */
@Getter
@Setter
@ToString
@Alias("noticeQuery")
public class NoticeQuery extends BaseQuery{

    private String mid;

    private Date time;

}
