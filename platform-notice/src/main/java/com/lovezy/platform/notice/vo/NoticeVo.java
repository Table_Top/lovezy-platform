package com.lovezy.platform.notice.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * Created by jin on 2017/12/9.
 */
@Getter
@Setter
@ToString
@Alias("noticeVo")
public class NoticeVo {

    private Integer noticeId;

    private String operator;

    private String operatorName;

    private Byte type;

    private Integer sourceId;

    private String data;

    private Date gmtCreate;

}
