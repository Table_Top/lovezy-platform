package com.lovezy.platform.notice.service;

import com.lovezy.platform.notice.model.Notice;
import com.lovezy.platform.notice.vo.NoticeMemberVo;
import org.jetbrains.annotations.NotNull;

/**
 * Created by jin on 2017/12/9.
 */
public interface NoticeMemberService {
    boolean newNoticeMember(Notice notice, String operator);

    NoticeMemberVo findNoticeMembers(@NotNull Integer noticeId);
}
