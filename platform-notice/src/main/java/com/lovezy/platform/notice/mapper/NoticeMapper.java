package com.lovezy.platform.notice.mapper;

import com.lovezy.platform.core.base.mapper.BaseExampleMapper;
import com.lovezy.platform.notice.model.Notice;
import com.lovezy.platform.notice.model.NoticeExample;
import com.lovezy.platform.notice.query.NoticeQuery;
import com.lovezy.platform.notice.vo.NoticeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface NoticeMapper extends BaseExampleMapper<Notice, NoticeExample, Integer> {
    List<NoticeVo> findNoticeList(NoticeQuery query);

    void readAllNotice(@Param("mid") String mid);

    NoticeVo findNoticeVo(@Param("noticeId") Integer noticeId);
}