package com.lovezy.platform.notice.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.ImmutableMap;
import com.lovezy.platform.core.base.Response;
import com.lovezy.platform.core.base.SiteContext;
import com.lovezy.platform.notice.model.Notice;
import com.lovezy.platform.notice.query.NoticeQuery;
import com.lovezy.platform.notice.service.NoticeService;
import com.lovezy.platform.notice.vo.NoticeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Map;

import static com.lovezy.platform.core.base.Response.ok;

/**
 * Created by jin on 2017/12/9.
 */
@RestController
@RequestMapping("/api/notice")
public class NoticeController {

    @Autowired
    private NoticeService noticeService;

    @GetMapping("/list")
    public ResponseEntity<Response> noticeList(Date time) {
        NoticeQuery query = new NoticeQuery();
        query.setMid(SiteContext.getCurrentMid());
        query.setTime(time);
        PageInfo<NoticeVo> noticeList = noticeService.findNoticeList(query);
        return ResponseEntity.ok(ok(noticeList));
    }

    @DeleteMapping({"", "/"})
    public ResponseEntity<Response> deleteNotice(@RequestBody Map<String,Integer> params) {
        Notice notice = new Notice();
        notice.setNoticeId(params.get("noticeId"));
        notice.setMemberId(SiteContext.getCurrentMid());
        noticeService.deleteNotice(notice);
        return ResponseEntity.ok(ok());
    }

    @GetMapping("/unReadCnt")
    public ResponseEntity<Response> unReadCnt() {
        NoticeQuery query = new NoticeQuery();
        query.setMid(SiteContext.getCurrentMid());
        Integer unReadCnt = noticeService.notReadNotice(query);
        return ResponseEntity.ok(ok(ImmutableMap.of("cnt", unReadCnt)));
    }

}
