package com.lovezy.platform.notice.service.impl;

import com.lovezy.platform.core.base.enums.StatusEnum;
import com.lovezy.platform.core.base.service.MapperService;
import com.lovezy.platform.member.service.MemberService;
import com.lovezy.platform.notice.mapper.NoticeMemberMapper;
import com.lovezy.platform.notice.model.Notice;
import com.lovezy.platform.notice.model.NoticeMember;
import com.lovezy.platform.notice.model.NoticeMemberExample;
import com.lovezy.platform.notice.service.NoticeMemberService;
import com.lovezy.platform.notice.vo.NoticeMemberVo;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jin on 2017/12/9.
 */
@Service
public class NoticeMemberServiceImpl extends MapperService<NoticeMember,Integer,NoticeMemberMapper> implements NoticeMemberService{

    @Autowired
    private MemberService memberService;

    @Override
    public boolean newNoticeMember(Notice notice, String operator) {
        NoticeMemberExample example = new NoticeMemberExample();
        Integer noticeId = notice.getNoticeId();
        example.createCriteria()
                .andNoticeIdEqualTo(noticeId)
                .andMemberIdEqualTo(operator)
                .andStatusEqualTo(StatusEnum.NORMAL.value());
        int count = mapper.countByExample(example);
        if (count == 0) {
            Date now = new Date();
            NoticeMember newMember = new NoticeMember();
            newMember.setNoticeId(noticeId);
            newMember.setMemberId(operator);
            newMember.setStatus(StatusEnum.NORMAL.value());
            newMember.setGmtCreate(now);
            newMember.setGmtModify(now);
            mapper.insert(newMember);
            return true;
        }
        return false;
    }

    @Override
    public NoticeMemberVo findNoticeMembers(@NotNull Integer noticeId) {
        List<String> noticeMemberIds = mapper.findNoticeMemberIds(noticeId);
        int num = noticeMemberIds.size();
        if (num >= 2) {
            noticeMemberIds = noticeMemberIds.subList(0, 2);
        }
        List<String> names = memberService.findNames(noticeMemberIds);
        String showNames = names.stream().collect(Collectors.joining("、"));
        NoticeMemberVo memberVo = new NoticeMemberVo();
        memberVo.setNames(showNames);
        memberVo.setNum(num);
        return memberVo;
    }



}
